# 项目周期

6个工作日 → 下周三下午 11号下午

# 服务器应用

服务器应用的ip

> 182.92.235.201
# 本地开发环境  
后台PC前端服务器ip地址为localhost:8080  
后台PC后端服务器ip地址为localhost:8083  
通过该应用可以访问请求，并且进行抓包

接口中的数据可以通过服务器访问直接获得数据，也可以访问接口文档网站

[Swagger UI](http://182.92.235.201:8083/swagger-ui.html)

# 项目进度表

【腾讯文档】38th项目二进度表
https://docs.qq.com/sheet/DSlh0cUl4d3B6UVNj

项目中的接口列表可以查看进度表
### 参与贡献
1. 李鑫 ( beiji )
2. 罗旭晨( nihaowa )
3. 杨帆 ( yangfan )
4. 张福强 ( zhangfuqiang )
5. 薛宇凡 ( charadic )
6. 唐伟 ( fly )
### 分工
- beiji 1 - 9
- zhangfuqiang 10 - 14  68 - 74
- fly 15 - 26
- charadic 27 - 39
- yanfan 40 -52
- nihaowa 53 - 67
### 进度  
正在进行的模块标记为黄色  
已完成的模块(没有bug的才算!!!)标记为绿色  
搞不定的模块标记为红色或者直接问组长

# 项目中的开发中常见的问题  
每天22点以后和组长演示后提交当日代码(单分支会覆盖，不要当内鬼)  
git提交流程 
```shell
git add .
git commit -m "Author:beiji Date:2022-05-05 17:19:00 Msg:README.md文档"
git push
```
错峰提交基本不会出现冲突，出现冲突后联系组长

## Shiro课程

Shiro的整合 → 视频课程13之后在进行整合

login和info两个请求，先给大家提供了伪代码的写法，整合完Shiro之后替换为Shiro的代码

## 订单状态

![image-20220505000645024](https://gitee.com/stone4j/picture-bed/raw/master/img/image-20220505000645024.png)

## 支付

支付不需要做，后面的微服务阶段做支付

订单相关的只需要修改对应的订单状态即可

## 数组

逆向工程根据数据库中的字段varchar生成的是String类型的成员变量

而前端需要响应的是数组数据

第二天我们讲TypeHandler的时候处理

## 静态资源

静态资源所处的location配置文件路径，不要配置为classpath路径

## 小程序

小程序默认的账号用户名和密码都是user123