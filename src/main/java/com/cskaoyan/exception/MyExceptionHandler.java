package com.cskaoyan.exception;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Charadic
 * @since 2022/05/11 15:47
 */
@RestControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(MyExcpetion.class)
    public BaseRespVo handleMyException(MyExcpetion e) {//形参中可以直接获得抛出的异常
        return BaseRespVo.ok(null, e.getMessage(), 666);
    }

}
