package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketBrand;
import com.cskaoyan.bean.reverseengineering.MarketBrandExample;
import com.cskaoyan.bean.reverseengineering.MarketRegion;
import com.cskaoyan.bean.reverseengineering.MarketRegionExample;
import com.cskaoyan.bean.bo.brandbo.BrandCreateBO;
import com.cskaoyan.bean.bo.brandbo.BrandDeleteBO;
import com.cskaoyan.bean.vo.RegionListVO;
import com.cskaoyan.bean.vo.brandvo.BrandListVO;
import com.cskaoyan.mapper.MarketBrandMapper;
import com.cskaoyan.mapper.MarketRegionMapper;
import com.cskaoyan.wx.bean.vo.brand.WxBrandListDetailVO;
import com.cskaoyan.wx.bean.vo.brand.WxBrandListVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 创建日期: 2022/05/05 17:20
 *
 * @author zhangfuqiang
 */

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    MarketRegionMapper marketRegionMapper;

    @Autowired
    MarketBrandMapper marketBrandMapper;

    @Override
    public RegionListVO getRegionList(RegionListVO regionListVO) {

        RegionListVO.DataEntity data = regionListVO.getData();

        MarketRegionExample marketRegionExample = new MarketRegionExample();
        MarketRegionExample.Criteria criteria = marketRegionExample.createCriteria();
        MarketRegionExample.Criteria criteria1 = criteria.andTypeEqualTo((byte) 1);
        //创建所有的省
        List<MarketRegion> provinces = marketRegionMapper.selectByExample(marketRegionExample);

        for (int i = 0; i < provinces.size(); i++) {
            //把每一个省份的数据赋值给data
            List<RegionListVO.DataEntity.ListEntity> list = data.addList();
            MarketRegion province = provinces.get(i);
            RegionListVO.DataEntity.ListEntity listEntity = list.get(i);

            Integer code = province.getCode();
            listEntity.setCode(code);
            listEntity.setId(province.getId());
            listEntity.setType(province.getType());
            listEntity.setName(province.getName());


            //查询对应省的市
            MarketRegionExample marketRegionExample2 = new MarketRegionExample();
            MarketRegionExample.Criteria criteria2 = marketRegionExample2.createCriteria();
            MarketRegionExample.Criteria criteria21 = criteria2.andCodeBetween(code, code + 10000);
            MarketRegionExample.Criteria criteria22 = criteria2.andTypeEqualTo((byte) 2);

            List<MarketRegion> citys = marketRegionMapper.selectByExample(marketRegionExample2);

            for (int j = 0; j < citys.size(); j++) {
                //把市的数据赋值给children
                List<RegionListVO.DataEntity.ListEntity.ChildrenEntity> children = listEntity.addChildren();
                RegionListVO.DataEntity.ListEntity.ChildrenEntity childrenEntity = children.get(j);
                MarketRegion city = citys.get(j);

                Integer code2 = city.getCode();
                childrenEntity.setCode(code2);
                childrenEntity.setId(city.getId());
                childrenEntity.setName(city.getName());
                childrenEntity.setType(city.getType());


                //查询对应市的县
                MarketRegionExample marketRegionExample3 = new MarketRegionExample();
                MarketRegionExample.Criteria criteria3 = marketRegionExample3.createCriteria();
                MarketRegionExample.Criteria criteria31 = criteria3.andCodeBetween(code2, code2 + 100);
                MarketRegionExample.Criteria criteria32 = criteria3.andTypeEqualTo((byte) 3);

                List<MarketRegion> counties = marketRegionMapper.selectByExample(marketRegionExample3);

                for (int k = 0; k < counties.size(); k++) {
                    //把县的数据赋值给children2
                    List<RegionListVO.DataEntity.ListEntity.ChildrenEntity.ChildrenEntity2> children2 = childrenEntity.addChildren();
                    RegionListVO.DataEntity.ListEntity.ChildrenEntity.ChildrenEntity2 childrenEntity2 = children2.get(k);
                    MarketRegion county = counties.get(k);

                    childrenEntity2.setCode(county.getCode());
                    childrenEntity2.setId(county.getId());
                    childrenEntity2.setName(county.getName());
                    childrenEntity2.setType(county.getType());
                }


            }

        }


        return regionListVO;
    }

    @Override
    public BrandListVO list(Integer page, Integer limit, String sort, String order, Integer id, String name) {

        MarketBrandExample marketBrandExample = new MarketBrandExample();
        marketBrandExample.setOrderByClause(sort + " " + order);
        MarketBrandExample.Criteria criteria = marketBrandExample.createCriteria();
        if (id != null) {
            criteria.andIdEqualTo(id);
        }
        criteria.andNameLike("%" + ((name == null) ? "" : name) + "%");
        List<MarketBrand> marketBrands = marketBrandMapper.selectByExample(marketBrandExample);

        //若删除状态为true则从链表中去除
        Iterator<MarketBrand> iterator = marketBrands.iterator();
        while (iterator.hasNext()){
            if(iterator.next().getDeleted()){
                iterator.remove();
            }
        }

        PageInfo<MarketBrand> pageInfo = new PageInfo<>(marketBrands);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BrandListVO brandListVO = new BrandListVO(total, pages, limit, page, marketBrands);
        return brandListVO;


    }

    @Override
    public Integer delete(BrandDeleteBO brandDeleteBO) {
        MarketBrand marketBrand = new MarketBrand();
        marketBrand.setDeleted(true);
        MarketBrandExample marketBrandExample = new MarketBrandExample();
        MarketBrandExample.Criteria criteria = marketBrandExample.createCriteria();
        criteria.andIdEqualTo(brandDeleteBO.getId());
        Integer i = marketBrandMapper.updateByExampleSelective(marketBrand, marketBrandExample);
        return i;
    }

    @Override
    public Integer update(BrandDeleteBO brandDeleteBO) {

        MarketBrand marketBrand = new MarketBrand();
        marketBrand.setDesc(brandDeleteBO.getDesc());
        marketBrand.setName(brandDeleteBO.getName());
        marketBrand.setPicUrl(brandDeleteBO.getPicUrl());
        marketBrand.setFloorPrice(brandDeleteBO.getFloorPrice());


        MarketBrandExample marketBrandExample = new MarketBrandExample();
        MarketBrandExample.Criteria criteria = marketBrandExample.createCriteria();
        criteria.andIdEqualTo(brandDeleteBO.getId());
        Integer i = marketBrandMapper.updateByExampleSelective(marketBrand, marketBrandExample);
        return i;
    }

    @Override
    public BrandDeleteBO create(BrandCreateBO brandCreateBO) {
        String desc = brandCreateBO.getDesc();
        BigDecimal floorPrice = brandCreateBO.getFloorPrice();
        String name = brandCreateBO.getName();
        String picUrl = brandCreateBO.getPicUrl();

        MarketBrand marketBrand = new MarketBrand();
        Date date = new Date();

        marketBrand.setName(name);
        marketBrand.setDesc(desc);
        marketBrand.setFloorPrice(floorPrice);
        marketBrand.setPicUrl(picUrl);
        marketBrand.setAddTime(date);
        marketBrand.setDeleted(false);
        marketBrand.setSortOrder((byte) 0);
        marketBrand.setUpdateTime(date);
        int i = marketBrandMapper.insertSelective(marketBrand);


        //把刚刚插入的数据查询出来
        BrandDeleteBO brandDeleteBO = new BrandDeleteBO();

        MarketBrandExample marketBrandExample = new MarketBrandExample();
        MarketBrandExample.Criteria criteria = marketBrandExample.createCriteria();
        criteria.andNameLike("%" + ((name == null) ? "" : name) + "%");
        //criteria.andDescLike("%"+((desc == null) ? "" : desc) +"%");
        //使用上述语句查询出错，不知道原因
        List<MarketBrand> marketBrands = marketBrandMapper.selectByExample(marketBrandExample);

        MarketBrand marketBrandNew = marketBrands.get(0);

        brandDeleteBO.setId(marketBrandNew.getId());
        brandDeleteBO.setAddTime(marketBrand.getAddTime());
        brandDeleteBO.setUpdateTime(marketBrand.getUpdateTime());
        brandDeleteBO.setDesc(desc);
        brandDeleteBO.setFloorPrice(floorPrice);
        brandDeleteBO.setName(name);
        brandDeleteBO.setPicUrl(picUrl);

        return brandDeleteBO;
    }

    @Override
    public WxBrandListVO queryAllBrands(Integer page, Integer limit) {
        String sort = "update_time";
        String order = "desc";

        PageHelper.startPage(page, limit);

        MarketBrandExample marketBrandExample = new MarketBrandExample();
        MarketBrandExample.Criteria criteria = marketBrandExample.createCriteria();
        marketBrandExample.setOrderByClause(sort + " " + order);

        criteria.andDeletedEqualTo(false);

        List<MarketBrand> marketBrands = marketBrandMapper.selectByExample(marketBrandExample);

        ArrayList<WxBrandListDetailVO> wxBrandListDetailVOS = new ArrayList<>();

        for (int i = 0; i < marketBrands.size(); i++) {
            WxBrandListDetailVO wxBrandListDetailVO = new WxBrandListDetailVO(marketBrands.get(i).getDesc(),
                    marketBrands.get(i).getFloorPrice(), marketBrands.get(i).getId(), marketBrands.get(i).getName(),
                    marketBrands.get(i).getPicUrl());

            wxBrandListDetailVOS.add(wxBrandListDetailVO);

        }

        PageInfo<MarketBrand> pageInfo = new PageInfo<>(marketBrands);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        return new WxBrandListVO(total, pages, limit, page, wxBrandListDetailVOS);
    }

    @Override
    public MarketBrand queryBrandDetail(Integer id) {
        MarketBrand marketBrand = marketBrandMapper.selectByPrimaryKey(id);
        return marketBrand;
    }
}
