package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketAd;
import com.cskaoyan.bean.bo.adbo.AdListBO;
import com.cskaoyan.bean.vo.promote.adVO.AdData;

import java.util.List;

/**
 * 创建日期: 2022/05/05 19:48
 *
 * @author yangfan
 */
public interface AdService {


    MarketAd create(MarketAd marketAd);

    MarketAd update(MarketAd marketAd);

    void delete(MarketAd marketAd);

    AdData list(AdListBO adListBO);
}
