package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketStorage;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.StorageListVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * 创建日期: 2022/05/05 21:58
 *
 * @author yangfan
 */
public interface StorageService {
    MarketStorage insert(MultipartFile multipartFile, HttpServletRequest request);

    StorageListVO query(String name, String key, BaseParam param);

    int storageDelete(MarketStorage marketStorage);

    int storageUpdate(MarketStorage marketStorage);
}
