package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketCoupon;
import com.cskaoyan.bean.reverseengineering.MarketCouponExample;
import com.cskaoyan.bean.bo.couponbo.CouponBO;
import com.cskaoyan.bean.reverseengineering.MarketCouponUser;
import com.cskaoyan.bean.reverseengineering.MarketCouponUserExample;
import com.cskaoyan.bean.vo.promote.couponVO.CouponData;
import com.cskaoyan.bean.vo.promote.couponVO.CouponListuserVO;
import com.cskaoyan.mapper.MarketCouponMapper;
import com.cskaoyan.mapper.MarketCouponUserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 创建日期: 2022/05/06 16:14
 *
 * @author yangfan
 */
@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    MarketCouponMapper marketCouponMapper;

    @Autowired
    MarketCouponUserMapper marketCouponUserMapper;

    // http://182.92.235.201:8083/admin/coupon/list?page=1&limit=20&sort=add_time&order=desc
    // http://182.92.235.201:8083/admin/coupon/list?page=1&limit=20&name=1&type=0&status=0&sort=add_time&order=desc
    @Override
    public CouponData list(CouponBO couponBO) {

        MarketCouponExample example = new MarketCouponExample();
        MarketCouponExample.Criteria criteria = example.createCriteria();

        // 排序规则
        example.setOrderByClause(couponBO.getSort() + " " + couponBO.getOrder());

        // 条件查询
        if (couponBO.getName() != null) {
            criteria.andNameLike("%" + couponBO.getName() + "%");
        }
        if (couponBO.getType() != null) {
            criteria.andTypeEqualTo(couponBO.getType().shortValue());
        }
        if (couponBO.getStatus() != null) {
            criteria.andStatusEqualTo(couponBO.getStatus().shortValue());
        }
        // 分页查询
        PageHelper.startPage(couponBO.getPage(),couponBO.getLimit());
        List<MarketCoupon> list = marketCouponMapper.selectByExample(example);

        PageInfo<MarketCoupon> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();


        CouponData couponData = new CouponData();
        couponData.setLimit(couponBO.getLimit());
        couponData.setPage(couponBO.getPage());
        couponData.setPages(pages);
        couponData.setTotal((int) total);
        couponData.setList(list);


        return couponData;
    }


    @Override
    public MarketCoupon create(MarketCoupon marketCoupon) {

        marketCoupon.setAddTime(new Date());
        marketCoupon.setUpdateTime(new Date());

        marketCouponMapper.insert(marketCoupon);
        return marketCoupon;
    }


    @Override
    public MarketCoupon read(Integer id) {

        MarketCoupon marketCoupon = marketCouponMapper.selectByPrimaryKey(id);

        return marketCoupon;
    }

    @Override
    public MarketCoupon update(MarketCoupon marketCoupon) {

        marketCoupon.setUpdateTime(new Date());

        MarketCouponExample example = new MarketCouponExample();
        example.createCriteria()
                .andIdEqualTo(marketCoupon.getId());

        marketCouponMapper.updateByExample(marketCoupon,example);

        return marketCoupon;
    }

    @Override
    public void delete(MarketCoupon marketCoupon) {

        marketCouponMapper.deleteByPrimaryKey(marketCoupon.getId());
    }

    // http://182.92.235.201:8083/admin/coupon/listuser?
    // page=1&limit=20&couponId=2&userId=1&status=0&sort=add_time&order=desc
    @Override
    public CouponListuserVO listuser(CouponBO couponBO) {

        MarketCouponUserExample example = new MarketCouponUserExample();
        MarketCouponUserExample.Criteria criteria = example.createCriteria();

        if (couponBO.getCouponId() != null) {
            criteria.andCouponIdEqualTo(couponBO.getCouponId());
        }
        if (couponBO.getStatus() != null) {
            criteria.andStatusEqualTo(couponBO.getStatus().shortValue());
        }
        List<MarketCouponUser> list = marketCouponUserMapper.selectByExample(example);

        PageInfo<MarketCouponUser> pageInfo = new PageInfo<>(list);
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();

        CouponListuserVO couponListuserVO = new CouponListuserVO();
        couponListuserVO.setLimit(couponBO.getLimit());
        couponListuserVO.setPage(couponBO.getPage());
        couponListuserVO.setList(list);
        couponListuserVO.setPages(pages);
        couponListuserVO.setTotal((int) total);
        return couponListuserVO;
    }
}
