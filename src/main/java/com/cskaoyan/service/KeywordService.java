package com.cskaoyan.service;

import com.cskaoyan.bean.bo.keywordbo.KeywordCreateBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordDeleteBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordListBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordUpdateBO;
import com.cskaoyan.bean.vo.keywordvo.KeywordCreateVO;
import com.cskaoyan.bean.vo.keywordvo.KeywordListVO;
import com.cskaoyan.bean.vo.keywordvo.KeywordUpdateVO;

public interface KeywordService {

    KeywordListVO query(KeywordListBO keywordListBO);

    KeywordCreateVO create(KeywordCreateBO keywordCreateBO);

    KeywordUpdateVO update(KeywordUpdateBO keywordUpdateBO);

    void delete(KeywordDeleteBO keywordDeleteBO);
}
