package com.cskaoyan.service;

import com.cskaoyan.bean.bo.issue.IssueCreateBO;
import com.cskaoyan.bean.bo.issue.IssueDeleteBO;
import com.cskaoyan.bean.bo.issue.IssueListBO;
import com.cskaoyan.bean.bo.issue.IssueUpdateBO;
import com.cskaoyan.bean.reverseengineering.MarketIssue;
import com.cskaoyan.bean.reverseengineering.MarketIssueExample;
import com.cskaoyan.bean.vo.issue.IssueCreateVO;
import com.cskaoyan.bean.vo.issue.IssueListIntermediateVO;
import com.cskaoyan.bean.vo.issue.IssueListVO;
import com.cskaoyan.bean.vo.issue.IssueUpdateVO;
import com.cskaoyan.mapper.MarketIssueMapper;
import com.cskaoyan.wx.bean.vo.issue.IssuesInWxQueryVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author tangwei
 * @since 2022/05/06 22:44
 */
@Service
public class IssueServiceImpl implements IssueService {
    @Autowired
    MarketIssueMapper marketIssueMapper;

    @Override
    public IssueListVO queryAllIssues(IssueListBO issueListBO) {
        Integer hasDeleted = 0;

        Integer page = issueListBO.getPage();
        Integer limit = issueListBO.getLimit();
        String sort = issueListBO.getSort();
        String order = issueListBO.getOrder();

        String question = issueListBO.getQuestion();

        PageHelper.startPage(page, limit);
        // List<IssueListIntermediateVO> issueListIntermediateVOList = marketIssueMapper.selectAllIssues(sort, order, hasDeleted);

        List<IssueListIntermediateVO> issueListIntermediateVOList = marketIssueMapper.selectAllIssues(sort, order, hasDeleted, question);

        PageInfo<IssueListIntermediateVO> pageInfo = new PageInfo<>(issueListIntermediateVOList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        return new IssueListVO(total, pages, limit, page, issueListIntermediateVOList);

    }

    @Override
    public IssueCreateVO createIssue(IssueCreateBO issueCreateBO) {
        Date date = new Date();
        issueCreateBO.setAddTime(date);
        issueCreateBO.setUpdateTime(date);

        marketIssueMapper.insertOneIssue(issueCreateBO);

        Integer id = issueCreateBO.getId();

        IssueCreateVO issueCreateVO = marketIssueMapper.selectIssueById(id);

        return issueCreateVO;
    }

    @Override
    public IssueUpdateVO updateIssue(IssueUpdateBO issueUpdateBO) {
        Date date = new Date();
        issueUpdateBO.setUpdateTime(date);

        marketIssueMapper.updateOneIssue(issueUpdateBO);
        IssueUpdateVO issueUpdateVO = marketIssueMapper.selectOneIssueById(issueUpdateBO.getId());
        return issueUpdateVO;
    }

    @Override
    public void deleteIssue(IssueDeleteBO issueDeleteBO) {
        Integer hasDeleted = 1;

        marketIssueMapper.deleteOneIssueById(issueDeleteBO.getId(), hasDeleted);
    }

    @Override
    public IssuesInWxQueryVO queryAllIssuesInWx(Integer page, Integer limit) {

        String sort = "add_time";
        String order = "desc";

        MarketIssueExample marketIssueExample = new MarketIssueExample();
        MarketIssueExample.Criteria criteria = marketIssueExample.createCriteria();

        criteria.andDeletedEqualTo(false);

        marketIssueExample.setOrderByClause(sort + " " + order);

        PageHelper.startPage(page, limit);

        List<MarketIssue> marketIssues = marketIssueMapper.selectByExample(marketIssueExample);

        PageInfo<MarketIssue> pageInfo = new PageInfo<>(marketIssues);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        return new IssuesInWxQueryVO(total, pages, limit, page, marketIssues);
    }
}