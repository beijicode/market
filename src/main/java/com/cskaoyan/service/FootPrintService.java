package com.cskaoyan.service;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 17:01
 */
public interface FootPrintService {

    void insertFootPrint(Integer id, String username);
}
