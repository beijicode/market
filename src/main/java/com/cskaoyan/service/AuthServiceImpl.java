package com.cskaoyan.service;

import com.cskaoyan.bean.login_demo.InfoData;
import com.cskaoyan.bean.reverseengineering.*;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.mapper.MarketPermissionMapper;
import com.cskaoyan.mapper.MarketRoleMapper;
import com.cskaoyan.mapper.PermissionManageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charadic&beiji
 * @since 2022/05/09 16:18
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    MarketAdminMapper marketAdminMapper;

    @Autowired
    MarketRoleMapper marketRoleMapper;

    @Autowired
    MarketPermissionMapper marketPermissionMapper;

    @Autowired
    PermissionManageMapper permissionManageMapper;

    @Override
    public MarketAdmin login(String username) {
        MarketAdmin marketAdmin = marketAdminMapper.queryByUsername(username);
        return marketAdmin;
    }

    @Override
    public InfoData authInfo(String username) {
        InfoData infoData = new InfoData();
        // name
        infoData.setName(username);
        // avatar
        MarketAdminExample marketAdminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        MarketAdmin marketAdmin = marketAdminMapper.selectByExample(marketAdminExample).get(0);
        infoData.setAvatar(marketAdmin.getAvatar());
        // roles
        Integer[] roleIds = marketAdmin.getRoleIds();
        MarketRoleExample marketRoleExample = new MarketRoleExample();
        MarketRoleExample.Criteria criteria1 = marketRoleExample.createCriteria();
        ArrayList<Integer> integers = new ArrayList<>();
        for (Integer roleId : roleIds) {
            if(roleId == 1){
                ArrayList<String> roles = new ArrayList<>();
                roles.add("超级管理员");
                infoData.setRoles(roles);
                List<String> perms = new ArrayList<>();
                perms.add("*");
                infoData.setPerms(perms);
                return infoData;
            }
            integers.add(roleId);
        }
        criteria1.andIdIn(integers);
        List<MarketRole> marketRoles = marketRoleMapper.selectByExample(marketRoleExample);
        ArrayList<String> strings = new ArrayList<>();
        for (MarketRole marketRole : marketRoles) {
            strings.add(marketRole.getName());
        }
        infoData.setRoles(strings);
        // perms
        MarketPermissionExample marketPermissionExample = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria2 = marketPermissionExample.createCriteria();
        criteria2.andRoleIdIn(integers);
        List<MarketPermission> marketPermissions = marketPermissionMapper.selectByExample(marketPermissionExample);
        List<String> list = new ArrayList<>();
        for (MarketPermission marketPermission : marketPermissions) {
            list.add(marketPermission.getPermission());
        }
        PermissionManageExample permissionManageExample = new PermissionManageExample();
        PermissionManageExample.Criteria criteria3 = permissionManageExample.createCriteria();
        criteria3.andNameIn(list);
        List<PermissionManage> permissionManages = permissionManageMapper.selectByExample(permissionManageExample);
        List<String> strings1 = new ArrayList<>();
        for (PermissionManage permissionManage : permissionManages) {
            strings1.add(permissionManage.getApi());
        }
        infoData.setPerms(strings1);


        return infoData;
    }
}
