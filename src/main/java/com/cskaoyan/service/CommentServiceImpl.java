package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketComment;
import com.cskaoyan.bean.reverseengineering.MarketCommentExample;
import com.cskaoyan.bean.bo.goodsbo.CommentBO;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.bean.vo.goodsvo.CommentListVO;
import com.cskaoyan.mapper.MarketCommentMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.wx.bean.bo.commentbo.WxCommentBO;
import com.cskaoyan.wx.bean.bo.commentbo.WxCommentPutBO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsCountVO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsUserInfo;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsUserListsVO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    MarketCommentMapper marketCommentMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public CommentListVO list(CommentBO commentBO) {
        Integer limit = commentBO.getLimit();
        Integer page = commentBO.getPage();
        String sort = commentBO.getSort();
        String order = commentBO.getOrder();

        PageHelper.startPage(page, limit);

        MarketCommentExample marketCommentExample = new MarketCommentExample();
        marketCommentExample.setOrderByClause(sort + " " + order);
        MarketCommentExample.Criteria criteria = marketCommentExample.createCriteria();
        if (commentBO.getUserId() != null && !"".equals(commentBO.getUserId())) {
            criteria.andUserIdEqualTo(Integer.parseInt(commentBO.getUserId()));
        }
        if (commentBO.getValueId() != null && !"".equals(commentBO.getValueId())) {
            criteria.andValueIdEqualTo(Integer.parseInt(commentBO.getValueId()));

        }
        criteria.andDeletedEqualTo(false);
        List<MarketComment> list = marketCommentMapper.selectByExample(marketCommentExample);

        PageInfo<MarketComment> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        CommentListVO commentListVO = new CommentListVO(limit, page, pages, (int) total, list);

        return commentListVO;
    }

    @Override
    public void delete(Map comment) {
        MarketComment marketComment = new MarketComment();
        marketComment.setId((Integer) comment.get("id"));
        marketComment.setDeleted(true);
        marketCommentMapper.updateByPrimaryKeySelective(marketComment);
    }



    /**
     * 小程序评论模块 显示评论列表
     * @param wxCommentBO
     * @return
     */
    @Override
    public WxCommentsVO commentList(WxCommentBO wxCommentBO) {

        Integer limit = wxCommentBO.getLimit();
        Integer page = wxCommentBO.getPage();

        PageHelper.startPage(page, limit);

        MarketCommentExample marketCommentExample = new MarketCommentExample();
        MarketCommentExample.Criteria criteria = marketCommentExample.createCriteria();
        criteria.andValueIdEqualTo(wxCommentBO.getValueId());
        criteria.andTypeEqualTo(wxCommentBO.getType());
        criteria.andDeletedEqualTo(false);
        if (wxCommentBO.getShowType() == 1){
            criteria.andHasPictureEqualTo(true);
        }
        List<MarketComment> marketCommentList = marketCommentMapper.selectByExample(marketCommentExample);

        PageInfo<MarketComment> pageInfo = new PageInfo<>(marketCommentList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        List<WxCommentsUserListsVO> wxCommentsUserListsVOS = new ArrayList<>();

        for (MarketComment marketComment : marketCommentList) {
            MarketUser marketUser = marketUserMapper.selectByPrimaryKey(marketComment.getUserId());
            WxCommentsUserInfo wxCommentsUserInfo = new WxCommentsUserInfo(marketUser.getNickname(),marketUser.getAvatar());
            WxCommentsUserListsVO wxCommentsUserListsVO = new WxCommentsUserListsVO(wxCommentsUserInfo, marketComment.getAddTime(), marketComment.getPicUrls(), marketComment.getAdminContent(), marketComment.getContent());
            wxCommentsUserListsVOS.add(wxCommentsUserListsVO);
        }

        return new WxCommentsVO((int)total,pages,limit,wxCommentsUserListsVOS);

    }

    /**
     * 小程序评论模块 显示评论数量
     * @param valueId
     * @param type
     * @return
     */
    @Override
    public WxCommentsCountVO commentCount(Integer valueId, Byte type) {

        // 计算出有图评论数
        // MarketCommentExample marketCommentExampleWithPic = new MarketCommentExample();
        // MarketCommentExample.Criteria criteriaWithPic = marketCommentExampleWithPic.createCriteria();
        // criteriaWithPic.andValueIdEqualTo(valueId);
        // criteriaWithPic.andTypeEqualTo(type);
        // List<MarketComment> marketCommentListWithPic = marketCommentMapper.selectByExample(marketCommentExampleWithPic);
        // int hasPicCount = marketCommentListWithPic.size();

        // 计算出全部评论数
        MarketCommentExample marketCommentExample = new MarketCommentExample();
        MarketCommentExample.Criteria criteria = marketCommentExample.createCriteria();
        criteria.andValueIdEqualTo(valueId);
        criteria.andTypeEqualTo(type);
        List<MarketComment> marketCommentList = marketCommentMapper.selectByExample(marketCommentExample);
        int allcount = marketCommentList.size();

        criteria.andHasPictureEqualTo(true);
        List<MarketComment> marketCommentListWithPic = marketCommentMapper.selectByExample(marketCommentExample);
        int hasPicCount = marketCommentListWithPic.size();

        return new WxCommentsCountVO(hasPicCount, allcount);
    }

    /**
     * 小程序评论模块 提交评论
     * @param wxCommentPutBO
     * @return
     */
    @Override
    public MarketComment commentPut(WxCommentPutBO wxCommentPutBO) {

        MarketComment marketComment = new MarketComment();

        marketComment.setType(wxCommentPutBO.getType());
        marketComment.setValueId(Integer.parseInt(wxCommentPutBO.getValueId()));
        marketComment.setContent(wxCommentPutBO.getContent());
        marketComment.setStar(wxCommentPutBO.getStar());
        marketComment.setHasPicture(wxCommentPutBO.getHasPicture());
        marketComment.setPicUrls(wxCommentPutBO.getPicUrls());
        marketComment.setUpdateTime(new Date());
        marketComment.setAddTime(new Date());
        // TODO: userId写死 后面再改
        marketComment.setUserId(1);

        int affectedRows = marketCommentMapper.insertSelective(marketComment);

        if (affectedRows != 1){

            return null;
        }

        return marketComment;
    }
}
