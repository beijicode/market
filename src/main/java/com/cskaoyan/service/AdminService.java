package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketAdmin;
import com.cskaoyan.bean.bo.AdminCreateBO;
import com.cskaoyan.bean.bo.AdminDeleteBO;
import com.cskaoyan.bean.bo.AdminUpdateBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.AdminListVO;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/5 20:25
 */
public interface AdminService {


    AdminListVO query(BaseParam param, String username);

    MarketAdmin adminCreate(AdminCreateBO adminCreateBO);

    int selectAdminByUsername(String username);

    int adminUpdate(AdminUpdateBO adminUpdateBO);

    MarketAdmin SelectAdminById(Integer id);

    int adminDelete(AdminDeleteBO adminDeleteBO);
}
