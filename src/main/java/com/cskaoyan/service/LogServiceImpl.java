package com.cskaoyan.service;


import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.reverseengineering.MarketLog;
import com.cskaoyan.bean.reverseengineering.MarketLogExample;
import com.cskaoyan.bean.vo.LogListVO;
import com.cskaoyan.mapper.MarketLogMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 23:58
 */
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    MarketLogMapper marketLogMapper;

    @Override
    public LogListVO query(BaseParam param, String name) {

        System.out.println(name);
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        // 分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        MarketLogExample marketLogExample = new MarketLogExample();
        MarketLogExample.Criteria criteria = marketLogExample.createCriteria();

        marketLogExample.setOrderByClause(sort+" "+order);
        if (name != null && !"".equals(name)){
            criteria.andAdminLike("%" + name + "%");
        }

        List<MarketLog> marketLogs = marketLogMapper.selectByExample(marketLogExample);
        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketLog> pageInfo = new PageInfo<>(marketLogs);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return LogListVO.data(total,pages,limit,page,marketLogs);
    }

    @Override
    public void insertLog(String ip, String username,String action) {

        MarketLog marketLog = new MarketLog();
        marketLog.setAdmin(username);
        marketLog.setIp(ip);
        marketLog.setAddTime(new Date());
        marketLog.setDeleted(false);
        marketLog.setAction(action);
        marketLog.setStatus(true);
        marketLog.setType(1);

        marketLogMapper.insertSelective(marketLog);
    }
}
