package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketFootprint;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.mapper.MarketFootprintMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 17:02
 */
@Service
public class FootPrintServiceImpl implements FootPrintService {

    @Autowired
    MarketFootprintMapper marketFootprintMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public void insertFootPrint(Integer goodsId, String username) {

        // 通过username获取userid
        MarketUser marketUser = marketUserMapper.queryByUsername(username);

        Integer userId = marketUser.getId();

        MarketFootprint marketFootprint = new MarketFootprint();
        marketFootprint.setUserId(userId);
        marketFootprint.setGoodsId(goodsId);
        marketFootprint.setAddTime(new Date());
        marketFootprint.setUpdateTime(new Date());
        marketFootprint.setDeleted(false);

        marketFootprintMapper.insertSelective(marketFootprint);
    }
}
