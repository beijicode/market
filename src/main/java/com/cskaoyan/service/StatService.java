package com.cskaoyan.service;

import com.cskaoyan.bean.vo.statvo.GoodStatVO;
import com.cskaoyan.bean.vo.statvo.OrderStatVO;
import com.cskaoyan.bean.vo.statvo.UserStatVO;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public interface StatService {

    UserStatVO getUser();

    GoodStatVO getGoods();

    OrderStatVO getOrders();

}
