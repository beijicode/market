package com.cskaoyan.service;

import com.cskaoyan.bean.po.GoodsStatPO;
import com.cskaoyan.bean.po.OrderStatPO;
import com.cskaoyan.bean.po.UserStatPO;
import com.cskaoyan.bean.vo.statvo.GoodStatVO;
import com.cskaoyan.bean.vo.statvo.OrderStatVO;
import com.cskaoyan.bean.vo.statvo.UserStatVO;
import com.cskaoyan.mapper.MarketOrderGoodsMapper;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
@Service
public class StatServiceImpl implements StatService {
    @Autowired
    MarketUserMapper marketUserMapper;

    @Autowired
    MarketOrderGoodsMapper marketOrderGoodsMapper;

    @Autowired
    MarketOrderMapper marketOrderMapper;

    @Override
    public UserStatVO getUser() {
        UserStatVO userStatVO = new UserStatVO();
        //给columns赋值
        List<String> columns = userStatVO.getColumns();
        columns.add("day");
        columns.add("users");

        //给Rows赋值
        List<UserStatPO> userStatPOs = marketUserMapper.getUserCount();
        userStatVO.setRows(userStatPOs);

        return userStatVO;
    }


    @Override
    public GoodStatVO getGoods() {
        //给columns赋值
        GoodStatVO goodStatVO = new GoodStatVO();
        List<String> columns = goodStatVO.getColumns();
        columns.add("day");
        columns.add("orders");
        columns.add("products");
        columns.add("amount");

        //给Rows赋值
        //convert(varchar(10),CreateTime,120) as CreateTime 这个限制为一天时间的语句没有搞出来
        List<GoodsStatPO> goodsStatPOs = marketOrderGoodsMapper.getOrderCount();
        goodStatVO.setRows(goodsStatPOs);


        return goodStatVO;
    }


    @Override
    public OrderStatVO getOrders() {
        //给columns赋值
        OrderStatVO orderStatVO = new OrderStatVO();
        List<String> columns = orderStatVO.getColumns();
        columns.add("day");
        columns.add("orders");
        columns.add("customers");
        columns.add("amount");
        columns.add("pcr");

        //给Rows赋值
        //convert(varchar(10),CreateTime,120) as CreateTime 这个限制为一天时间的语句没有搞出来
        //去重并计数
        List<OrderStatPO> orderStatPOs = marketOrderMapper.getOrderCount();

        for (int i = 0; i < orderStatPOs.size(); i++) {
            Double pcr = orderStatPOs.get(i).getAmount()/orderStatPOs.get(i).getCustomers();
            orderStatPOs.get(i).setPcr(pcr);
        }

        orderStatVO.setRows(orderStatPOs);

        return orderStatVO;
    }
}
