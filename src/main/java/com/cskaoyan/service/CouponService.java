package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketCoupon;
import com.cskaoyan.bean.bo.couponbo.CouponBO;
import com.cskaoyan.bean.vo.promote.couponVO.CouponData;
import com.cskaoyan.bean.vo.promote.couponVO.CouponListuserVO;

import java.util.List;

public interface CouponService {
    CouponData list(CouponBO couponBO);

    MarketCoupon create(MarketCoupon marketCoupon);

    MarketCoupon read(Integer id);

    MarketCoupon update(MarketCoupon marketCoupon);

    void delete(MarketCoupon marketCoupon);

    CouponListuserVO listuser(CouponBO couponBO);
}
