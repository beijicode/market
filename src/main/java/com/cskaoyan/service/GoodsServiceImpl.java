package com.cskaoyan.service;

import com.cskaoyan.bean.bo.GoodsCreateBO;
import com.cskaoyan.bean.bo.goodsbo.GoodsListBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.reverseengineering.*;
import com.cskaoyan.bean.vo.goodsvo.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.wx.bean.WxGoodsListData;
import com.cskaoyan.wx.bean.vo.goods.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Charadic&beiji
 * @since 2022/05/06 11:30
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Autowired
    MarketGoodsAttributeMapper marketGoodsAttributeMapper;

    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Autowired
    MarketGoodsProductMapper marketGoodsProductMapper;

    @Autowired
    MarketGoodsSpecificationMapper marketGoodsSpecificationMapper;

    @Autowired
    MarketBrandMapper marketBrandMapper;

    @Autowired
    MarketAdMapper marketAdMapper;

    @Autowired
    MarketCouponMapper marketCouponMapper;

    @Autowired
    MarketTopicMapper marketTopicMapper;

    @Autowired
    MarketIssueMapper marketIssueMapper;

    @Autowired
    MarketCollectMapper marketCollectMapper;

    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Autowired
    MarketCommentMapper marketCommentMapper;

    @Override
    public GoodsListVO list(GoodsListBO goodsListBO) {
        Integer page = goodsListBO.getPage();
        Integer limit = goodsListBO.getLimit();
        String sort = goodsListBO.getSort();
        String order = goodsListBO.getOrder();

        if (limit == 0) {
            MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
            MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
            criteria.andDeletedEqualTo(false);
            List<MarketGoods> list = marketGoodsMapper.selectByExampleWithBLOBs(marketGoodsExample);
            GoodsListVO goodsListVO = new GoodsListVO(list.size(), 0, 0, 0, list);
            goodsListVO.setList(list);
            return goodsListVO;
        }

        PageHelper.startPage(page, limit);

        MarketGoodsExample marketGoodsExample = new MarketGoodsExample();

        marketGoodsExample.setOrderByClause(sort + " " + order);
        MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
        if (goodsListBO.getGoodsId() != null && !"".equals(goodsListBO.getGoodsId())) {
            criteria.andIdEqualTo(Integer.parseInt(goodsListBO.getGoodsId()));
        }
        if (goodsListBO.getGoodsSn() != null && !"".equals(goodsListBO.getGoodsSn())) {
            criteria.andGoodsSnEqualTo(goodsListBO.getGoodsSn());
        }
        criteria.andNameLike("%" + ((goodsListBO.getName() == null) ? "" : goodsListBO.getName()) + "%");
        criteria.andDeletedEqualTo(false);

        List<MarketGoods> list = marketGoodsMapper.selectByExampleWithBLOBs(marketGoodsExample);

        PageInfo<MarketGoods> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        GoodsListVO goodsListVO = new GoodsListVO((int) total, pages, limit, page, list);
        return goodsListVO;


    }

    @Override
    public void delete(Map goods) {
        MarketGoods marketGoods = new MarketGoods();
        marketGoods.setId((Integer) goods.get("id"));
        marketGoods.setDeleted(true);
        marketGoodsMapper.updateByPrimaryKeySelective(marketGoods);
    }

    @Override
    public GoodsDetailVO detail(Integer id) {
        GoodsDetailVO goodsDetailVO = new GoodsDetailVO();
        MarketGoods marketGoods = marketGoodsMapper.selectByPrimaryKey(id);
        goodsDetailVO.setGoods(marketGoods);

        MarketGoodsAttributeExample marketGoodsAttributeExample = new MarketGoodsAttributeExample();
        MarketGoodsAttributeExample.Criteria attributeExampleCriteria = marketGoodsAttributeExample.createCriteria();
        attributeExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGoodsAttribute> marketGoodsAttributes = marketGoodsAttributeMapper.selectByExample(marketGoodsAttributeExample);
        goodsDetailVO.setAttributes(marketGoodsAttributes);

        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey(marketGoods.getCategoryId());
        Integer[] integers = new Integer[]{marketCategory.getPid(), marketCategory.getId()};
        goodsDetailVO.setCategoryIds(integers);

        MarketGoodsProductExample marketGoodsProductExample = new MarketGoodsProductExample();
        MarketGoodsProductExample.Criteria productExampleCriteria = marketGoodsProductExample.createCriteria();
        productExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGoodsProduct> marketGoodsProducts = marketGoodsProductMapper.selectByExample(marketGoodsProductExample);
        goodsDetailVO.setProducts(marketGoodsProducts);

        MarketGoodsSpecificationExample marketGoodsSpecificationExample = new MarketGoodsSpecificationExample();
        MarketGoodsSpecificationExample.Criteria goodsSpecificationExampleCriteria = marketGoodsSpecificationExample.createCriteria();
        goodsSpecificationExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGoodsSpecification> marketGoodsSpecifications = marketGoodsSpecificationMapper.selectByExample(marketGoodsSpecificationExample);
        goodsDetailVO.setSpecifications(marketGoodsSpecifications);

        return goodsDetailVO;
    }

    @Override
    public GoodsCatAndBrandVO catAndBrand() {
        GoodsCatAndBrandVO goodsCatAndBrandVO = new GoodsCatAndBrandVO();

        List<GoodsBrandVO> brandVOList = marketBrandMapper.selectAll();
        goodsCatAndBrandVO.setBrandList(brandVOList);

        List<GoodsCategoryVO> categoryList = marketCategoryMapper.selectAll();
        for (GoodsCategoryVO goodsCategoryVO : categoryList) {
            List<CategoryChildren> categoryChildren = marketCategoryMapper.selectChildren(goodsCategoryVO.getValue());
            goodsCategoryVO.setChildren(categoryChildren);
        }
        goodsCatAndBrandVO.setCategoryList(categoryList);

        return goodsCatAndBrandVO;

    }

    @Override
    public void create(GoodsCreateBO goodsCreateBO) {
        MarketGoods goods = goodsCreateBO.getGoods();
        goods.setAddTime(new Date());
        goods.setUpdateTime(new Date());
        marketGoodsMapper.insertSelective(goods);
        Integer id = goods.getId();

        List<MarketGoodsAttribute> attributes = goodsCreateBO.getAttributes();
        for (MarketGoodsAttribute attribute : attributes) {
            attribute.setGoodsId(id);
            attribute.setAddTime(new Date());
            attribute.setUpdateTime(new Date());
            marketGoodsAttributeMapper.insertSelective(attribute);
        }

        List<MarketGoodsSpecification> specifications = goodsCreateBO.getSpecifications();
        for (MarketGoodsSpecification specification : specifications) {
            specification.setGoodsId(id);
            specification.setAddTime(new Date());
            specification.setUpdateTime(new Date());
            marketGoodsSpecificationMapper.insertSelective(specification);
        }

        List<MarketGoodsProduct> products = goodsCreateBO.getProducts();
        for (MarketGoodsProduct product : products) {
            product.setId(id);
            product.setAddTime(new Date());
            product.setUpdateTime(new Date());
            marketGoodsProductMapper.insertSelective(product);
        }
    }

    @Override
    public void update(GoodsCreateBO goodsUpdateBO) {
        MarketGoods goods = goodsUpdateBO.getGoods();
        goods.setUpdateTime(new Date());
        marketGoodsMapper.updateByPrimaryKeySelective(goods);

        List<MarketGoodsAttribute> attributes = goodsUpdateBO.getAttributes();
        for (MarketGoodsAttribute attribute : attributes) {
            if (attribute.getId() == null) {
                attribute.setGoodsId(goods.getId());
                attribute.setAddTime(new Date());
                attribute.setUpdateTime(new Date());
                marketGoodsAttributeMapper.insertSelective(attribute);
            } else {
                attribute.setUpdateTime(new Date());
                marketGoodsAttributeMapper.updateByPrimaryKeySelective(attribute);
            }
        }

        List<MarketGoodsSpecification> specifications = goodsUpdateBO.getSpecifications();
        for (MarketGoodsSpecification specification : specifications) {
            specification.setUpdateTime(new Date());
            marketGoodsSpecificationMapper.updateByPrimaryKeySelective(specification);
        }

        List<MarketGoodsProduct> products = goodsUpdateBO.getProducts();
        for (MarketGoodsProduct product : products) {
            product.setUpdateTime(new Date());
            marketGoodsProductMapper.updateByPrimaryKeySelective(product);
        }

    }

    @Override
    public WxHomeIndexVO wxGoodsIndex() {
        WxHomeIndexVO wxHomeIndexVO = new WxHomeIndexVO();
        // 查询轮播图内的推广广告
        wxHomeIndexVO.setBanner(marketAdMapper.selectByExample(new MarketAdExample()));
        // 查询品牌
        wxHomeIndexVO.setBrandList(marketBrandMapper.selectByExample(new MarketBrandExample()));
        // 查询商品类别
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria();
        criteria.andPidEqualTo(0);
        wxHomeIndexVO.setChannel(marketCategoryMapper.selectByExample(marketCategoryExample));
        // 查询优惠券
        wxHomeIndexVO.setCouponList(marketCouponMapper.selectByExample(new MarketCouponExample()));
        // 商品展示
        //List<FloorGoodsVO> floorGoodsList = homeIndexVO.getFloorGoodsList();
        List<WxFloorGoodsVO> floorGoodsList = new ArrayList<>();
        List<MarketCategory> channel = wxHomeIndexVO.getChannel();
        for (MarketCategory marketCategory : channel) {
            WxFloorGoodsVO wxFloorGoodsVO = new WxFloorGoodsVO();
            wxFloorGoodsVO.setId(marketCategory.getId());
            wxFloorGoodsVO.setName(marketCategory.getName());
            // 根据大分类查询大分类下的小分类
            MarketCategoryExample marketCategoryExample1 = new MarketCategoryExample();
            MarketCategoryExample.Criteria criteria2 = marketCategoryExample1.createCriteria();
            criteria2.andPidEqualTo(marketCategory.getId());
            List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample1);
            ArrayList<Integer> integers = new ArrayList<>();
            for (MarketCategory category : marketCategories) {
                integers.add(category.getId());
            }
            // 根据小分类的id去商品表中查询对应的category_id
            MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
            MarketGoodsExample.Criteria criteria1 = marketGoodsExample.createCriteria();
            criteria1.andCategoryIdIn(integers);
            criteria1.andIsHotEqualTo(true);
            wxFloorGoodsVO.setGoodsList(marketGoodsMapper.selectByExample(marketGoodsExample));
            floorGoodsList.add(wxFloorGoodsVO);
        }
        wxHomeIndexVO.setFloorGoodsList(floorGoodsList);

        //List<MarketGoods> goodsList = marketGoodsMapper.selectByExample(new MarketGoodsExample());
        // TODO 团购
        wxHomeIndexVO.setGrouponList(new ArrayList<>());


        // 商品表中的热卖商品
        MarketGoodsExample marketGoodsExample1 = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria1 = marketGoodsExample1.createCriteria();
        criteria1.andIsHotEqualTo(true);
        wxHomeIndexVO.setHotGoodsList(marketGoodsMapper.selectByExample(marketGoodsExample1));
        // 商品表中的新品
        MarketGoodsExample marketGoodsExample2 = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria2 = marketGoodsExample2.createCriteria();
        criteria2.andIsNewEqualTo(true);
        wxHomeIndexVO.setNewGoodsList(marketGoodsMapper.selectByExample(marketGoodsExample2));
        // 人气推荐
        wxHomeIndexVO.setTopicList(marketTopicMapper.selectByExample(new MarketTopicExample()));
        return wxHomeIndexVO;
    }

    @Override
    public WxCategoryVO queryWxGoodsCategory(Integer id) {
        Integer index = id;
        // 查询商品类别
        WxCategoryVO wxCategoryVO = new WxCategoryVO();
        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey(id);
        Integer pid = marketCategory.getPid();
        if (pid == 0) {
            wxCategoryVO.setParentCategory(marketCategory);
            // 根据父分类id查询该分类下的子分类
            MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
            MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria();
            criteria.andPidEqualTo(id);
            wxCategoryVO.setBrotherCategory(marketCategoryMapper.selectByExample(marketCategoryExample));
            wxCategoryVO.setCurrentCategory(wxCategoryVO.getBrotherCategory().get(0));
            return wxCategoryVO;
        }
        // id变为父分类id
        id = pid == 0 ? id : pid;
        MarketCategory marketCategory1 = marketCategoryMapper.selectByPrimaryKey(id);
        wxCategoryVO.setParentCategory(marketCategory1);
        // 根据父分类id查询该分类下的子分类
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria();
        criteria.andPidEqualTo(id);
        wxCategoryVO.setBrotherCategory(marketCategoryMapper.selectByExample(marketCategoryExample));
        for (MarketCategory category : wxCategoryVO.getBrotherCategory()) {
            System.out.println(category.getId());
            System.out.println(index);
            if (index.equals(category.getId())) {
                System.out.println(category);
                wxCategoryVO.setCurrentCategory(category);
            }
        }
        return wxCategoryVO;
    }

    @Override
    public WxGoodsListData queryWxGoodsList(Integer categoryId, BaseParam param, String keyword, Integer brandId,String username) {
        if(brandId != null){
            Integer page = param.getPage();//页码
            Integer limit = param.getLimit();//数据量

            //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
            PageHelper.startPage(page, limit);
            //执行查询过程中拼接分页信息
            MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
            MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
            criteria.andBrandIdEqualTo(brandId);
            List<MarketGoods> marketGoodsList = marketGoodsMapper.selectByExample(marketGoodsExample);
            //total、pages、list
            //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
            //使用其有参构造方法 → 形参写来源于数据库的list
            PageInfo<MarketGoods> pageInfo = new PageInfo<>(marketGoodsList);
            long total = pageInfo.getTotal();
            int pages = pageInfo.getPages();
            return WxGoodsListData.data(total, pages, limit, page, marketGoodsList,null );
        }
        // 按照categoryId显示商品
        if(categoryId != 0){
            Integer page = param.getPage();//页码
            Integer limit = param.getLimit();//数据量

            //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
            PageHelper.startPage(page, limit);
            //执行查询过程中拼接分页信息
            MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
            MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
            criteria.andCategoryIdEqualTo(categoryId);
            List<MarketGoods> marketGoodsList = marketGoodsMapper.selectByExample(marketGoodsExample);
            List<MarketCategory> marketCategoryList = marketCategoryMapper.selectByExample(new MarketCategoryExample());

            //total、pages、list
            //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
            //使用其有参构造方法 → 形参写来源于数据库的list
            PageInfo<MarketGoods> pageInfo = new PageInfo<>(marketGoodsList);
            long total = pageInfo.getTotal();
            int pages = pageInfo.getPages();
            return WxGoodsListData.data(total, pages, limit, page, marketGoodsList, marketCategoryList);
        }
        else {
            // categoryId = 0, 搜索功能
            MarketSearchHistory marketSearchHistory = new MarketSearchHistory();
            // 通过username查出userid
            if (username != null){
                MarketUser marketUser = marketUserMapper.queryByUsername(username);

                marketSearchHistory.setUserId(marketUser.getId());
            }else {
                marketSearchHistory.setUserId(-1);
            }

            marketSearchHistory.setKeyword(keyword);
            marketSearchHistory.setFrom("wx");
            marketSearchHistory.setAddTime(new Date());
            marketSearchHistoryMapper.insertSelective(marketSearchHistory);
            Integer page = param.getPage();//页码
            Integer limit = param.getLimit();//数据量
            String sort = param.getSort();//排序列
            String order = param.getOrder();//desc\asc
            PageHelper.startPage(page, limit);
            MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
            MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
            marketGoodsExample.setOrderByClause(sort + " " + order);

            if (keyword != null && !"".equals(keyword)) {
                criteria.andNameLike("%" + keyword + "%");
            }
            List<MarketGoods> marketGoodsList = marketGoodsMapper.selectByExample(marketGoodsExample);
            ArrayList<Integer> integers = new ArrayList<>();
            for (MarketGoods marketGoods : marketGoodsList) {
                integers.add(marketGoods.getCategoryId());
            }
            MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
            MarketCategoryExample.Criteria categoryExampleCriteria = marketCategoryExample.createCriteria();
            if(integers.size() == 0){
                return WxGoodsListData.data(0,0,limit,0,new ArrayList<>(),new ArrayList<>());
            }
            categoryExampleCriteria.andIdIn(integers);
            List<MarketCategory> marketCategoryList = marketCategoryMapper.selectByExample(marketCategoryExample);

            //total、pages、list
            //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
            //使用其有参构造方法 → 形参写来源于数据库的list
            PageInfo<MarketGoods> pageInfo = new PageInfo<>(marketGoodsList);
            long total = pageInfo.getTotal();
            int pages = pageInfo.getPages();
            return WxGoodsListData.data(total, pages, limit, page, marketGoodsList,marketCategoryList);
        }
    }

    /**
     * 首页点击商品查看详情
     *
     * @author beiji
     * @since 2022/05/08 21:55
     */
    @Override
    public WxGoodsDetailVO queryWxGoodsDetail(Integer id) {
        WxGoodsDetailVO wxGoodsDetailVO = new WxGoodsDetailVO();
        // 前端传入商品id
        // attribute
        MarketGoodsAttributeExample marketGoodsAttributeExample = new MarketGoodsAttributeExample();
        MarketGoodsAttributeExample.Criteria criteria = marketGoodsAttributeExample.createCriteria();
        criteria.andGoodsIdEqualTo(id);
        wxGoodsDetailVO.setAttribute(marketGoodsAttributeMapper.selectByExample(marketGoodsAttributeExample));
        // TODO comment
        WxGoodsDetailCommentVO wxGoodsDetailCommentVO = new WxGoodsDetailCommentVO();
        // wxGoodsDetailCommentVO.setCount(0);
        MarketCommentExample marketCommentExample = new MarketCommentExample();
        marketCommentExample.createCriteria().andValueIdEqualTo(id);
        marketCommentExample.setOrderByClause("id DESC");
        List<MarketComment> marketComments = marketCommentMapper.selectByExample(marketCommentExample);
        wxGoodsDetailCommentVO.setCount(marketComments.size());

        List<WxCommentVO> data = new ArrayList<>();

        for (MarketComment marketComment : marketComments) {

            WxCommentVO wxCommentVO = new WxCommentVO();
            wxCommentVO.setAddTime(marketComment.getAddTime());
            wxCommentVO.setAdminContent(marketComment.getAdminContent());
            wxCommentVO.setAvatar(((int) marketComment.getStar()));
            wxCommentVO.setContent(marketComment.getContent());
            wxCommentVO.setId(marketComment.getId());
            // 查询用户名称
            MarketUser marketUser = marketUserMapper.selectByPrimaryKey(marketComment.getUserId());
            wxCommentVO.setNickname(marketUser.getNickname());
            wxCommentVO.setPicList(marketComment.getPicUrls());
            data.add(wxCommentVO);
        }
        wxGoodsDetailCommentVO.setData(data);
        wxGoodsDetailVO.setComment(wxGoodsDetailCommentVO);
        // TODO groupon
        wxGoodsDetailVO.setGroupon(new ArrayList<>());
        // info
        MarketGoods marketGoods = marketGoodsMapper.selectByPrimaryKey(id);
        wxGoodsDetailVO.setInfo(marketGoods);
        // issue
        wxGoodsDetailVO.setIssue(marketIssueMapper.selectByExample(new MarketIssueExample()));
        // productList
        MarketGoodsProductExample marketGoodsProductExample = new MarketGoodsProductExample();
        MarketGoodsProductExample.Criteria goodsProductExampleCriteria = marketGoodsProductExample.createCriteria();
        goodsProductExampleCriteria.andGoodsIdEqualTo(id);
        wxGoodsDetailVO.setProductList(marketGoodsProductMapper.selectByExample(marketGoodsProductExample));
        // TODO share
        wxGoodsDetailVO.setShare(false);
        // TODO shareImage
        wxGoodsDetailVO.setShareImage("");
        // specificationList
        List<WxMarketGoodsSpecificationVO> wxMarketGoodsSpecificationVOS = new ArrayList<>();

        WxMarketGoodsSpecificationVO wxMarketGoodsSpecificationVO = new WxMarketGoodsSpecificationVO();
        MarketGoodsSpecificationExample marketGoodsSpecificationExample = new MarketGoodsSpecificationExample();
        MarketGoodsSpecificationExample.Criteria goodsSpecificationExampleCriteria = marketGoodsSpecificationExample.createCriteria();
        goodsSpecificationExampleCriteria.andGoodsIdEqualTo(id);
        wxMarketGoodsSpecificationVO.setValueList(marketGoodsSpecificationMapper.selectByExample(marketGoodsSpecificationExample));
        // 查询到所有的规格
        // 初始化一个规格
        wxMarketGoodsSpecificationVO.setName(wxMarketGoodsSpecificationVO.getValueList().get(0).getSpecification());
        wxMarketGoodsSpecificationVO.setValueList(wxMarketGoodsSpecificationVO.getValueList());
        wxMarketGoodsSpecificationVOS.add(wxMarketGoodsSpecificationVO);
        wxGoodsDetailVO.setSpecificationList(new ArrayList<>());
        // 后续新增规格或者颜色分类
        for (MarketGoodsSpecification marketGoodsSpecification : wxMarketGoodsSpecificationVO.getValueList()) {
            for (WxMarketGoodsSpecificationVO marketGoodsSpecificationVO : wxGoodsDetailVO.getSpecificationList()) {
                if (marketGoodsSpecificationVO == null || !marketGoodsSpecificationVO.getName().equals(marketGoodsSpecification.getSpecification())) {
                    WxMarketGoodsSpecificationVO goodsSpecificationVO = new WxMarketGoodsSpecificationVO();
                    goodsSpecificationVO.setName(marketGoodsSpecification.getSpecification());
                    List<MarketGoodsSpecification> specificationList = new ArrayList<>();
                    specificationList.add(marketGoodsSpecification);
                    goodsSpecificationVO.setValueList(specificationList);
                    wxMarketGoodsSpecificationVOS.add(goodsSpecificationVO);
                }
                if (marketGoodsSpecificationVO.getName().equals(marketGoodsSpecification.getSpecification())) {
                    marketGoodsSpecificationVO.getValueList().add(marketGoodsSpecification);
                }
            }
        }
        wxGoodsDetailVO.setSpecificationList(wxMarketGoodsSpecificationVOS);

        // userHasCollect
        MarketCollectExample marketCollectExample = new MarketCollectExample();
        MarketCollectExample.Criteria collectExampleCriteria = marketCollectExample.createCriteria();
        collectExampleCriteria.andValueIdEqualTo(id);
        wxGoodsDetailVO.setUserHasCollect(marketCollectMapper.selectByExample(marketCollectExample).size());
        // brand
        wxGoodsDetailVO.setBrand(marketBrandMapper.selectByPrimaryKey(wxGoodsDetailVO.getInfo().getBrandId()));
        return wxGoodsDetailVO;
    }

    @Override
    public WxGoodsRelatedVO queryWxGoodsRelated(Integer id) {
        WxGoodsRelatedVO wxGoodsRelatedVO = new WxGoodsRelatedVO();

        MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
        criteria.andCategoryIdEqualTo(marketGoodsMapper.selectByPrimaryKey(id).getCategoryId());
        List<MarketGoods> marketGoodsList = marketGoodsMapper.selectByExample(marketGoodsExample);
        int total = marketGoodsList.size();

        if (total < 7) {
            wxGoodsRelatedVO.setList(marketGoodsList);
        } else {
            ArrayList<MarketGoods> marketGoods = new ArrayList<>();
            // random 范围 3 -> size-3
            Random random = new Random();
            int i = random.nextInt(total - 6);
            marketGoods.add(marketGoodsList.get(i));
            marketGoods.add(marketGoodsList.get(i + 1));
            marketGoods.add(marketGoodsList.get(i + 2));
            marketGoods.add(marketGoodsList.get(i + 3));
            marketGoods.add(marketGoodsList.get(i + 4));
            marketGoods.add(marketGoodsList.get(i + 5));
            wxGoodsRelatedVO.setList(marketGoods);
        }

        wxGoodsRelatedVO.setLimit(6);
        wxGoodsRelatedVO.setPage(1);
        wxGoodsRelatedVO.setPages(total / 6);
        wxGoodsRelatedVO.setTotal(total);
        return wxGoodsRelatedVO;
    }

}
