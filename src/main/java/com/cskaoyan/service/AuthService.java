package com.cskaoyan.service;

import com.cskaoyan.bean.login_demo.InfoData;
import com.cskaoyan.bean.reverseengineering.MarketAdmin;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/09 16:15
 */
public interface AuthService {
    MarketAdmin login(String username);

    InfoData authInfo(String username);
}
