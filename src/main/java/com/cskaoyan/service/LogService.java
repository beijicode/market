package com.cskaoyan.service;

import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.LogListVO;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 23:57
 */
public interface LogService {
    LogListVO query(BaseParam param, String name);

    void insertLog(String ip, String username,String action);
}
