package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketSystem;
import com.cskaoyan.bean.reverseengineering.MarketSystemExample;
import com.cskaoyan.bean.vo.configvo.ExpressConfigVO;
import com.cskaoyan.bean.vo.configvo.MallConfigVO;
import com.cskaoyan.bean.vo.configvo.OrderConfigVO;
import com.cskaoyan.bean.vo.configvo.WXConfigVO;
import com.cskaoyan.mapper.MarketSystemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    MarketSystemMapper marketSystemMapper;

    @Override
    public MallConfigVO getMallConfig() {
        //根据key模糊查询到对应记录
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("market_mall_"+"%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);

        //给MallConfigVO赋值
        MallConfigVO mallConfigVO = new MallConfigVO();
        HashMap<String, String> map = new HashMap<>();
        for (MarketSystem marketSystem : marketSystems) {
            map.put(marketSystem.getKeyName(),marketSystem.getKeyValue());
        }

        mallConfigVO.setMarket_mall_address(map.get("market_mall_address"));
        mallConfigVO.setMarket_mall_latitude(map.get("market_mall_latitude"));
        mallConfigVO.setMarket_mall_longitude(map.get("market_mall_longitude"));
        mallConfigVO.setMarket_mall_qq(map.get("market_mall_qq"));
        mallConfigVO.setMarket_mall_phone(map.get("market_mall_phone"));
        mallConfigVO.setMarket_mall_name(map.get("market_mall_name"));
        
        return mallConfigVO;
    }

    @Override
    public ExpressConfigVO getExpressConfig() {
        //根据key模糊查询到对应记录
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("market_express_"+"%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);

        //给ExpressConfigVO赋值
        ExpressConfigVO expressConfigVO = new ExpressConfigVO();
        HashMap<String, String> map = new HashMap<>();
        for (MarketSystem marketSystem : marketSystems) {
            map.put(marketSystem.getKeyName(),marketSystem.getKeyValue());
        }

        expressConfigVO.setMarket_express_freight_value(map.get("market_express_freight_value"));
        expressConfigVO.setMarket_express_freight_min(map.get("market_express_freight_min"));

        return expressConfigVO;
    }

    @Override
    public OrderConfigVO getOrderConfig() {
        //根据key模糊查询到对应记录
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("market_order_"+"%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);

        //给OrderConfigVO赋值
        OrderConfigVO orderConfigVO = new OrderConfigVO();
        HashMap<String, String> map = new HashMap<>();
        for (MarketSystem marketSystem : marketSystems) {
            map.put(marketSystem.getKeyName(),marketSystem.getKeyValue());
        }

        orderConfigVO.setMarket_order_comment(map.get("market_order_comment"));
        orderConfigVO.setMarket_order_unconfirm(map.get("market_order_unconfirm"));
        orderConfigVO.setMarket_order_unpaid(map.get("market_order_unpaid"));


        return orderConfigVO;
    }

    @Override
    public WXConfigVO getWXConfig() {
        //根据key模糊查询到对应记录
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("market_wx_"+"%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);

        //给OrderConfigVO赋值
        WXConfigVO wxConfigVO = new WXConfigVO();
        HashMap<String, String> map = new HashMap<>();
        for (MarketSystem marketSystem : marketSystems) {
            map.put(marketSystem.getKeyName(),marketSystem.getKeyValue());
        }

        wxConfigVO.setMarket_wx_catlog_goods(map.get("market_wx_catlog_goods"));
        wxConfigVO.setMarket_wx_catlog_list(map.get("market_wx_catlog_list"));
        wxConfigVO.setMarket_wx_index_brand(map.get("market_wx_index_brand"));
        wxConfigVO.setMarket_wx_index_hot(map.get("market_wx_index_hot"));
        wxConfigVO.setMarket_wx_index_topic(map.get("market_wx_index_topic"));
        wxConfigVO.setMarket_wx_share(map.get("market_wx_share"));
        wxConfigVO.setMarket_wx_index_new(map.get("market_wx_index_new"));

        return wxConfigVO;
    }

    @Override
    public void updateMallConfig(MallConfigVO mallConfigVO) throws IllegalAccessException {
        MarketSystem marketSystem = new MarketSystem();
        Class<? extends MallConfigVO> aClass = mallConfigVO.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            MarketSystemExample marketSystemExample = new MarketSystemExample();
            MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
            declaredField.setAccessible(true);
            criteria.andKeyNameEqualTo(declaredField.getName());
            marketSystem.setKeyValue((String) declaredField.get(mallConfigVO));
            marketSystemMapper.updateByExampleSelective(marketSystem,marketSystemExample);
        }

    }

    @Override
    public void updateExpressConfig(ExpressConfigVO expressConfigVO) throws IllegalAccessException {

        MarketSystem marketSystem = new MarketSystem();
        Class<? extends ExpressConfigVO> aClass = expressConfigVO.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            MarketSystemExample marketSystemExample = new MarketSystemExample();
            MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
            declaredField.setAccessible(true);
            criteria.andKeyNameEqualTo(declaredField.getName());
            marketSystem.setKeyValue((String) declaredField.get(expressConfigVO));
            marketSystemMapper.updateByExampleSelective(marketSystem,marketSystemExample);
        }
    }

    @Override
    public void updateOrderConfig(OrderConfigVO orderConfigVO) throws IllegalAccessException {

        MarketSystem marketSystem = new MarketSystem();
        Class<? extends OrderConfigVO> aClass = orderConfigVO.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            MarketSystemExample marketSystemExample = new MarketSystemExample();
            MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
            declaredField.setAccessible(true);
            criteria.andKeyNameEqualTo(declaredField.getName());
            marketSystem.setKeyValue((String) declaredField.get(orderConfigVO));
            marketSystemMapper.updateByExampleSelective(marketSystem,marketSystemExample);
        }
    }

    @Override
    public void updateWXConfig(WXConfigVO wxConfigVO) throws IllegalAccessException {

        MarketSystem marketSystem = new MarketSystem();
        Class<? extends WXConfigVO> aClass = wxConfigVO.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            MarketSystemExample marketSystemExample = new MarketSystemExample();
            MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
            declaredField.setAccessible(true);
            criteria.andKeyNameEqualTo(declaredField.getName());
            marketSystem.setKeyValue((String) declaredField.get(wxConfigVO));
            marketSystemMapper.updateByExampleSelective(marketSystem,marketSystemExample);
        }
    }
}
