package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketRole;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.RoleListVO;
import com.cskaoyan.bean.vo.RoleOptionsVO;
import com.cskaoyan.bean.vo.permissionvo.RolePermissionsVO;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/5 22:38
 */
public interface RoleService {
    RoleListVO query(BaseParam param, String username);

    MarketRole roleCreate(MarketRole marketRole);

    Long selectRole(String name);

    RoleOptionsVO selectRoleOptions();

    int roleDelete(MarketRole marketRole);

    int roleUpdate(MarketRole marketRole);

    RolePermissionsVO rolePermissions(Integer roleId);

    void rolePermissionsUpdate(String[] permissions,Integer roleId);
}
