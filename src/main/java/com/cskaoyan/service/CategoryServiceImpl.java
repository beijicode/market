package com.cskaoyan.service;

import com.cskaoyan.bean.bo.category.CategoryCreateBO;
import com.cskaoyan.bean.bo.category.CategoryDeleteBO;
import com.cskaoyan.bean.bo.category.CategoryUpdateBO;
import com.cskaoyan.bean.reverseengineering.MarketCategory;
import com.cskaoyan.bean.reverseengineering.MarketCategoryExample;
import com.cskaoyan.bean.vo.category.*;
import com.cskaoyan.mapper.MarketCategoryMapper;
import com.cskaoyan.wx.bean.vo.catalog.WxCatalogCurrentVO;
import com.cskaoyan.wx.bean.vo.catalog.WxCatalogIndexVO;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author tangwei
 * @since 2022/05/05 20:27
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Override
    public CategoryListVO queryAllCategory() {
        // 页码和每页条数怎么来的？
        Integer page = 1;
        Integer limit = 20;
        Integer hadParentId = 0;
        Integer haeDeleted = 0;

        List<ParentCategoryVO> parentCategoryVOList = new ArrayList<>();

        // 先查出来pId为0的类别，获得其id，由它的id查它的子类别
        PageHelper.startPage(page, limit);
        // 怎么用？
        // List<MarketCategory> marketCategoryList = marketCategoryMapper.selectByExample(MarketCategoryExample example);

        // 根据有无pid查出父类别
        List<Integer> parentCategoryId = marketCategoryMapper.selectParentCategoryIdByHasParentId(hadParentId, haeDeleted);

        for (int i = 0; i < parentCategoryId.size(); i++) {
            // 查出每个父类别对应有的孩子类别
            List<ChildrenOfCategoryVO> childrenOfCategoryVOList = marketCategoryMapper.selectChildrenByParentCategoryId(parentCategoryId.get(i), haeDeleted);

            ParentCategoryIntermediateVO parentCategoryIntermediateVO = new ParentCategoryIntermediateVO();
            // 为封装父类别由父类别id查出父类别的信息
            parentCategoryIntermediateVO = marketCategoryMapper.selectParentCategoryIntermediatById(parentCategoryId.get(i), haeDeleted);

            ParentCategoryVO parentCategoryVO = new ParentCategoryVO();

            parentCategoryVO.setId(parentCategoryIntermediateVO.getId());
            parentCategoryVO.setName(parentCategoryIntermediateVO.getName());
            parentCategoryVO.setKeywords(parentCategoryIntermediateVO.getKeywords());
            parentCategoryVO.setDesc(parentCategoryIntermediateVO.getDesc());
            parentCategoryVO.setIconUrl(parentCategoryIntermediateVO.getIconUrl());
            parentCategoryVO.setPicUrl(parentCategoryIntermediateVO.getPicUrl());
            parentCategoryVO.setLevel(parentCategoryIntermediateVO.getLevel());

            parentCategoryVO.setChildren(childrenOfCategoryVOList);

            parentCategoryVOList.add(parentCategoryVO);

        }

        // total、pages、list
        // 通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        // 使用其有参构造方法 → 形参写来源于数据库的list
        // PageInfo<Integer> pageInfo = new PageInfo<>(parentCategoryId);
        // long total = pageInfo.getTotal();
        // int pages = pageInfo.getPages();

        long total = 20;
        int pages = 1;

        return new CategoryListVO(total, pages, limit, page, parentCategoryVOList);
    }

    @Override
    public CategorySimpleVO queryCategorySimple() {
        Integer page = 1;
        Integer limit = 20;
        Integer hadParentId = 0;
        Integer haeDeleted = 0;

        PageHelper.startPage(page, limit);

        List<CategorySimpleIntermediateVO> categorySimpleIntermediateVOList = marketCategoryMapper.queryCategorySimple(hadParentId, haeDeleted);

        // PageInfo<CategorySimpleIntermediateVO> pageInfo = new PageInfo<>(categorySimpleIntermediateVOList);
        // long total = pageInfo.getTotal();
        // int pages = pageInfo.getPages();

        long total = 20;
        int pages = 1;

        return new CategorySimpleVO(total, pages, limit, page, categorySimpleIntermediateVOList);

    }

    @Override
    public void createCategory(CategoryCreateBO categoryCreateBO) {

        Date date = new Date();
        // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        categoryCreateBO.setSortOrder(0);
        categoryCreateBO.setAddTime(date);
        categoryCreateBO.setUpdateTime(date);
        categoryCreateBO.setDeleted(0);
        marketCategoryMapper.insertOneCategory(categoryCreateBO);
    }

    @Override
    public CategoryCreateVO queryOneCategoryById(Integer categoryCreateBOId) {
        CategoryCreateVO categoryCreateVO = marketCategoryMapper.selectOneCategoryById(categoryCreateBOId);
        return categoryCreateVO;
    }

    @Override
    public int updateCategory(CategoryUpdateBO categoryUpdateBO) {

        // 改二级类目
        if (categoryUpdateBO.getChildren() == null) {
            // 不填一级类目，返回错误代码
            if (categoryUpdateBO.getPid() == null) {
                return 401;

            } else {
                // else if (categoryUpdateBO.getId() != null) {
                // 填了一级类目
                Date date = new Date();
                categoryUpdateBO.setUpdateTime(date);

                marketCategoryMapper.updateSecondCategoryById(categoryUpdateBO.getId(), categoryUpdateBO);
                return 0;
            }
        } else {
            // 改一级类目级别, 把它的子类目也加到它的父类目
            // if (categoryUpdateBO.getPid() != 0 && categoryUpdateBO.getChildren().size() == 0) {
            if (categoryUpdateBO.getPid() != null) {
                Date date = new Date();
                categoryUpdateBO.setUpdateTime(date);
                marketCategoryMapper.updateFirstCategoryById(categoryUpdateBO.getId(), categoryUpdateBO);

                // 查出子类目，把子类目的父类目改了
                Integer newParentId = categoryUpdateBO.getPid();
                List<Integer> childrenId = marketCategoryMapper.selectChildrenIdByParentId(categoryUpdateBO.getId());
                for (int i = 0; i < childrenId.size(); i++) {
                    marketCategoryMapper.updateChildrenPIdById(childrenId.get(i), newParentId);
                }
                return 0;
            } else {
                // 不改一级类目级别
                categoryUpdateBO.setPid(0);

                Date date = new Date();
                categoryUpdateBO.setUpdateTime(date);

                marketCategoryMapper.updateFirstCategoryById(categoryUpdateBO.getId(), categoryUpdateBO);
                return 0;
            }
        }
        // return 0;
    }

    @Override
    public void deleteCategory(CategoryDeleteBO categoryDeleteBO) {
        if (categoryDeleteBO.getChildren() == null) {
            // 删除二级类目
            marketCategoryMapper.updateDeleteStatusByCategoryId(categoryDeleteBO.getId());
        } else {
            // 删除一级类目
            // 获取一级类目的子类目的id
            List<Integer> childrenId = marketCategoryMapper.selectChildrenIdByParentId(categoryDeleteBO.getId());
            for (int i = 0; i < childrenId.size(); i++) {
                marketCategoryMapper.updateDeleteStatusByCategoryId(childrenId.get(i));
            }

            marketCategoryMapper.updateDeleteStatusByCategoryId(categoryDeleteBO.getId());
        }
    }

    @Override
    public WxCatalogIndexVO queryAllCategorysAtWx() {
        WxCatalogIndexVO wxCatalogIndexVO = new WxCatalogIndexVO();

        // 查所有的一级类目
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria();
        String sort = "id";
        String order = "asc";
        marketCategoryExample.setOrderByClause(sort + " " + order);
        criteria.andDeletedEqualTo(false);
        criteria.andPidEqualTo(0);

        List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample);
        wxCatalogIndexVO.setCategoryList(marketCategories);

        // 查id最小的一级类目
        Integer currentCategoryId = marketCategories.get(0).getId();

        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey(currentCategoryId);
        wxCatalogIndexVO.setCurrentCategory(marketCategory);

        // 查id最小的一级类目的二级类目
        MarketCategoryExample marketCategoryExample1 = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria1 = marketCategoryExample1.createCriteria();
        marketCategoryExample1.setOrderByClause(sort + " " + order);
        criteria1.andDeletedEqualTo(false);
        criteria1.andPidEqualTo(currentCategoryId);
        List<MarketCategory> marketCategories1 = marketCategoryMapper.selectByExample(marketCategoryExample1);
        wxCatalogIndexVO.setCurrentSubCategory(marketCategories1);

        return wxCatalogIndexVO;
    }

    @Override
    public WxCatalogCurrentVO queryCurrentCatalog(Integer id) {
        WxCatalogCurrentVO wxCatalogCurrentVO = new WxCatalogCurrentVO();

        // 查当前的一级类目
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria();
        String sort = "id";
        String order = "asc";
        marketCategoryExample.setOrderByClause(sort + " " + order);
        criteria.andDeletedEqualTo(false);
        criteria.andPidEqualTo(0);
        criteria.andIdEqualTo(id);

        List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample);

        wxCatalogCurrentVO.setCurrentCategory(marketCategories.get(0));

        // 查当前一级目录的二级目录
        MarketCategoryExample marketCategoryExample1 = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria1 = marketCategoryExample1.createCriteria();
        marketCategoryExample1.setOrderByClause(sort + " " + order);
        criteria1.andDeletedEqualTo(false);
        criteria1.andPidEqualTo(id);
        List<MarketCategory> marketCategories1 = marketCategoryMapper.selectByExample(marketCategoryExample1);
        wxCatalogCurrentVO.setCurrentSubCategory(marketCategories1);

        return wxCatalogCurrentVO;
    }
}