package com.cskaoyan.service;

import com.cskaoyan.bean.bo.GoodsCreateBO;
import com.cskaoyan.bean.bo.goodsbo.GoodsListBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.goodsvo.GoodsCatAndBrandVO;
import com.cskaoyan.bean.vo.goodsvo.GoodsDetailVO;
import com.cskaoyan.bean.vo.goodsvo.GoodsListVO;
import com.cskaoyan.wx.bean.WxGoodsListData;
import com.cskaoyan.wx.bean.vo.goods.WxCategoryVO;
import com.cskaoyan.wx.bean.vo.goods.WxGoodsDetailVO;
import com.cskaoyan.wx.bean.vo.goods.WxGoodsRelatedVO;
import com.cskaoyan.wx.bean.vo.goods.WxHomeIndexVO;

import java.util.Map;

public interface GoodsService {
    GoodsListVO list(GoodsListBO goodsListBO);

    void delete(Map goods);

    GoodsDetailVO detail(Integer id);

    GoodsCatAndBrandVO catAndBrand();

    void create(GoodsCreateBO goodsCreateBO);

    void update(GoodsCreateBO goodsUpdateBO);

    WxHomeIndexVO wxGoodsIndex();

    WxCategoryVO queryWxGoodsCategory(Integer id);

    WxGoodsListData queryWxGoodsList(Integer categoryId, BaseParam param,String keyword,Integer brandId,String username);

    WxGoodsDetailVO queryWxGoodsDetail(Integer id);

    WxGoodsRelatedVO queryWxGoodsRelated(Integer id);
}
