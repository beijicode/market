package com.cskaoyan.service;

import com.cskaoyan.bean.bo.ProfilePasswordBO;
import com.cskaoyan.bean.login_demo.*;
import com.cskaoyan.bean.reverseengineering.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.DashboardVO;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.Md5Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author stone
 * @date 2022/01/06 16:36
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Autowired
    MarketAddressMapper marketAddressMapper;

    @Autowired
    MarketCollectMapper marketCollectMapper;

    @Autowired
    MarketFootprintMapper marketFootprintMapper;

    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Autowired
    MarketFeedbackMapper marketFeedbackMapper;

    @Autowired
    MarketAdminMapper marketAdminMapper;

    @Override
    public UserData query(BaseParam param, String username, String mobile) {
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        List<User> list = userMapper.select(sort, order, username, mobile);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<User> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return UserData.data(total, pages, limit, page, list);
    }

    @Override
    public DashboardVO dashboard() {
        DashboardVO dashboardVO = new DashboardVO();
        dashboardVO.setGoodsTotal(userMapper.dashboard1());
        dashboardVO.setProductTotal(userMapper.dashboard2());
        dashboardVO.setOrderTotal(userMapper.dashboard3());
        dashboardVO.setUserTotal(userMapper.dashboard4());
        return dashboardVO;
    }

    @Override
    public int updateUser(MarketUser user) {
        int rows = marketUserMapper.updateByPrimaryKeySelective(user);
        return rows;
    }

    @Override
    public MarketUser detail(Integer id) {
        MarketUser marketUser = marketUserMapper.selectByPrimaryKey(id);
        return marketUser;
    }

    @Override
    public AddressData queryAddr(String name, Integer userId, BaseParam param) {
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        // List<MarketAddress> list = userMapper.selectAdd(sort, order);
        MarketAddressExample example = new MarketAddressExample();
        MarketAddressExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause(sort + " " + order);
        if (name != null && !"".equals(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (userId != null && !"".equals(userId)) {
            criteria.andUserIdEqualTo(userId);
        }

        List<MarketAddress> list = marketAddressMapper.selectByExample(example);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketAddress> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return AddressData.data(total, pages, limit, page, list);
    }

    @Override
    public CollectData queryCollect(Integer userId, Integer valueId, BaseParam param) {
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        // List<MarketAddress> list = userMapper.selectAdd(sort, order);
        MarketCollectExample example = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause(sort + " " + order);
        if (userId != null && !"".equals(userId)) {
            criteria.andUserIdEqualTo(userId);
        }
        if (valueId != null && !"".equals(valueId)) {
            criteria.andValueIdEqualTo(valueId);
        }

        List<MarketCollect> list = marketCollectMapper.selectByExample(example);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketCollect> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return CollectData.data(total, pages, limit, page, list);
    }

    @Override
    public FootprintData queryFoot(Integer userId, Integer goodsId, BaseParam param) {
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        // List<MarketAddress> list = userMapper.selectAdd(sort, order);
        MarketFootprintExample example = new MarketFootprintExample();
        MarketFootprintExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause(sort + " " + order);
        if (userId != null && !"".equals(userId)) {
            criteria.andUserIdEqualTo(userId);
        }
        if (goodsId != null && !"".equals(goodsId)) {
            criteria.andGoodsIdEqualTo(goodsId);
        }

        List<MarketFootprint> list = marketFootprintMapper.selectByExample(example);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketFootprint> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return FootprintData.data(total, pages, limit, page, list);
    }

    @Override
    public HistoryData querySearchHistory(Integer userId, String keyword, BaseParam param) {
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        // List<MarketAddress> list = userMapper.selectAdd(sort, order);
        MarketSearchHistoryExample example = new MarketSearchHistoryExample();
        MarketSearchHistoryExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause(sort + " " + order);
        if (userId != null && !"".equals(userId)) {
            criteria.andUserIdEqualTo(userId);
        }
        if (keyword != null && !"".equals(keyword)) {
            criteria.andKeywordLike("%" + keyword + "%");
        }

        List<MarketSearchHistory> list = marketSearchHistoryMapper.selectByExample(example);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketSearchHistory> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return HistoryData.data(total, pages, limit, page, list);
    }

    @Override
    public FeedbackData queryFeedback(String username, Integer id, BaseParam param) {
        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        //分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        // List<MarketAddress> list = userMapper.selectAdd(sort, order);
        MarketFeedbackExample example = new MarketFeedbackExample();
        MarketFeedbackExample.Criteria criteria = example.createCriteria();
        example.setOrderByClause(sort + " " + order);
        if (id != null && !"".equals(id)) {
            criteria.andIdEqualTo(id);
        }
        if (username != null && !"".equals(username)) {
            criteria.andUsernameLike("%" + username + "%");
        }

        List<MarketFeedback> list = marketFeedbackMapper.selectByExample(example);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketFeedback> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return FeedbackData.data(total, pages, limit, page, list);
    }

    @Override
    public int updateAdminPassword(String username, ProfilePasswordBO profilePasswordBO) {
        MarketAdminExample marketAdminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        criteria.andPasswordEqualTo(Md5Utils.getMd5(profilePasswordBO.getOldPassword()));
        MarketAdmin marketAdmin = new MarketAdmin();
        marketAdmin.setPassword(Md5Utils.getMd5(profilePasswordBO.getNewPassword()));
        int i = marketAdminMapper.updateByExampleSelective(marketAdmin, marketAdminExample);
        return i;
    }

}
