package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketAdmin;
import com.cskaoyan.bean.reverseengineering.MarketAdminExample;
import com.cskaoyan.bean.bo.AdminCreateBO;
import com.cskaoyan.bean.bo.AdminDeleteBO;
import com.cskaoyan.bean.bo.AdminUpdateBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.AdminListVO;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.util.Md5Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/5 20:27
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    MarketAdminMapper marketAdminMapper;

    /**
     * 管理员管理，管理员列表
     * @param param
     * @param username
     * @return
     */
    @Override
    public AdminListVO query(BaseParam param, String username) {


        Integer page = param.getPage();//页码
        Integer limit = param.getLimit();//数据量
        String sort = param.getSort();//排序列
        String order = param.getOrder();//desc\asc
        // 分页插件 PageHelper，辅助我们做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        //执行查询过程中拼接分页信息
        MarketAdminExample marketAdminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();

        marketAdminExample.setOrderByClause(sort+" "+order);
        if ( username != null && !"".equals(username)){
            criteria.andUsernameLike("%" + username + "%");
        }
        criteria.andDeletedEqualTo(false);
        List<MarketAdmin> marketAdmins = marketAdminMapper.selectByExample(marketAdminExample);

        //total、pages、list
        //通过PageHelper帮我们来获得一些和分页相关的信息PageInfo
        //使用其有参构造方法 → 形参写来源于数据库的list
        PageInfo<MarketAdmin> pageInfo = new PageInfo<>(marketAdmins);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return AdminListVO.data(total,pages,limit,page,marketAdmins);
    }

    /**
     * 管理员管理，新增管理员
     * @param adminCreateBO
     * @return
     */
    @Override
    public MarketAdmin adminCreate(AdminCreateBO adminCreateBO) {

        MarketAdmin marketAdmin = new MarketAdmin();
        marketAdmin.setUsername(adminCreateBO.getUsername());
        marketAdmin.setPassword(Md5Utils.getMd5(adminCreateBO.getPassword()));
        marketAdmin.setAvatar(adminCreateBO.getAvatar());
        marketAdmin.setRoleIds(adminCreateBO.getRoleIds());
        marketAdmin.setUpdateTime(new Date());
        marketAdmin.setDeleted(false);
        int affectedRows = marketAdminMapper.insertSelective(marketAdmin);
        if (affectedRows != 1){
            BaseRespVo.ok(null,"创建管理员失败！",500);
        }
        return marketAdmin;
    }

    /**
     * 管理员管理，新增管理员,查询管理员名称是否重复
     * @param username
     * @return
     */
    @Override
    public int selectAdminByUsername(String username) {

        MarketAdminExample marketAdminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();
        criteria.andUsernameEqualTo(username);

        int count = (int) marketAdminMapper.countByExample(marketAdminExample);
        return count;

    }

    /**
     * 管理员管理，修改管理员
     * @param adminUpdateBO
     * @return
     */
    @Override
    public int adminUpdate(AdminUpdateBO adminUpdateBO) {

        MarketAdminExample marketAdminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();
        criteria.andIdEqualTo(adminUpdateBO.getId());
        MarketAdmin marketAdmin = new MarketAdmin(adminUpdateBO.getId(), adminUpdateBO.getUsername(), adminUpdateBO.getPassword(), adminUpdateBO.getLastLoginIp(), adminUpdateBO.getLastLoginTime(), adminUpdateBO.getAvatar(), adminUpdateBO.getAddTime(), new Date(), adminUpdateBO.getDeleted(), adminUpdateBO.getRoleIds());
        int affectedRows = marketAdminMapper.updateByPrimaryKeySelective(marketAdmin);
        return affectedRows;
    }

    /**
     * 通过id查询管理员
     * @param id
     * @return
     */
    @Override
    public MarketAdmin SelectAdminById(Integer id) {

        MarketAdmin marketAdmin = marketAdminMapper.selectByPrimaryKey(id);
        return marketAdmin;
    }

    @Override
    public int adminDelete(AdminDeleteBO adminDeleteBO) {

        MarketAdmin marketAdmin = new MarketAdmin();
        marketAdmin.setId(adminDeleteBO.getId());
        marketAdmin.setDeleted(true);
        marketAdmin.setUpdateTime(new Date());

        int affectedRows = marketAdminMapper.updateByPrimaryKeySelective(marketAdmin);

        return affectedRows;
    }
}
