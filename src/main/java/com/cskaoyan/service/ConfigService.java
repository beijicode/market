package com.cskaoyan.service;

import com.cskaoyan.bean.vo.configvo.ExpressConfigVO;
import com.cskaoyan.bean.vo.configvo.MallConfigVO;
import com.cskaoyan.bean.vo.configvo.OrderConfigVO;
import com.cskaoyan.bean.vo.configvo.WXConfigVO;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public interface ConfigService {
    MallConfigVO getMallConfig();

    ExpressConfigVO getExpressConfig();

    OrderConfigVO getOrderConfig();

    WXConfigVO getWXConfig();

    void updateMallConfig(MallConfigVO mallConfigVO) throws IllegalAccessException;

    void updateExpressConfig(ExpressConfigVO expressConfigVO) throws IllegalAccessException;

    void updateOrderConfig(OrderConfigVO orderConfigVO) throws IllegalAccessException;

    void updateWXConfig(WXConfigVO wxConfigVO) throws IllegalAccessException;
}
