package com.cskaoyan.service;

import com.cskaoyan.bean.bo.ProfilePasswordBO;
import com.cskaoyan.bean.login_demo.*;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.DashboardVO;

public interface UserService {
    UserData query(BaseParam param, String username,String mobile);


    DashboardVO dashboard();


    int updateUser(MarketUser user);


    MarketUser detail(Integer id);

    AddressData queryAddr(String name, Integer userId, BaseParam param);

    CollectData queryCollect(Integer userId, Integer valueId, BaseParam param);

    FootprintData queryFoot(Integer userId, Integer goodsId, BaseParam param);

    HistoryData querySearchHistory(Integer userId, String keyword, BaseParam param);

    FeedbackData queryFeedback(String username, Integer id, BaseParam param);

    int updateAdminPassword(String username, ProfilePasswordBO profilePasswordBO);
}
