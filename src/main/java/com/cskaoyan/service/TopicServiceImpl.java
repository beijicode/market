package com.cskaoyan.service;


import com.cskaoyan.bean.bo.topicbo.TopicBO;
import com.cskaoyan.bean.bo.topicbo.TopicIntegerBO;
import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketTopic;
import com.cskaoyan.bean.reverseengineering.MarketTopicExample;
import com.cskaoyan.bean.vo.promote.topicVO.TopicData;
import com.cskaoyan.bean.vo.promote.topicVO.TopicReadVO;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketTopicMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 创建日期: 2022/05/06 20:05
 *
 * @author yangfan
 */
@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    MarketTopicMapper marketTopicMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public TopicData list(TopicBO topicBO) {

        MarketTopicExample example = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = example.createCriteria();

        if (topicBO.getTitle() != null) {
            criteria.andTitleLike("%" + topicBO.getTitle() + "%");
        }
        if (topicBO.getSubtitle() != null) {
            criteria.andSubtitleLike("%" + topicBO.getSubtitle() + "%");
        }
        // 排序
        if((topicBO.getSort() != null || "".equals(topicBO.getSort())) && (topicBO.getLimit() != null || "".equals(topicBO.getLimit()))){
            example.setOrderByClause(topicBO.getSort() + " " + topicBO.getOrder());
        }
        // example.setOrderByClause(topicBO.getSort() + " " + topicBO.getOrder());
        // 分页
        PageHelper.startPage(topicBO.getPage(),topicBO.getLimit());

        List<MarketTopic> list = marketTopicMapper.selectByExample(example);
        PageInfo<MarketTopic> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        TopicData topicData = new TopicData();
        topicData.setList(list);
        topicData.setLimit(topicBO.getLimit());
        topicData.setPage(topicBO.getPage());
        topicData.setPages(pages);
        topicData.setTotal((int) total);


        return topicData;
    }


    @Override
    public MarketTopic create(MarketTopic marketTopic) {

        marketTopic.setAddTime(new Date());
        marketTopic.setUpdateTime(new Date());

        marketTopicMapper.insert(marketTopic);

        return marketTopic;
    }

    @Override
    public TopicReadVO selectByTopicId(Integer id) {

        MarketTopic topic = marketTopicMapper.selectByPrimaryKey(id);

        Integer[] goods = topic.getGoods();

        List<MarketGoods> marketGoods = new ArrayList<>();
        for (Integer goodId : goods) {

            marketGoods.add(marketGoodsMapper.selectByPrimaryKey(goodId));

        }

        TopicReadVO topicReadVO = new TopicReadVO();
        topicReadVO.setGoodsList(marketGoods);
        topicReadVO.setTopic(topic);
        return topicReadVO;
    }

    @Override
    public MarketTopic update(MarketTopic marketTopic) {

        marketTopic.setUpdateTime(new Date());

        marketTopicMapper.updateByPrimaryKey(marketTopic);

        return marketTopic;
    }

    @Override
    public void delete(MarketTopic marketTopic) {
        marketTopicMapper.deleteByPrimaryKey(marketTopic.getId());
    }

    @Override
    public void batchDelete(TopicIntegerBO ids) {
        Integer[] idss = ids.getIds();
        for (Integer id : idss) {
            marketTopicMapper.deleteByPrimaryKey(id);
        }
    }
}
