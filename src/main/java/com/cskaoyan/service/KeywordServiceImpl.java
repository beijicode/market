package com.cskaoyan.service;

import com.cskaoyan.bean.reverseengineering.MarketKeyword;
import com.cskaoyan.bean.reverseengineering.MarketKeywordExample;
import com.cskaoyan.bean.bo.keywordbo.KeywordCreateBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordDeleteBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordListBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordUpdateBO;
import com.cskaoyan.bean.vo.keywordvo.KeywordCreateVO;
import com.cskaoyan.bean.vo.keywordvo.KeywordListVO;
import com.cskaoyan.bean.vo.keywordvo.KeywordUpdateVO;
import com.cskaoyan.mapper.MarketKeywordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/05 20:12
 */

@Service
public class KeywordServiceImpl implements KeywordService {

    @Autowired
    MarketKeywordMapper marketKeywordMapper;

    @Override
    public KeywordListVO query(KeywordListBO keywordListBO) {
        Integer page = keywordListBO.getPage();
        Integer limit = keywordListBO.getLimit();
        String sort = keywordListBO.getSort();
        String order = keywordListBO.getOrder();

        PageHelper.startPage(page, limit);

        MarketKeywordExample marketKeywordExample = new MarketKeywordExample();
        marketKeywordExample.setOrderByClause(sort + " " + order);
        MarketKeywordExample.Criteria criteria = marketKeywordExample.createCriteria();
        criteria.andKeywordLike("%" + ((keywordListBO.getKeyword() == null) ? "" : keywordListBO.getKeyword()) + "%");
        criteria.andUrlLike("%" + ((keywordListBO.getUrl() == null) ? "" : keywordListBO.getUrl()) + "%");
        criteria.andDeletedEqualTo(false);
        List<MarketKeyword> list = marketKeywordMapper.selectByExample(marketKeywordExample);


        PageInfo<MarketKeyword> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        KeywordListVO keywordListVO = new KeywordListVO((int) total, pages, limit, page, list);
        return keywordListVO;

    }

    @Override
    public KeywordCreateVO create(KeywordCreateBO keywordCreateBO) {
        MarketKeyword marketKeyword = new MarketKeyword(null, keywordCreateBO.getKeyword(), keywordCreateBO.getUrl(), keywordCreateBO.getIsHot(), keywordCreateBO.getIsDefault(), null, new Date(), new Date(), null);
        marketKeywordMapper.insertSelective(marketKeyword);
        MarketKeyword selectByPrimaryKey = marketKeywordMapper.selectByPrimaryKey(marketKeyword.getId());
        KeywordCreateVO keywordCreateVO = new KeywordCreateVO(selectByPrimaryKey.getAddTime(), selectByPrimaryKey.getId(), selectByPrimaryKey.getKeyword(), selectByPrimaryKey.getUpdateTime(), selectByPrimaryKey.getUrl());
        return keywordCreateVO;
    }

    @Override
    public KeywordUpdateVO update(KeywordUpdateBO keywordUpdateBO) {
        MarketKeyword marketKeyword = new MarketKeyword(keywordUpdateBO.getId(), keywordUpdateBO.getKeyword(), keywordUpdateBO.getUrl(), keywordUpdateBO.getIsHot(), keywordUpdateBO.getIsDefault(), keywordUpdateBO.getSortOrder(), keywordUpdateBO.getAddTime(), new Date(), keywordUpdateBO.getDeleted());
        marketKeywordMapper.updateByPrimaryKeySelective(marketKeyword);
        MarketKeyword selectByPrimaryKey = marketKeywordMapper.selectByPrimaryKey(keywordUpdateBO.getId());
        KeywordUpdateVO keywordUpdateVO = new KeywordUpdateVO(selectByPrimaryKey.getId(), selectByPrimaryKey.getKeyword(), selectByPrimaryKey.getUrl(), selectByPrimaryKey.getIsHot(), selectByPrimaryKey.getIsDefault(), selectByPrimaryKey.getSortOrder(), selectByPrimaryKey.getAddTime(), selectByPrimaryKey.getUpdateTime(), selectByPrimaryKey.getDeleted());
        return keywordUpdateVO;
    }

    @Override
    public void delete(KeywordDeleteBO keywordDeleteBO) {
        MarketKeyword marketKeyword = new MarketKeyword(keywordDeleteBO.getId(), keywordDeleteBO.getKeyword(), keywordDeleteBO.getUrl(), keywordDeleteBO.getIsHot(), keywordDeleteBO.getIsDefault(), keywordDeleteBO.getSortOrder(), keywordDeleteBO.getAddTime(), new Date(), true);
        marketKeywordMapper.updateByPrimaryKeySelective(marketKeyword);
    }
}
