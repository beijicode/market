package com.cskaoyan.service;

import com.cskaoyan.bean.bo.brandbo.BrandCreateBO;
import com.cskaoyan.bean.bo.brandbo.BrandDeleteBO;
import com.cskaoyan.bean.reverseengineering.MarketBrand;
import com.cskaoyan.bean.vo.RegionListVO;
import com.cskaoyan.bean.vo.brandvo.BrandListVO;
import com.cskaoyan.wx.bean.vo.brand.WxBrandListVO;


/**
 * 创建日期: 2022/05/05 17:20
 *
 * @author zhangfuqiang
 */

public interface BrandService {
    RegionListVO getRegionList(RegionListVO regionListVO);

    BrandListVO list(Integer page, Integer limit, String sort, String order, Integer id, String name);

    Integer delete(BrandDeleteBO brandDeleteBO);

    Integer update(BrandDeleteBO brandDeleteBO);

    BrandDeleteBO create(BrandCreateBO brandCreateBO);

    WxBrandListVO queryAllBrands(Integer page, Integer limit);

    MarketBrand queryBrandDetail(Integer id);
}
