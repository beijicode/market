package com.cskaoyan.service;

import com.cskaoyan.bean.bo.issue.IssueCreateBO;
import com.cskaoyan.bean.bo.issue.IssueDeleteBO;
import com.cskaoyan.bean.bo.issue.IssueListBO;
import com.cskaoyan.bean.bo.issue.IssueUpdateBO;
import com.cskaoyan.bean.vo.issue.IssueCreateVO;
import com.cskaoyan.bean.vo.issue.IssueListVO;
import com.cskaoyan.bean.vo.issue.IssueUpdateVO;
import com.cskaoyan.wx.bean.vo.issue.IssuesInWxQueryVO;

/**
 *
 * @since 2022/05/06 22:44
 * @author tangwei
 */
public interface IssueService {

    IssueListVO queryAllIssues(IssueListBO issueListBO);

    IssueCreateVO createIssue(IssueCreateBO issueCreateBO);

    IssueUpdateVO updateIssue(IssueUpdateBO issueUpdateBO);

    void deleteIssue(IssueDeleteBO issueDeleteBO);

    IssuesInWxQueryVO queryAllIssuesInWx(Integer page, Integer limit);
}