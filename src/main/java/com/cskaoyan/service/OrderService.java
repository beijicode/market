package com.cskaoyan.service;

import com.cskaoyan.bean.bo.order.OrderListBO;
import com.cskaoyan.bean.reverseengineering.MarketOrder;
import com.cskaoyan.bean.vo.order.ExpressListVO;
import com.cskaoyan.bean.vo.order.OrderDetailVO;
import com.cskaoyan.bean.vo.order.OrderListVO;
import com.cskaoyan.wx.bean.bo.aftersalebo.AftersaleBO;
import com.cskaoyan.wx.bean.bo.orderbo.WxOrderListBO;
import com.cskaoyan.wx.bean.bo.orderbo.WxOrderSubmitBO;
import com.cskaoyan.wx.bean.po.orderpo.WxComment;
import com.cskaoyan.wx.bean.vo.WxUserIndexVO;
import com.cskaoyan.wx.bean.vo.aftersale.WxAfterSaleDetail;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailOrderGoodsVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderSubmitVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderVO;

import java.util.List;
import java.util.Map;

public interface OrderService {

    OrderListVO queryAllOrders(OrderListBO orderListBO);

    List<ExpressListVO> queryAllExpress();

    boolean deleteOneOrder(Map orderId);

    OrderDetailVO queryOrderInfoBy(Integer id);

    void shipOrder(Map orderShip);

    void refundOrder(Map orderRefund);

    WxOrderVO list(WxOrderListBO orderListBO,String username);

    WxOrderDetailVO detail(Integer orderId);

    void cancel(Integer orderId);

    void delete(Integer orderId);

    void refund(int orderId);

    void confirm(int orderId);

    void prepay(int orderId);

    int reply(Map comment);


    void aftersaleSubmit(AftersaleBO aftersaleBO);

    WxAfterSaleDetail aftersaleDetail(Integer orderId);

    WxOrderDetailOrderGoodsVO goods(Integer orderId, Integer goodsId);

    void comment(WxComment wxComment);

    WxOrderSubmitVO submit(WxOrderSubmitBO wxOrderSubmitBO,String username);

    List<MarketOrder> queryOrderByStatus(int status);

    void modifyOrderStatusByOrderId(Integer id);

    WxUserIndexVO selectOrderIndex(String username);
}
