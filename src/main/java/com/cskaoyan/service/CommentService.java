package com.cskaoyan.service;

import com.cskaoyan.bean.bo.goodsbo.CommentBO;
import com.cskaoyan.bean.reverseengineering.MarketComment;
import com.cskaoyan.bean.vo.goodsvo.CommentListVO;
import com.cskaoyan.wx.bean.bo.commentbo.WxCommentBO;
import com.cskaoyan.wx.bean.bo.commentbo.WxCommentPutBO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsCountVO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsVO;

import java.util.Map;

public interface CommentService {

    CommentListVO list(CommentBO commentBO);

    void delete(Map comment);

    WxCommentsVO commentList(WxCommentBO wxCommentBO);

    WxCommentsCountVO commentCount(Integer valueId, Byte type);

    MarketComment commentPut(WxCommentPutBO wxCommentPutBO);
}
