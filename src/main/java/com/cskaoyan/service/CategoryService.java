package com.cskaoyan.service;

import com.cskaoyan.bean.bo.category.CategoryCreateBO;
import com.cskaoyan.bean.bo.category.CategoryDeleteBO;
import com.cskaoyan.bean.bo.category.CategoryUpdateBO;
import com.cskaoyan.bean.vo.category.CategoryCreateVO;
import com.cskaoyan.bean.vo.category.CategoryListVO;
import com.cskaoyan.bean.vo.category.CategorySimpleVO;
import com.cskaoyan.wx.bean.vo.catalog.WxCatalogCurrentVO;
import com.cskaoyan.wx.bean.vo.catalog.WxCatalogIndexVO;

public interface CategoryService {
    CategoryListVO queryAllCategory();

    CategorySimpleVO queryCategorySimple();

    void createCategory(CategoryCreateBO categoryCreateBO);

    CategoryCreateVO queryOneCategoryById(Integer categoryCreateBOId);

    int updateCategory(CategoryUpdateBO categoryUpdateBO);

    void deleteCategory(CategoryDeleteBO categoryDeleteBO);

    WxCatalogIndexVO queryAllCategorysAtWx();

    WxCatalogCurrentVO queryCurrentCatalog(Integer id);

}
