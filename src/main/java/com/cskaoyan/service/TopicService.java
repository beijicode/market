package com.cskaoyan.service;


import com.cskaoyan.bean.bo.topicbo.TopicBO;
import com.cskaoyan.bean.bo.topicbo.TopicIntegerBO;
import com.cskaoyan.bean.reverseengineering.MarketTopic;
import com.cskaoyan.bean.vo.promote.topicVO.TopicData;
import com.cskaoyan.bean.vo.promote.topicVO.TopicReadVO;


/**
 * 创建日期: 2022/05/06 20:05
 *
 * @author yangfan
 */
public interface TopicService {
    TopicData list(TopicBO topicBO);

    MarketTopic create(MarketTopic marketTopic);

    TopicReadVO selectByTopicId(Integer id);

    MarketTopic update(MarketTopic marketTopic);

    void delete(MarketTopic marketTopic);

    void batchDelete(TopicIntegerBO ids);
}
