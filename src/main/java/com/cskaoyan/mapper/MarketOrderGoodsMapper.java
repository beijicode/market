package com.cskaoyan.mapper;

import com.cskaoyan.bean.reverseengineering.MarketOrderGoods;
import com.cskaoyan.bean.reverseengineering.MarketOrderGoodsExample;
import com.cskaoyan.bean.po.GoodsStatPO;
import com.cskaoyan.wx.bean.po.orderpo.WxMarketOrderGoods;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailOrderGoodsVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderGoodslistVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketOrderGoodsMapper {
    long countByExample(MarketOrderGoodsExample example);

    int deleteByExample(MarketOrderGoodsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketOrderGoods record);

    int insertSelective(MarketOrderGoods record);

    List<MarketOrderGoods> selectByExample(MarketOrderGoodsExample example);

    MarketOrderGoods selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketOrderGoods record, @Param("example") MarketOrderGoodsExample example);

    int updateByExample(@Param("record") MarketOrderGoods record, @Param("example") MarketOrderGoodsExample example);

    int updateByPrimaryKeySelective(MarketOrderGoods record);

    int updateByPrimaryKey(MarketOrderGoods record);

    List<GoodsStatPO> getOrderCount();

    WxOrderGoodslistVO selectById(Integer id);

    List<WxOrderDetailOrderGoodsVO> selectByPrimaryKey2(Integer orderId);

    WxOrderDetailOrderGoodsVO selectByPrimaryKey3(@Param("orderId") Integer orderId, @Param("goodsId") Integer goodsId);

    void insert2(WxMarketOrderGoods wxMarketOrderGoods);

    Integer selectOrderStatus(int i);
}