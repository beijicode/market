package com.cskaoyan.mapper;

import com.cskaoyan.bean.vo.order.ExpressListVO;
import com.cskaoyan.bean.reverseengineering.MarketOrder;
import com.cskaoyan.bean.reverseengineering.MarketOrderExample;
import com.cskaoyan.bean.po.OrderStatPO;
import com.cskaoyan.wx.bean.po.orderpo.WxMarketOrder;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailExpressInfoVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailOrderInfoVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketOrderMapper {
    long countByExample(MarketOrderExample example);

    int deleteByExample(MarketOrderExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketOrder record);

    int insertSelective(MarketOrder record);

    List<MarketOrder> selectByExample(MarketOrderExample example);

    MarketOrder selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketOrder record, @Param("example") MarketOrderExample example);

    int updateByExample(@Param("record") MarketOrder record, @Param("example") MarketOrderExample example);

    int updateByPrimaryKeySelective(MarketOrder record);

    int updateByPrimaryKey(MarketOrder record);

    List<ExpressListVO> selectAllExpress();


    List<OrderStatPO> getOrderCount();


    WxOrderDetailOrderInfoVO selectByPrimaryKey2(Integer orderId);

    WxOrderDetailExpressInfoVO selectExpress(String shipChannel);

    WxMarketOrder selectByPrimaryKey3(Integer orderId);

    void updateByPrimaryKey2(WxMarketOrder marketOrder);

    String selectExpNameByexpCode(String expCode);

    void insert2(WxMarketOrder wxMarketOrder);


    Integer selectOrderStatus(@Param("i") int i, @Param("id") Integer id);


    Integer selectComments(Integer id);
}