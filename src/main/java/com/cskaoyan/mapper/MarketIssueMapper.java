package com.cskaoyan.mapper;

import com.cskaoyan.bean.bo.issue.IssueCreateBO;
import com.cskaoyan.bean.bo.issue.IssueUpdateBO;
import com.cskaoyan.bean.vo.issue.IssueCreateVO;
import com.cskaoyan.bean.vo.issue.IssueListIntermediateVO;
import com.cskaoyan.bean.vo.issue.IssueUpdateVO;
import com.cskaoyan.bean.reverseengineering.MarketIssue;
import com.cskaoyan.bean.reverseengineering.MarketIssueExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketIssueMapper {
    long countByExample(MarketIssueExample example);

    int deleteByExample(MarketIssueExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketIssue record);

    int insertSelective(MarketIssue record);

    List<MarketIssue> selectByExample(MarketIssueExample example);

    MarketIssue selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketIssue record, @Param("example") MarketIssueExample example);

    int updateByExample(@Param("record") MarketIssue record, @Param("example") MarketIssueExample example);

    int updateByPrimaryKeySelective(MarketIssue record);

    int updateByPrimaryKey(MarketIssue record);

    List<IssueListIntermediateVO> selectAllIssues(@Param("sort") String sort, @Param("order") String order, @Param("hasDeleted") Integer hasDeleted, @Param("question") String question);

    void insertOneIssue(@Param("issueCreateBO") IssueCreateBO issueCreateBO);

    IssueCreateVO selectIssueById(@Param("id") Integer id);

    void updateOneIssue(@Param("issueUpdateBO") IssueUpdateBO issueUpdateBO);

    IssueUpdateVO selectOneIssueById(@Param("id") Integer id);

    void deleteOneIssueById(@Param("id") Integer id, @Param("hasDeleted") Integer hasDeleted);

}