package com.cskaoyan.mapper;

import com.cskaoyan.bean.reverseengineering.MarketCategory;
import com.cskaoyan.bean.reverseengineering.MarketCategoryExample;
import com.cskaoyan.bean.bo.category.CategoryCreateBO;
import com.cskaoyan.bean.bo.category.CategoryUpdateBO;
import com.cskaoyan.bean.vo.category.CategoryCreateVO;
import com.cskaoyan.bean.vo.category.CategorySimpleIntermediateVO;
import com.cskaoyan.bean.vo.category.ChildrenOfCategoryVO;
import com.cskaoyan.bean.vo.category.ParentCategoryIntermediateVO;
import com.cskaoyan.bean.vo.goodsvo.CategoryChildren;
import com.cskaoyan.bean.vo.goodsvo.GoodsCategoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketCategoryMapper {
    long countByExample(MarketCategoryExample example);

    int deleteByExample(MarketCategoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCategory record);

    int insertSelective(MarketCategory record);

    List<MarketCategory> selectByExample(MarketCategoryExample example);

    MarketCategory selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCategory record, @Param("example") MarketCategoryExample example);

    int updateByExample(@Param("record") MarketCategory record, @Param("example") MarketCategoryExample example);

    int updateByPrimaryKeySelective(MarketCategory record);

    int updateByPrimaryKey(MarketCategory record);

    List<Integer> selectParentCategoryIdByHasParentId(@Param("hadParentId") Integer hadParentId, @Param("haeDeleted") Integer haeDeleted);

    List<ChildrenOfCategoryVO> selectChildrenByParentCategoryId(@Param("parentCategoryId") Integer parentCategoryId, @Param("haeDeleted") Integer haeDeleted);

    ParentCategoryIntermediateVO selectParentCategoryIntermediatById(@Param("id") Integer id, @Param("haeDeleted") Integer haeDeleted);

    List<CategorySimpleIntermediateVO> queryCategorySimple(@Param("hadParentId") Integer hadParentId, @Param("haeDeleted") Integer haeDeleted);

    void insertOneCategory(CategoryCreateBO categoryCreateBO);

    CategoryCreateVO selectOneCategoryById(@Param("id") Integer id);

    void updateSecondCategoryById(@Param("id") Integer id, @Param("categoryUpdateBO") CategoryUpdateBO categoryUpdateBO);

    void updateFirstCategoryById(@Param("id") Integer id, @Param("categoryUpdateBO") CategoryUpdateBO categoryUpdateBO);

    List<Integer> selectChildrenIdByParentId(@Param("id") Integer id);

    void updateChildrenPIdById(@Param("id") Integer id, @Param("newParentId") Integer newParentId);

    void updateDeleteStatusByCategoryId(@Param("id") Integer id);

    List<GoodsCategoryVO> selectAll();

    List<CategoryChildren> selectChildren(Integer value);
}
