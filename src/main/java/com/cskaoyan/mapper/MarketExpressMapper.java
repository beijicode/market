package com.cskaoyan.mapper;

import org.apache.ibatis.annotations.Param;

public interface MarketExpressMapper {

    String getNameByCode(@Param("code") String code);
}
