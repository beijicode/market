package com.cskaoyan.mapper;

import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.bean.reverseengineering.MarketUserExample;
import com.cskaoyan.bean.po.UserStatPO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketUserMapper {
    long countByExample(MarketUserExample example);

    int deleteByExample(MarketUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketUser record);

    int insertSelective(MarketUser record);

    List<MarketUser> selectByExample(MarketUserExample example);

    MarketUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketUser record, @Param("example") MarketUserExample example);

    int updateByExample(@Param("record") MarketUser record, @Param("example") MarketUserExample example);

    int updateByPrimaryKeySelective(MarketUser record);

    int updateByPrimaryKey(MarketUser record);

    List<UserStatPO> getUserCount();

    MarketUser queryByUsername(String username);

    Integer selectByusername(String username);
}
