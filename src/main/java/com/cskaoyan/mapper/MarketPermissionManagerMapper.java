package com.cskaoyan.mapper;

import com.cskaoyan.bean.reverseengineering.MarketPermission;
import com.cskaoyan.bean.vo.permissionvo.ChildrenVO;
import com.cskaoyan.bean.vo.permissionvo.NextChildrenVO;
import com.cskaoyan.bean.vo.permissionvo.SystemPermissionsVO;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/9 22:57
 */
public interface MarketPermissionManagerMapper {
    List<SystemPermissionsVO> selectOutPermission();

    List<NextChildrenVO> selectMiddlePermissionsByPid(Integer tId);

    List<ChildrenVO> selectLastPermissionsByPid(Integer pId);

    String[] selectAllIdByRoleId(Integer roleId);

    List<MarketPermission> selectRolePermissionByRoleId(Integer roleId);

    String[] selectAllPermissionFromPermissionManageTable();
}
