package com.cskaoyan.mapper;

import com.cskaoyan.bean.login_demo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<User> select(@Param("sort") String sort, @Param("order") String order, @Param("username") String username, @Param("mobile") String mobile);


    int dashboard1();

    int dashboard2();

    int dashboard3();

    int dashboard4();


    Integer queryUserIdByUsername(String username);
}
