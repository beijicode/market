package com.cskaoyan.mapper;


import com.cskaoyan.bean.reverseengineering.PermissionManage;
import com.cskaoyan.bean.reverseengineering.PermissionManageExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PermissionManageMapper {
    long countByExample(PermissionManageExample example);

    int deleteByExample(PermissionManageExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(PermissionManage record);

    int insertSelective(PermissionManage record);

    List<PermissionManage> selectByExample(PermissionManageExample example);

    PermissionManage selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") PermissionManage record, @Param("example") PermissionManageExample example);

    int updateByExample(@Param("record") PermissionManage record, @Param("example") PermissionManageExample example);

    int updateByPrimaryKeySelective(PermissionManage record);

    int updateByPrimaryKey(PermissionManage record);
}