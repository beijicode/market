package com.cskaoyan.wx.scheduletask;

import com.cskaoyan.bean.reverseengineering.*;
import com.cskaoyan.mapper.MarketCouponMapper;
import com.cskaoyan.mapper.MarketCouponUserMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.service.OrderService;
import com.cskaoyan.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;


import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/10 14:18
 */
@Component
public class ScheduleTask {

    @Autowired
    OrderService orderService;

    @Autowired
    UserService userService;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Autowired
    MarketCouponMapper marketCouponMapper;

    @Autowired
    MarketCouponUserMapper marketCouponUserMapper;

    /**
     * 定时扫描所有未付款订单，修改订单状态，即取消订单
     */
    @Scheduled(fixedRate = 10000)// 每隔10s扫描一次
    public void checkOrder() {

        // ===================== lxc 商家确认发货 用户超过5分钟未确认收货 系统自动确认 ===============
        // 把订单状态为301的订单全取出来
        List<MarketOrder> orders = orderService.queryOrderByStatus(301);
        // 获取当前时间
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long current = timestamp.getTime();
        // 将订单循环出来
        for (MarketOrder order : orders) {
            Date shipDate = order.getShipTime();
            long confirmTime = shipDate.getTime();
            if ((current - confirmTime) > 120000) {
                // 如果时间大于两分钟,则系统自动确认，修改订单状态为已收货(401)
                orderService.modifyOrderStatusByOrderId(order.getId());
            }
        }
    }

    /**
     * 定时扫描优惠券，把过期的优惠券状态改了
     */
    @Scheduled(initialDelay = 1000, fixedRate = 86400000)// 启动后延迟一秒扫描，后每隔24h扫描一次
    public void checkCouponDate() {
        MarketCouponExample marketCouponExample = new MarketCouponExample();
        List<MarketCoupon> couponList = marketCouponMapper.selectByExample(marketCouponExample);
        for (MarketCoupon marketCoupon : couponList) {
            if (marketCoupon.getEndTime() == null) {
                continue;
            }
            if (marketCoupon.getEndTime().getTime() <= System.currentTimeMillis()) {
                marketCoupon.setStatus((short) 1);
                marketCoupon.setUpdateTime(new Date());
                marketCouponMapper.updateByPrimaryKeySelective(marketCoupon);
            }
        }

        MarketCouponUserExample marketCouponUserExample = new MarketCouponUserExample();
        List<MarketCouponUser> couponUserList = marketCouponUserMapper.selectByExample(marketCouponUserExample);
        for (MarketCouponUser marketCouponUser : couponUserList) {
            try {
                if (marketCouponUser.getEndTime().getTime() <= System.currentTimeMillis()) {
                    marketCouponUser.setStatus((short) 2);
                    marketCouponUser.setUpdateTime(new Date());
                    marketCouponUserMapper.updateByPrimaryKeySelective(marketCouponUser);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("market_coupon_user表中id为:" + marketCouponUser.getId() + "的end_time为null");
            }

        }
    }
}
