package com.cskaoyan.wx.bean.po.orderpo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 创建日期: 2022/05/09 16:14
 *
 * @author yangfan
 */
public class TimeUtils {
    public static String timeTrans(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
        return dateFormat.format(date);
    }
}
