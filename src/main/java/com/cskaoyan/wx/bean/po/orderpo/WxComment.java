package com.cskaoyan.wx.bean.po.orderpo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建日期: 2022/05/09 19:41
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxComment {

    private String content;

    private boolean hasPicture;

    private Integer orderGoodsId;

    private String[] picUrls;

    private short star;
}
