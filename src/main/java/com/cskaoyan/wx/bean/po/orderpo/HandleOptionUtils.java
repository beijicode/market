package com.cskaoyan.wx.bean.po.orderpo;

import com.cskaoyan.mapper.MarketOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 创建日期: 2022/05/08 09:54
 *
 * @author yangfan
 */
public class HandleOptionUtils {

    public static HandleOption hanle(Integer orderStatus) {

        HandleOption handleOption = new HandleOption();
        // 未付款
        if (orderStatus == 101) {
            handleOption.setCancel(true);
            handleOption.setPay(true);
        }
        // 用户取消
        if (orderStatus == 102) {
            handleOption.setDelete(true);
        }
        // 系统取消
        if (orderStatus == 103) {
            handleOption.setDelete(true);
        }
        // 已付款
        if (orderStatus == 201) {
            handleOption.setRefund(true);
        }
        // 申请退款
        // if (orderStatus == 202) {
        //
        // }

        // 已退款

        // 已发货
        if (orderStatus == 301) {
            handleOption.setConfirm(true);
        }
        // 用户收货
        if (orderStatus == 401) {
            handleOption.setAftersale(true);
            handleOption.setRebuy(true);
            handleOption.setDelete(true);
            handleOption.setComment(true);
        }

        // 系统收货
        if (orderStatus == 402) {
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
            handleOption.setComment(true);
            handleOption.setAftersale(true);
        }

        return handleOption;

    }

    public static String orderStatus(Integer orderStatus){
        if (orderStatus == 101){
            return "未付款";
        }
        if (orderStatus == 102){
            return "用户取消";
        }
        if (orderStatus == 103){
            return "系统取消";
        }
        if (orderStatus == 201){
            return "已付款";
        }
        if (orderStatus == 202){
            return "申请退款";
        }
        if (orderStatus == 203){
            return "已退款";
        }
        if (orderStatus == 301){
            return "已发货";
        }
        if (orderStatus == 401){
            return "用户收货";
        }
        else {
            return "系统收货";
        }
    }
}
