package com.cskaoyan.wx.bean.po.orderpo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 创建日期: 2022/05/10 13:15
 *
 * @author yangfan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxMarketCart {
    private Integer id;

    private Integer userId;

    private Integer goodsId;

    private String goodsSn;

    private String goodsName;

    private Integer productId;

    private BigDecimal price;

    private Integer number;

    private String[] specifications;

    private Boolean checked;

    private String picUrl;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}
