package com.cskaoyan.wx.bean.po.orderpo;

import com.cskaoyan.mapper.MarketOrderMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 创建日期: 2022/05/07 17:29
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HandleOption {

    // 此层反应的是标签，如果有标签则反应为true

    private Boolean aftersale = false;

    private Boolean cancel = false;

    private Boolean comment = false;

    private Boolean confirm = false;

    private Boolean delete = false;

    private Boolean pay = false;

    private Boolean rebuy = false;

    private Boolean refund = false;


}
