package com.cskaoyan.wx.bean.vo.commentvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 19:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCommentsUserListsVO {

    private WxCommentsUserInfo userInfo;
    private Date addTime;
    private String[] picList;
    private String adminContent;
    private String content;
}
