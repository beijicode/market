package com.cskaoyan.wx.bean.vo;

import com.cskaoyan.bean.reverseengineering.MarketCoupon;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/07 22:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCouponListVO {
    Integer limit;
    Integer page;
    List<MarketCouponVO> list;
    Integer pages;
    Integer total;
}
