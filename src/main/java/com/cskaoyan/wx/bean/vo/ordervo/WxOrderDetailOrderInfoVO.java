package com.cskaoyan.wx.bean.vo.ordervo;

import com.cskaoyan.wx.bean.po.orderpo.HandleOption;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 创建日期: 2022/05/08 12:54
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxOrderDetailOrderInfoVO {
    private Integer id;

    private Integer userId;

    private String orderSn;

    private Short orderStatus;

    private Short aftersaleStatus;

    private String consignee;

    private String mobile;

    private String address;

    private String message;

    private BigDecimal goodsPrice;

    private BigDecimal freightPrice;

    private BigDecimal couponPrice;

    private BigDecimal integralPrice;

    private BigDecimal grouponPrice;

    private BigDecimal orderPrice;

    private BigDecimal actualPrice;

    private String payId;

    private Date payTime;

    // 快递公司名称
    private String expName;

    // 快递公司代码
    private String expCode;

    // 快递编号
    private String expNo;

    private Date shipTime;

    private BigDecimal refundAmount;

    private String refundType;

    private String refundContent;

    private Date refundTime;

    private Date confirmTime;

    private Short comments;

    private Date endTime;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    private HandleOption handleOption;

    private String orderStatusText;
}
