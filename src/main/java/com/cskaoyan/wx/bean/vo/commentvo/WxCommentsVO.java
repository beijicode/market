package com.cskaoyan.wx.bean.vo.commentvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 19:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCommentsVO {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private List<WxCommentsUserListsVO> list;
}
