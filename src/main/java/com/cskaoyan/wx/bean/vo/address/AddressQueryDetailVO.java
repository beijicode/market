package com.cskaoyan.wx.bean.vo.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 *
 * @since 2022/05/08 15:13
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressQueryDetailVO {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    private String addressDetail;
    private String areaCode;
    private String city;
    private String county;
    private Boolean deleted;
    private Integer id;
    private Boolean IsDefault;
    private String name;
    private String province;
    private String tel;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;
    private Integer userId;
}