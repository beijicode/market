package com.cskaoyan.wx.bean.vo.commentvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 19:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCommentsUserInfo {

    private String nickName;
    private String avatarUrl;

}
