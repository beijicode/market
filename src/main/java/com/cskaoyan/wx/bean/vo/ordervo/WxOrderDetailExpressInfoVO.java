package com.cskaoyan.wx.bean.vo.ordervo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建日期: 2022/05/08 12:53
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxOrderDetailExpressInfoVO {

    // private Integer id;

    // private String code;

    private String[] name;

}
