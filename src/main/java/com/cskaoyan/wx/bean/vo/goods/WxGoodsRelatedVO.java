package com.cskaoyan.wx.bean.vo.goods;

import com.cskaoyan.bean.reverseengineering.MarketGoods;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/09 16:48
 * @author: beiji
 */
@Data
public class WxGoodsRelatedVO {
    private Integer limit;
    private Integer page;
    private Integer pages;
    private Integer total;
    private List<MarketGoods> list;
}
