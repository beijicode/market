package com.cskaoyan.wx.bean.vo;

import com.github.pagehelper.PageHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/07 16:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxFootprintListVO {
    Integer limit;
    List<WxFootprintAndGoodsVO> list;
    Integer page;
    Integer pages;
    Integer total;
}
