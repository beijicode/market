package com.cskaoyan.wx.bean.vo.goods;

import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/08 23:52
 * @author: beiji
 */
@Data
public class WxGoodsDetailCommentVO {
    private Integer count;
    List<WxCommentVO> data;
}
