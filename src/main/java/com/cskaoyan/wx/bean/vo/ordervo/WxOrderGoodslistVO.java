package com.cskaoyan.wx.bean.vo.ordervo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 创建日期: 2022/05/07 22:38
 *
 * @author yangfan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxOrderGoodslistVO {

    private Integer id;

    private String goodsName;

    private Short number;

    private BigDecimal price;

    private String[] specifications;

    private String picUrl;

}
