package com.cskaoyan.wx.bean.vo.goods;

import com.cskaoyan.bean.reverseengineering.MarketCategory;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/08 17:51
 * @author: beiji
 */
@Data
public class WxCategoryVO {
    private List<MarketCategory> brotherCategory;
    private MarketCategory currentCategory;
    private MarketCategory parentCategory;
}
