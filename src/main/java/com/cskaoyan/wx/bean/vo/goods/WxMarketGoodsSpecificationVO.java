package com.cskaoyan.wx.bean.vo.goods;

import com.cskaoyan.bean.reverseengineering.MarketGoodsSpecification;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/09 09:01
 * @author: beiji
 */
@Data
public class WxMarketGoodsSpecificationVO {
    private String name;
    private List<MarketGoodsSpecification> valueList;
}
