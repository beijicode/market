package com.cskaoyan.wx.bean.vo.issue;

import com.cskaoyan.bean.reverseengineering.MarketIssue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * 
 * @since 2022/05/10 22:51
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssuesInWxQueryVO {
    private long total;
    private int pages;
    private int limit;
    private int page;

    private List<MarketIssue> list;
}