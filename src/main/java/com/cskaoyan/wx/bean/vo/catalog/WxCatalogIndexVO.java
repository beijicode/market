package com.cskaoyan.wx.bean.vo.catalog;

import com.cskaoyan.bean.reverseengineering.MarketCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/09 16:00
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCatalogIndexVO {
    List<MarketCategory> categoryList;
    MarketCategory currentCategory;
    List<MarketCategory> currentSubCategory;
}