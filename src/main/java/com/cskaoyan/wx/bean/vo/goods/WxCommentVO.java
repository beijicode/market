package com.cskaoyan.wx.bean.vo.goods;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 创建日期: 2022/05/11 14:59
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCommentVO {

    private Integer id;

    private Date addTime;

    private String adminContent;

    private Integer avatar;

    private String content;

    private String nickname;

    private String[] picList;


}
