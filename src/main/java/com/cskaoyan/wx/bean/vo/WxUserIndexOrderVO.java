package com.cskaoyan.wx.bean.vo;

import lombok.Data;

/**
 * @description:
 * @date: 2022/05/10 17:46
 * @author: beiji
 */
@Data
public class WxUserIndexOrderVO {

    private Integer unpaid;
    private Integer unship;
    private Integer unrecv;
    private Integer uncomment;
}
