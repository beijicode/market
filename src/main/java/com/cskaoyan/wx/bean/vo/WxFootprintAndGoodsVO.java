package com.cskaoyan.wx.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/07 16:56
 */
@Data
public class WxFootprintAndGoodsVO {
    Integer id;
    Date addTime;
    String brief;
    Integer goodsId;
    String name;
    String picUrl;
    Integer retailPrice;
}
