package com.cskaoyan.wx.bean.vo.ordervo;

import com.cskaoyan.wx.bean.po.orderpo.HandleOption;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 创建日期: 2022/05/07 22:40
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxOrderListVO {
    private Integer id;

    private String orderSn;

    private String orderStatusText;

    private Short aftersaleStatus;

    private BigDecimal actualPrice;

    private boolean isGroupin = false;

    private List<WxOrderDetailOrderGoodsVO> goodsList;

    private HandleOption handleOption;


}
