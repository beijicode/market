package com.cskaoyan.wx.bean.vo.commentvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 21:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCommentsCountVO {

    private Integer hasPicCount;
    private Integer allCount;

}
