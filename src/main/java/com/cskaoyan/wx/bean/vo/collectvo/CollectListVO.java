package com.cskaoyan.wx.bean.vo.collectvo;

import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public class CollectListVO {

    /**
     * total : 16
     * pages : 2
     * limit : 10
     * page : 1
     * list : [{"brief":"我去饿","picUrl":"http://182.92.235.201:8083/wx/storage/fetch/okschj5po1c6k0fibv6u.JPG","valueId":1181034,"name":"实打实","id":26,"type":0,"retailPrice":1231}]
     */
    private Long total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListEntity> list = new LinkedList<>();



    public List<ListEntity> addList() {
        ListEntity listEntity = new ListEntity();
        list.add(listEntity);
        return list;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setList(List<ListEntity> list) {
        this.list = list;
    }

    public Long getTotal() {
        return total;
    }

    public Integer getPages() {
        return pages;
    }

    public Integer getLimit() {
        return limit;
    }

    public Integer getPage() {
        return page;
    }

    public List<ListEntity> getList() {
        return list;
    }

    public class ListEntity {
        /**
         * brief : 我去饿
         * picUrl : http://182.92.235.201:8083/wx/storage/fetch/okschj5po1c6k0fibv6u.JPG
         * valueId : 1181034
         * name : 实打实
         * id : 26
         * type : 0
         * retailPrice : 1231.0
         */
        private String brief;
        private String picUrl;
        private Integer valueId;
        private String name;
        private Integer id;
        private Byte type;
        private double retailPrice;

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public void setValueId(Integer valueId) {
            this.valueId = valueId;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public void setType(Byte type) {
            this.type = type;
        }

        public void setRetailPrice(double retailPrice) {
            this.retailPrice = retailPrice;
        }

        public String getBrief() {
            return brief;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public Integer getValueId() {
            return valueId;
        }

        public String getName() {
            return name;
        }

        public Integer getId() {
            return id;
        }

        public Byte getType() {
            return type;
        }

        public double getRetailPrice() {
            return retailPrice;
        }
    }
}
