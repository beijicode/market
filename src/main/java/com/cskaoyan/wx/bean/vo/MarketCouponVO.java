package com.cskaoyan.wx.bean.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Charadic
 * @since 2022/05/10 10:56
 */
@Data
public class MarketCouponVO {
    Integer days;
    String desc;
    BigDecimal discount;
    Integer id;
    BigDecimal min;
    String name;
    String tag;
}
