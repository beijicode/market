package com.cskaoyan.wx.bean.vo.cartvo;

import com.cskaoyan.bean.reverseengineering.MarketAddress;
import com.cskaoyan.bean.reverseengineering.MarketCart;

import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/9
 */
public class CartCheckOutVO {

    /**
     * grouponRulesId : 0
     * actualPrice : 3.0
     * orderTotalPrice : 3.0
     * cartId : 161
     * userCouponId : 0
     * couponId : 0
     * goodsTotalPrice : 1.0
     * addressId : 21
     * grouponPrice : 0
     * checkedAddress : {"addTime":"2022-05-08 16:18:06","city":"唐山市","county":"路北区","updateTime":"2022-05-08 16:51:07","userId":1,"areaCode":"130203","isDefault":true,"addressDetail":"广场路","deleted":false,"province":"河北省","name":"小王","tel":"12366665555","id":21}
     * couponPrice : 0
     * availableCouponLength : 0
     * freightPrice : 2
     * checkedGoodsList : [{"productId":351,"addTime":"2022-05-08 15:10:38","goodsId":1181102,"goodsSn":"薛欣宇test","updateTime":"2022-05-08 17:10:12","userId":1,"specifications":["标准"],"number":1,"picUrl":"http://182.92.235.201:8083/wx/storage/fetch/d3ml3d6mpimy6i0x9g3p.jpg","deleted":false,"price":1,"checked":false,"id":161,"goodsName":"test"}]
     */
    private Integer grouponRulesId;
    private Double actualPrice;
    private Double orderTotalPrice;
    private Integer cartId;
    private Integer userCouponId;
    private Integer couponId;
    private Double goodsTotalPrice;
    private Integer addressId;
    private Integer grouponPrice;
    private MarketAddress checkedAddress;
    private Integer couponPrice;
    private Integer availableCouponLength;
    private Integer freightPrice;
    private List<MarketCart> checkedGoodsList;

    public void setGrouponRulesId(Integer grouponRulesId) {
        this.grouponRulesId = grouponRulesId;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public void setOrderTotalPrice(Double orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public void setUserCouponId(Integer userCouponId) {
        this.userCouponId = userCouponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public void setGoodsTotalPrice(Double goodsTotalPrice) {
        this.goodsTotalPrice = goodsTotalPrice;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public void setGrouponPrice(Integer grouponPrice) {
        this.grouponPrice = grouponPrice;
    }

    public void setCheckedAddress(MarketAddress checkedAddress) {
        this.checkedAddress = checkedAddress;
    }

    public void setCouponPrice(Integer couponPrice) {
        this.couponPrice = couponPrice;
    }

    public void setAvailableCouponLength(Integer availableCouponLength) {
        this.availableCouponLength = availableCouponLength;
    }

    public void setFreightPrice(Integer freightPrice) {
        this.freightPrice = freightPrice;
    }

    public void setCheckedGoodsList(List<MarketCart> checkedGoodsList) {
        this.checkedGoodsList = checkedGoodsList;
    }

    public Integer getGrouponRulesId() {
        return grouponRulesId;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public Double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public Integer getCartId() {
        return cartId;
    }

    public Integer getUserCouponId() {
        return userCouponId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public Double getGoodsTotalPrice() {
        return goodsTotalPrice;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public Integer getGrouponPrice() {
        return grouponPrice;
    }

    public MarketAddress getCheckedAddress() {
        return checkedAddress;
    }

    public Integer getCouponPrice() {
        return couponPrice;
    }

    public Integer getAvailableCouponLength() {
        return availableCouponLength;
    }

    public Integer getFreightPrice() {
        return freightPrice;
    }

    public List<MarketCart> getCheckedGoodsList() {
        return checkedGoodsList;
    }

}
