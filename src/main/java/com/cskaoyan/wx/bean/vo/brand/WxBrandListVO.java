package com.cskaoyan.wx.bean.vo.brand;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/08 23:51
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxBrandListVO {

    private Long total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxBrandListDetailVO> list;


}