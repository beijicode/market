package com.cskaoyan.wx.bean.vo.goods;

import com.cskaoyan.bean.reverseengineering.*;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/08 20:49
 * @author: beiji
 */
@Data
public class WxGoodsDetailVO {
    private List<MarketGoodsAttribute> attribute;
    private MarketBrand brand;
    private WxGoodsDetailCommentVO comment;
    private List<MarketGroupon> groupon;
    private MarketGoods info;
    private List<MarketIssue> issue;
    private List<MarketGoodsProduct> productList;
    private Boolean share;
    private String shareImage;
    private List<WxMarketGoodsSpecificationVO> specificationList;
    private Integer userHasCollect;
}
