package com.cskaoyan.wx.bean.vo.ordervo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/08 12:50
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxOrderDetailVO {

    private List<WxOrderDetailExpressInfoVO> expressInfo;

    private List<WxOrderDetailOrderGoodsVO> orderGoods;

    private WxOrderDetailOrderInfoVO orderInfo;

}
