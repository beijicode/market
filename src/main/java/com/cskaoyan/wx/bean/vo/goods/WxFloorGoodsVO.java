package com.cskaoyan.wx.bean.vo.goods;

import com.cskaoyan.bean.reverseengineering.MarketGoods;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/07 17:27
 * @author: beiji
 */

@Data
public class WxFloorGoodsVO {
    private Integer id;
    private String name;
    private List<MarketGoods> goodsList;
}
