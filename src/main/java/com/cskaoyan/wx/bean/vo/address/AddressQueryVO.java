package com.cskaoyan.wx.bean.vo.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/08 15:06
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressQueryVO {
    private Long total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码

    private List<AddressQueryDetailVO> list;
    
}