package com.cskaoyan.wx.bean.vo;

import com.cskaoyan.bean.reverseengineering.MarketCoupon;
import com.cskaoyan.bean.reverseengineering.MarketCouponUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/07 22:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCouponUserListVO {
    Integer limit;
    Integer page;
    List<MarketCouponUserVO> list;
    Integer pages;
    Integer total;
}
