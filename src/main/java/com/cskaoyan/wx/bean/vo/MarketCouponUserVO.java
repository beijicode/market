package com.cskaoyan.wx.bean.vo;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/10 10:52
 */
@Data
public class MarketCouponUserVO {
    Boolean available = false;
    Integer cid;
    String desc;
    BigDecimal discount;
    Date endTime;
    Integer id;
    BigDecimal min;
    String name;
    Date startTime;
    String tag;

}
