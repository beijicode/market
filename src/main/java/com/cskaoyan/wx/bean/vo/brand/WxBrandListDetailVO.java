package com.cskaoyan.wx.bean.vo.brand;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 
 * @since 2022/05/08 23:52
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxBrandListDetailVO {

    private String desc;
    private BigDecimal floorPrice;
    private Integer id;
    private String name;
    private String picUrl;
}
