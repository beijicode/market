package com.cskaoyan.wx.bean.vo.searchvo;

import com.cskaoyan.bean.reverseengineering.MarketKeyword;
import com.cskaoyan.bean.reverseengineering.MarketSearchHistory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/7 17:28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchIndexVO {

    MarketKeyword defaultKeyword;
    List<MarketKeyword> hotKeywordList;
    List<MarketSearchHistory> historyKeywordList;
}
