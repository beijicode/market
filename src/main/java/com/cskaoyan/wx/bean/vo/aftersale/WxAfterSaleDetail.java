package com.cskaoyan.wx.bean.vo.aftersale;

import com.cskaoyan.bean.reverseengineering.MarketAftersale;
import com.cskaoyan.wx.bean.po.orderpo.WxMarketOrder;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailOrderGoodsVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/09 17:01
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxAfterSaleDetail {

    private MarketAftersale aftersale;

    private WxMarketOrder order;

    private List<WxOrderDetailOrderGoodsVO> orderGoods;
}
