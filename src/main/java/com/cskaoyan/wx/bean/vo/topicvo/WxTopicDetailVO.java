package com.cskaoyan.wx.bean.vo.topicvo;

import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketTopic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 17:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxTopicDetailVO {

    private List<MarketGoods> goods;
    private MarketTopic topic;
}
