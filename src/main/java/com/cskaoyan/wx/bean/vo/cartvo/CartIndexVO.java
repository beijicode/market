package com.cskaoyan.wx.bean.vo.cartvo;

import com.cskaoyan.bean.reverseengineering.MarketCart;

import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public class CartIndexVO {

    /**
     * cartTotal : {"goodsCount":7,"checkedGoodsCount":7,"goodsAmount":847,"checkedGoodsAmount":847}
     * cartList : [{"productId":303,"addTime":"2022-05-08 11:23:56","goodsId":1181055,"goodsSn":"1005691","updateTime":"2022-05-08 14:00:22","userId":1,"specifications":["S"],"number":3,"picUrl":"http://182.92.235.201:8083/wx/storage/fetch/08isyo7vp0m4v1jwz2c6.jpg","deleted":false,"price":169,"checked":true,"id":129,"goodsName":"Shark"},{"productId":351,"addTime":"2022-05-08 12:33:03","goodsId":1181102,"goodsSn":"薛欣宇test","updateTime":"2022-05-08 14:06:33","userId":1,"specifications":["标准"],"number":2,"picUrl":"","deleted":false,"price":1,"checked":true,"id":133,"goodsName":"test"},{"productId":304,"addTime":"2022-05-08 12:49:17","goodsId":1181055,"goodsSn":"1005691","updateTime":"2022-05-08 14:07:36","userId":1,"specifications":["M"],"number":2,"picUrl":"http://182.92.235.201:8083/wx/storage/fetch/h4q1on6uzta0ii3rsuvr.jpg","deleted":false,"price":169,"checked":true,"id":134,"goodsName":"Shark"}]
     */
    private CartTotalEntity cartTotal = new CartTotalEntity();
    private List<MarketCart> cartList;

    public void setCartTotal(CartTotalEntity cartTotal) {
        this.cartTotal = cartTotal;
    }

    public void setCartList(List<MarketCart> cartList) {
        this.cartList = cartList;
    }

    public CartTotalEntity getCartTotal() {
        return cartTotal;
    }

    public List<MarketCart> getCartList() {
        return cartList;
    }

    public class CartTotalEntity {
        /**
         * goodsCount : 7
         * checkedGoodsCount : 7
         * goodsAmount : 847.0
         * checkedGoodsAmount : 847.0
         */
        private int goodsCount;
        private int checkedGoodsCount;
        private double goodsAmount;
        private double checkedGoodsAmount;

        public void setGoodsCount(int goodsCount) {
            this.goodsCount = goodsCount;
        }

        public void setCheckedGoodsCount(int checkedGoodsCount) {
            this.checkedGoodsCount = checkedGoodsCount;
        }

        public void setGoodsAmount(double goodsAmount) {
            this.goodsAmount = goodsAmount;
        }

        public void setCheckedGoodsAmount(double checkedGoodsAmount) {
            this.checkedGoodsAmount = checkedGoodsAmount;
        }

        public int getGoodsCount() {
            return goodsCount;
        }

        public int getCheckedGoodsCount() {
            return checkedGoodsCount;
        }

        public double getGoodsAmount() {
            return goodsAmount;
        }

        public double getCheckedGoodsAmount() {
            return checkedGoodsAmount;
        }
    }

}
