package com.cskaoyan.wx.bean.vo.goods;

import com.cskaoyan.bean.reverseengineering.*;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @date: 2022/05/07 16:46
 * @author: beiji
 */
@Data
public class WxHomeIndexVO {
    private List<MarketAd> banner;
    private List<MarketBrand> brandList;
    private List<MarketCategory> channel;
    private List<MarketCoupon> couponList;
    private List<WxFloorGoodsVO> floorGoodsList;

    //TODO 团购表没有查询
    private List<Object> grouponList;

    private List<MarketGoods> hotGoodsList;
    private List<MarketGoods> newGoodsList;
    private List<MarketTopic> topicList;
}
