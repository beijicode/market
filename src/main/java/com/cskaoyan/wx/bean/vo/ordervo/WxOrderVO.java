package com.cskaoyan.wx.bean.vo.ordervo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/07 22:47
 *
 * @author yangfan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxOrderVO {

    private List<WxOrderListVO> list;

    private Integer limit;

    private Integer page;

    private Integer pages;

    private Integer total;

}
