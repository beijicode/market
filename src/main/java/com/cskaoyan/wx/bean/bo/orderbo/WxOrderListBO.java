package com.cskaoyan.wx.bean.bo.orderbo;

import lombok.Data;

/**
 * 创建日期: 2022/05/07 22:49
 *
 * @author yangfan
 */
@Data
public class WxOrderListBO {

    private Integer showType;

    private Integer page;

    private Integer limit;

}
