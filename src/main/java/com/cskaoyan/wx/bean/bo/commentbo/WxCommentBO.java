package com.cskaoyan.wx.bean.bo.commentbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 18:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxCommentBO {

    private Integer valueId;
    private Byte type;
    private Integer showType;
    private Integer page;
    private Integer limit;
}
