package com.cskaoyan.wx.bean.bo.collectbo;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/9
 */
public class CollectBO {

    /**
     * valueId : 1152008
     * type : 0
     */
    private Integer valueId;
    private Byte type;

    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Integer getValueId() {
        return valueId;
    }

    public Byte getType() {
        return type;
    }
}
