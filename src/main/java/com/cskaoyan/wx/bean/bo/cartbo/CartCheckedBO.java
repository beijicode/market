package com.cskaoyan.wx.bean.bo.cartbo;

import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public class CartCheckedBO {

    /**
     * productIds : [303,351,304]
     * isChecked : 0
     */
    private List<Integer> productIds;
    private Integer isChecked;

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public void setIsChecked(Integer isChecked) {
        this.isChecked = isChecked;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }

    public Integer getIsChecked() {
        return isChecked;
    }
}
