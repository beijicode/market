package com.cskaoyan.wx.bean.bo.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 
 * @since 2022/05/08 17:02
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressSaveBO {

    @NotNull
    private String addressDetail;
    private String areaCode;
    private String city;
    private String county;
    private Integer id;
    private Boolean isDefault;
    private String name;
    private String province;
    // @Pattern(regexp = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\\\\d{8}$")
    @Pattern(regexp = "((13[0-9])|(14[589])|(15[0-9])|(16[689])|(17[79])|(18[0-9])|(199))\\d{8}")
    private String tel;
    private String username;

    // @DateTimeFormat(pattern = "yyyy-MM-dd")
    // private Date updateTime;
}