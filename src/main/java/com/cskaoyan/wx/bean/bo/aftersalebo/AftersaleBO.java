package com.cskaoyan.wx.bean.bo.aftersalebo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 创建日期: 2022/05/09 15:21
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AftersaleBO {

    private BigDecimal amount;

    private String orderId;

    private String[] pictures;

    private String reason;

    private Integer type;

    private String typeDesc;

}
