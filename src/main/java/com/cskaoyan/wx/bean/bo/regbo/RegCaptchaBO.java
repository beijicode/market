package com.cskaoyan.wx.bean.bo.regbo;

import lombok.Data;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 9:48
 */
@Data
public class RegCaptchaBO {

    private String mobile;
}
