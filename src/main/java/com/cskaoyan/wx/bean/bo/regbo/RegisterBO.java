package com.cskaoyan.wx.bean.bo.regbo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 10:09
 */
@Data
public class RegisterBO {

    private String code;
    private String mobile;
    private String password;
    private String username;
    private String wxCode;
}
