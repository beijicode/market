package com.cskaoyan.wx.bean.bo.commentbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 21:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxCommentPutBO {

    private Byte type;
    private String valueId;
    private String content;
    private Short star;
    private Boolean hasPicture;
    private String[] picUrls;

}
