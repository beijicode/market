package com.cskaoyan.wx.bean.bo.cartbo;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/9
 */
public class CartAddBO {

    /**
     * number : 1
     * productId : 351
     * goodsId : 1181102
     */
    private Integer number;
    private Integer productId;
    private Integer goodsId;

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getProductId() {
        return productId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }
}
