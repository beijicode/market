package com.cskaoyan.wx.bean.bo.cartbo;

import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public class CartDeleteBO {

    /**
     * productIds : [20]
     */
    private List<Integer> productIds;

    public void setProductIds(List<Integer> productIds) {
        this.productIds = productIds;
    }

    public List<Integer> getProductIds() {
        return productIds;
    }
}
