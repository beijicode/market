package com.cskaoyan.wx.bean.bo.cartbo;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public class CartUpdateBO {

    /**
     * number : 1
     * productId : 1
     * goodsId : 3
     * id : 1
     */
    private Integer number;
    private Integer productId;
    private Integer goodsId;
    private Integer id;

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public Integer getProductId() {
        return productId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public Integer getId() {
        return id;
    }
}
