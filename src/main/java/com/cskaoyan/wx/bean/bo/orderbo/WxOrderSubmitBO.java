package com.cskaoyan.wx.bean.bo.orderbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 创建日期: 2022/05/09 22:06
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxOrderSubmitBO {

    private Integer addressId;

    private Integer cartId;

    private Integer couponId;

    private Integer grouponLinkId = 0;

    private Integer grouponRulesId = 0;

    private String message;

    private Integer userCouponId;
}
