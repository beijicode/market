package com.cskaoyan.wx.bean;

import lombok.Data;

@Data
public class WxAdminInfoBean {
    /**
     * nickName : user123
     * avatarUrl : ""
     */

    private String nickName;
    private String avatarUrl;
}
