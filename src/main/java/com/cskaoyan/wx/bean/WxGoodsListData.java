package com.cskaoyan.wx.bean;

import com.cskaoyan.bean.reverseengineering.MarketCategory;
import com.cskaoyan.bean.reverseengineering.MarketGoods;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author beiji
 * @date 2022/05/06 16:28
 */
@NoArgsConstructor
@Data
public class WxGoodsListData {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketGoods> list; //当前页的数据
    private List<MarketCategory> filterCategoryList;
    //alt + s

    public static WxGoodsListData data(long total, Integer pages, Integer limit, Integer page, List list, List filterCategoryList) {
        WxGoodsListData userData = new WxGoodsListData();
        userData.setTotal((int) total);
        userData.setPages(pages);
        userData.setPage(page);
        userData.setLimit(limit);
        userData.setList(list);
        userData.setFilterCategoryList(filterCategoryList);
        return userData;
    }
}
