package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.service.OrderService;
import com.cskaoyan.wx.bean.WxAdminInfoBean;
import com.cskaoyan.wx.bean.WxLoginUserData;
import com.cskaoyan.wx.bean.vo.WxUserIndexVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description:
 * @date: 2022/05/07 14:47
 * @author: beiji
 */
@RestController
@RequestMapping("wx/user")
public class WxUserController {

    @Autowired
    OrderService orderService;

    /**
     * 用户界面index功能
     * @author beiji
     * @since 2022/05/08 21:39
     */
    @GetMapping("index")
    public BaseRespVo WxUserIndex() {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        WxUserIndexVO wxUserIndexVO = orderService.selectOrderIndex(username);

        return BaseRespVo.ok(wxUserIndexVO, "成功", 0);
    }

}
