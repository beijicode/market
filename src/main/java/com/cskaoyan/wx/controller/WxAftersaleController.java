package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.service.OrderService;
import com.cskaoyan.wx.bean.bo.aftersalebo.AftersaleBO;
import com.cskaoyan.wx.bean.vo.aftersale.WxAfterSaleDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 创建日期: 2022/05/09 15:18
 * 申请售后
 * @author yangfan
 */
@RestController
@RequestMapping("wx/aftersale")
public class WxAftersaleController {

    @Autowired
    OrderService orderService;

    // 申请售后
    @RequestMapping("submit")
    public BaseRespVo sumbit(@RequestBody AftersaleBO aftersaleBO) {

        orderService.aftersaleSubmit(aftersaleBO);

        return BaseRespVo.ok(null,"成功",0);
    }

    // 售后详情页
    @RequestMapping("detail")
    public BaseRespVo detail(Integer orderId) {

        WxAfterSaleDetail data = orderService.aftersaleDetail(orderId);

        return BaseRespVo.ok(data,"成功",0);
    }
}
