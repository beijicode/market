package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.service.CategoryService;
import com.cskaoyan.wx.bean.vo.catalog.WxCatalogCurrentVO;
import com.cskaoyan.wx.bean.vo.catalog.WxCatalogIndexVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @since 2022/05/09 15:32
 * @author tangwei
 */
@RequestMapping("wx/catalog")
@RestController
public class WxCatalogController {
    @Autowired
    CategoryService categoryService;

    /**
     * 微信：总的分类页面
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/9 16:57
     */
    @RequestMapping("/index")
    public BaseRespVo catalogIndex() {
        WxCatalogIndexVO wxCatalogIndexVO = categoryService.queryAllCategorysAtWx();
        return BaseRespVo.ok(wxCatalogIndexVO, "成功", 0);
    }

    /**
     * 微信：当前的分类页面
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/9 16:57
     */
    @RequestMapping("/current")
    public BaseRespVo currentCatalog(Integer id) {
        WxCatalogCurrentVO wxCatalogCurrentVO = categoryService.queryCurrentCatalog(id);
        return BaseRespVo.ok(wxCatalogCurrentVO, "成功", 0);

    }
}