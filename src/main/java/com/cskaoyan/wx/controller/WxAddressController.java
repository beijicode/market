package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.exception.MyExcpetion;
import com.cskaoyan.wx.bean.bo.address.AddressSaveBO;
import com.cskaoyan.wx.bean.vo.address.AddressQueryDetailVO;
import com.cskaoyan.wx.bean.vo.address.AddressQueryVO;
import com.cskaoyan.wx.service.WxAddressService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author tangwei
 * @since 2022/05/08 14:55
 */
@RequestMapping("wx/address")
@RestController
public class WxAddressController {
    @Autowired
    WxAddressService wxAddressService;

    /**
     * 显示收货地址界面
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/8 16:26
     */
    @RequestMapping("/list")
    public BaseRespVo queryAddress() {
        // Integer userId = 1;

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();

        AddressQueryVO addressQueryVO = wxAddressService.queryAddress(username);
        return BaseRespVo.ok(addressQueryVO, "成功", 0);
    }

    /**
     * 显示一个收货地址详情
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/8 17:34
     */
    @RequestMapping("/detail")
    public BaseRespVo showAddressDetail(Integer id) {
        AddressQueryDetailVO addressQueryDetailVO = wxAddressService.showAddressDetail(id);
        return BaseRespVo.ok(addressQueryDetailVO, "成功", 0);
    }

    /**
     * 修改一个收货地址信息
     * 新建收货地址的信息也在这里
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/8 17:34
     */
    @RequestMapping("/save")
    public BaseRespVo saveAddress(@Validated @RequestBody AddressSaveBO addressSaveBO, BindingResult bindingResult) throws MyExcpetion {

        if (bindingResult.hasFieldErrors()) {
            throw new MyExcpetion("电话号码或者地址详情格式不对");
            // return BaseRespVo.ok(401, "电话号码或者地址详情格式不对");
        }

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        addressSaveBO.setUsername(username);

        wxAddressService.saveAddress(addressSaveBO);
        Integer data = addressSaveBO.getId();
        return BaseRespVo.ok(data, "成功", 0);
    }

    /**
     * 删除一个收货地址
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/8 21:45
     */
    @RequestMapping("/delete")
    public BaseRespVo deleteAddress(@RequestBody Map deleteAddress) {
        wxAddressService.deleteAddress(deleteAddress);
        return BaseRespVo.ok(0, "成功");
    }


}
