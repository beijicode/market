package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.wx.bean.bo.collectbo.CollectBO;
import com.cskaoyan.wx.bean.vo.collectvo.CollectListVO;
import com.cskaoyan.wx.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
@RestController
@RequestMapping("wx/collect")
public class WxCollectController {

    @Autowired
    CollectService collectService;

    /**
     * 方法说明 获得商品收藏列表
     * @author zhanfuqiang
     * @date 2022/5/8 23:01
     * @param type
     * @param page
     * @param limit
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("list")
    public BaseRespVo list(Byte type,Integer page,Integer limit) {
        CollectListVO collectListVO = collectService.list(type,page,limit);
        return BaseRespVo.ok(collectListVO, "成功", 0);
    }

    /**
     * 方法说明 收藏商品或者取消收藏
     * @author zhanfuqiang
     * @date 2022/5/9 9:13
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("addordelete")
    public BaseRespVo addordelete(@RequestBody CollectBO collectBO) {
        int i = collectService.addordelete(collectBO);
        if(i == 1){
            return BaseRespVo.ok(0,"成功");
        }
        return BaseRespVo.invalidData("修改收藏失败");
    }
}
