package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.service.OrderService;
import com.cskaoyan.wx.bean.bo.orderbo.WxOrderId;
import com.cskaoyan.wx.bean.bo.orderbo.WxOrderListBO;
import com.cskaoyan.wx.bean.bo.orderbo.WxOrderSubmitBO;
import com.cskaoyan.wx.bean.po.orderpo.WxComment;
import com.cskaoyan.wx.bean.po.orderpo.WxMarketOrder;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailOrderGoodsVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderDetailVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderSubmitVO;
import com.cskaoyan.wx.bean.vo.ordervo.WxOrderVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 创建日期: 2022/05/07 17:27
 *
 * @author yangfan
 */
@RestController
@RequestMapping("wx/order")
public class WxOrderController {

    @Autowired
    OrderService orderService;



    // 显示订单
    @RequestMapping("list")
    public BaseRespVo list(WxOrderListBO orderListBO) {

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();

        WxOrderVO wxOrderVO = orderService.list(orderListBO,username);

        return BaseRespVo.ok(wxOrderVO,"成功",0);
    }

    // 订单详情页
    @RequestMapping("detail")
    public BaseRespVo detail(Integer orderId) {

        WxOrderDetailVO wxOrderDetailVO = orderService.detail(orderId);

        return BaseRespVo.ok(wxOrderDetailVO,"成功",0);

    }

    // 用户取消订单
    @RequestMapping("cancel")
    public BaseRespVo cancel(@RequestBody WxOrderId orderId) {

        orderService.cancel(orderId.getOrderId());

        return BaseRespVo.ok(null,"成功",0);
    }

    // 订单逻辑删除
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody WxOrderId orderId) {

        orderService.delete(orderId.getOrderId());

        return BaseRespVo.ok(null,"成功",0);

    }

    // 申请退款
    @RequestMapping("refund")
    public BaseRespVo refund(@RequestBody WxOrderId orderId) {

        orderService.refund(orderId.getOrderId());

        return BaseRespVo.ok(null,"成功",0);

    }

    // 确认收货
    @RequestMapping("confirm")
    public BaseRespVo confirm(@RequestBody WxOrderId orderId) {

        orderService.confirm(orderId.getOrderId());

        return BaseRespVo.ok(null,"成功",0);

    }

    // 待评价商品的显示 goods
    // http://localhost:8083/wx/order/goods?orderId=1&goodsId=1
    @RequestMapping("goods")
    public BaseRespVo goods(Integer orderId, Integer goodsId) {

        WxOrderDetailOrderGoodsVO data = orderService.goods(orderId,goodsId);

        return BaseRespVo.ok(data,"成功",0);
    }

    // 发表评价 comment
    @RequestMapping("comment")
    public BaseRespVo comment(@RequestBody WxComment wxComment) {

        orderService.comment(wxComment);

        return BaseRespVo.ok(null,"成功",0);
    }


    // 支付 prepay
    @RequestMapping("prepay")
    public BaseRespVo prepay(@RequestBody WxOrderId orderId) {

        orderService.prepay(orderId.getOrderId());

        return BaseRespVo.ok(null,"成功",0);
    }


    // 订单提交 submit
    @RequestMapping("submit")
    public BaseRespVo submit(@RequestBody WxOrderSubmitBO wxOrderSubmitBO) {

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();

        WxOrderSubmitVO data = orderService.submit(wxOrderSubmitBO, username);

        return BaseRespVo.ok(data,"成功",0);

    }
}
