package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketFeedback;
import com.cskaoyan.wx.service.WxFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/07 16:29
 */
@RestController
@RequestMapping("wx/feedback")
public class WxFeedbackController {

    @Autowired
    WxFeedbackService wxFeedbackService;

    /**
     * 反馈
     * @param marketFeedback
     * @return
     */
    @RequestMapping("submit")
    public BaseRespVo submit(@RequestBody MarketFeedback marketFeedback){
        wxFeedbackService.submit(marketFeedback);
        marketFeedback.setAddTime(new Date());
        marketFeedback.setUpdateTime(new Date());
        return BaseRespVo.ok(null,"成功");
    }
}
