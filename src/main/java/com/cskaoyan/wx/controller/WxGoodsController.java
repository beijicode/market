package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.reverseengineering.MarketFootprint;
import com.cskaoyan.bean.vo.WxAboutVO;
import com.cskaoyan.service.FootPrintService;
import com.cskaoyan.service.GoodsService;
import com.cskaoyan.service.UserService;
import com.cskaoyan.wx.bean.WxGoodsListData;
import com.cskaoyan.wx.bean.vo.goods.WxCategoryVO;
import com.cskaoyan.wx.bean.vo.goods.WxGoodsDetailVO;
import com.cskaoyan.wx.bean.vo.goods.WxGoodsRelatedVO;
import com.cskaoyan.wx.bean.vo.goods.WxHomeIndexVO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @date: 2022/05/07 16:28
 * @author: beiji
 */
@RestController
@RequestMapping("wx")
public class WxGoodsController {
    @Autowired
    GoodsService goodsService;

    @Autowired
    UserService userService;

    @Autowired
    FootPrintService footPrintService;

    /**
     * 小程序首页index功能
     *
     * @author beiji
     * @since 2022/05/07 21:40
     */
    @RequestMapping("home/index")
    public BaseRespVo goodsIndex() {
        WxHomeIndexVO wxHomeIndexVO = goodsService.wxGoodsIndex();
        return BaseRespVo.ok(wxHomeIndexVO, "成功", 0);
    }

    @RequestMapping("home/about")
    public BaseRespVo about() {
        return BaseRespVo.ok(new WxAboutVO("武汉","31.201","121.52","beiji","021-1622-321","1221211"),"成功");
    }

    /**
     * 主页搜索栏内显示商品总数
     *
     * @author beiji
     * @since 2022/05/07 21:40
     */
    @RequestMapping("goods/count")
    public BaseRespVo goodsCount() {
        return BaseRespVo.ok(userService.dashboard().getGoodsTotal(), "成功");
    }

    /**
     * 点击首页商品父分类图标 显示父子分类
     *
     * @author beiji
     * @since 2022/05/09 19:52
     */
    @RequestMapping("goods/category")
    public BaseRespVo goodsCategory(Integer id) {
        WxCategoryVO wxCategoryVO = goodsService.queryWxGoodsCategory(id);
        return BaseRespVo.ok(wxCategoryVO, "成功", 0);
    }

    /**
     * 点击首页商品父分类图标 根据分类显示商品
     * categoryId = 0时为搜索框功能
     * categoryId ！= 0时为按照分类id显示商品功能
     *
     * @author beiji
     * @since 2022/05/09 19:52
     */
    @RequestMapping("goods/list")
    public BaseRespVo goodsList(Integer categoryId, BaseParam param, String keyword, Integer brandId) {

        Subject subject = SecurityUtils.getSubject();
        String username = null;
        if (subject.isAuthenticated()) {
            username = (String) subject.getPrincipals().getPrimaryPrincipal();
        }

        WxGoodsListData goodsListData = goodsService.queryWxGoodsList(categoryId, param, keyword, brandId,username);


        return BaseRespVo.ok(goodsListData, "成功");
    }

    /**
     * 查看商品详情
     *
     * @author beiji
     * @since 2022/05/09 19:53
     */
    @RequestMapping("goods/detail")
    public BaseRespVo goodsDetail(Integer id) {

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();


        footPrintService.insertFootPrint(id,username);

        WxGoodsDetailVO wxGoodsDetailVO = goodsService.queryWxGoodsDetail(id);
        return BaseRespVo.ok(wxGoodsDetailVO, "成功");
    }

    /**
     * 商品详情下显示关联商品
     *
     * @author beiji
     * @since 2022/05/09 19:53
     */
    @RequestMapping("goods/related")
    public BaseRespVo goodsRelated(Integer id) {
        WxGoodsRelatedVO wxGoodsRelatedVO = goodsService.queryWxGoodsRelated(id);
        return BaseRespVo.ok(wxGoodsRelatedVO, "成功", 0);
    }

}
