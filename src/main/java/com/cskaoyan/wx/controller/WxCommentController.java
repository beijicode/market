package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketComment;
import com.cskaoyan.service.CommentService;
import com.cskaoyan.wx.bean.bo.commentbo.WxCommentBO;
import com.cskaoyan.wx.bean.bo.commentbo.WxCommentPutBO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsCountVO;
import com.cskaoyan.wx.bean.vo.commentvo.WxCommentsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @description:
 * @author: lxc
 * @date: 2022/5/8 18:14
 */
@RequestMapping("wx/comment")
@RestController
public class WxCommentController {

    @Autowired
    CommentService commentService;

    /**
     * 小程序评论模块 显示评论列表
     * @param
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo commentList(WxCommentBO wxCommentBO){

        WxCommentsVO wxCommentsVO = commentService.commentList(wxCommentBO);

        return BaseRespVo.ok(wxCommentsVO,"成功",0);
    }

    /**
     * 小程序评论模块 显示评论数量
     * @param valueId
     * @param type
     * @return
     */
    @RequestMapping("count")
    public BaseRespVo commentCount(Integer valueId,Byte type){

        WxCommentsCountVO wxCommentsCountVO = commentService.commentCount(valueId,type);

        return BaseRespVo.ok(wxCommentsCountVO,"成功",0);
    }

    /**
     * 小程序评论模块 提交评论
     * @param wxCommentPutBO
     * @return
     */
    @RequestMapping("post")
    public BaseRespVo commentPut(@RequestBody WxCommentPutBO wxCommentPutBO){

        MarketComment marketComment = commentService.commentPut(wxCommentPutBO);

        if (marketComment == null){
            return BaseRespVo.ok(null,"评论提交错误",500);
        }

        return BaseRespVo.ok(marketComment,"成功",0);
    }
}
