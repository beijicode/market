package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketStorage;
import com.cskaoyan.service.StorageService;
import com.cskaoyan.wx.service.WxStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Charadic
 * @since 2022/05/07 15:41
 */
@RestController
@RequestMapping("wx/storage")
public class WxStorageController {

    @Autowired
    WxStorageService WxstorageService;

    @RequestMapping("upload")
    public BaseRespVo upload(MultipartFile file, HttpServletRequest request){
        MarketStorage marketStorage = WxstorageService.upload(file,request);
        return BaseRespVo.ok(marketStorage,"成功",0);
    }
}
