package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.service.IssueService;
import com.cskaoyan.wx.bean.vo.issue.IssuesInWxQueryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @since 2022/05/10 22:44
 * @author tangwei
 */
@RequestMapping("wx/issue")
@RestController
public class WxIssueController {

    @Autowired
    IssueService issueService;

    @RequestMapping("/list")
    public BaseRespVo queryAllIssuesInWx(Integer page, Integer limit) {
        IssuesInWxQueryVO issuesInWxQueryVO = issueService.queryAllIssuesInWx(page, limit);
        return BaseRespVo.ok(issuesInWxQueryVO, "成功", 0);
    }
}