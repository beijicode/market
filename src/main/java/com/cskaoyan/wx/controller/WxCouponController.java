package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.wx.bean.vo.WxCouponListVO;
import com.cskaoyan.wx.bean.vo.WxCouponUserListVO;
import com.cskaoyan.wx.service.WxCouponService;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Charadic
 * @since 2022/05/07 22:27
 */
@RestController
@RequestMapping("wx/coupon")
public class WxCouponController {

    @Autowired
    WxCouponService wxCouponService;

    /**
     * 点进去全部优惠券展示
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo<WxCouponListVO> list(Integer page, Integer limit) {
        WxCouponListVO wxCouponListVO = wxCouponService.list(page, limit);
        return BaseRespVo.ok(wxCouponListVO, "成功");
    }

    /**
     * 展示个人所拥有的优惠券
     * @param status
     * @param page
     * @param limit
     * @return
     */
    @RequestMapping("mylist")
    public BaseRespVo<WxCouponUserListVO> myList(Short status, Integer page, Integer limit) {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        WxCouponUserListVO wxCouponUserListVO = wxCouponService.myList(status, page, limit, username);
        return BaseRespVo.ok(wxCouponUserListVO, "成功");
    }

    /**
     * 在下单时选择优惠券
     * @param cartId
     * @param grouponRulesId
     * @return
     */
    @RequestMapping("selectlist")
    public BaseRespVo<WxCouponUserListVO> selectList(Integer cartId, Integer grouponRulesId) {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        WxCouponUserListVO wxCouponUserListVO = wxCouponService.selectList(cartId, grouponRulesId, username);
        return BaseRespVo.ok(wxCouponUserListVO, "成功");
    }

    /**
     * 点击优惠券的方式获得优惠券
     * @param coupon
     * @return
     */
    @RequestMapping("receive")
    public BaseRespVo receive(@RequestBody Map coupon) {
        System.out.println(1);
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        Integer code = wxCouponService.receive(coupon, username);
        if (code == 740) {
            return BaseRespVo.ok(740, "优惠券已经领取过");
        } else if (code == 741) {
            return BaseRespVo.ok(741, "优惠券不可用");
        }
        return BaseRespVo.ok(null, "成功");
    }

    /**
     * 兑换码兑换优惠券
     * @param coupon
     * @return
     */
    @RequestMapping("exchange")
    public BaseRespVo exchange(@RequestBody Map coupon) {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        Integer code = wxCouponService.exchange(coupon, username);
        if (code == 740) {
            return BaseRespVo.ok(740, "优惠券已经领取过");
        } else if (code == 741) {
            return BaseRespVo.ok(741, "优惠券不可用");
        } else if (code == 742) {
            return BaseRespVo.ok(742, "优惠券不正确");
        }
        return BaseRespVo.ok(null, "成功");
    }

}
