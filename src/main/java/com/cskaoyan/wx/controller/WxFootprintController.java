package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.wx.bean.vo.WxFootprintListVO;
import com.cskaoyan.wx.service.WxFootPrintService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Charadic
 * @since 2022/05/07 16:44
 */
@RestController
@RequestMapping("wx/footprint")
public class WxFootprintController {

    @Autowired
    WxFootPrintService wxFootPrintService;

    /**
     * 历史足迹展示
     * @param limit
     * @param page
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo<WxFootprintListVO> list(Integer limit, Integer page) {
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated()){
            return BaseRespVo.ok(null,"未登录",700);
        }
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();

        WxFootprintListVO wxFootprintListVO = wxFootPrintService.list(limit, page,username);
        return BaseRespVo.ok(wxFootprintListVO, "成功");
    }

    /**
     * 历史足迹删除
     * @param footprint
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo<WxFootprintListVO> delete(@RequestBody Map footprint) {
        wxFootPrintService.delete(footprint);
        return BaseRespVo.ok(null, "成功");
    }
}

