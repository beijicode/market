package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.bo.topicbo.TopicBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.promote.topicVO.TopicData;
import com.cskaoyan.bean.vo.promote.topicVO.TopicReadVO;
import com.cskaoyan.service.TopicService;
import com.cskaoyan.wx.bean.vo.topicvo.WxTopicDetailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/7 16:12
 */
@RequestMapping("wx/topic")
@RestController
public class WxTopicController {

    @Autowired
    TopicService topicService;

    /**
     * 专题精选模块 列表显示
     * @param topicBO
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo topicList(TopicBO topicBO){

        TopicData topicData = topicService.list(topicBO);
        return BaseRespVo.ok(topicData,"成功",0);
    }

    /**
     * 专题精选模块 细节显示
     * @param id
     * @return
     */
    @RequestMapping("detail")
    public BaseRespVo topicDetail(Integer id){

        TopicReadVO topicReadVO = topicService.selectByTopicId(id);
        WxTopicDetailVO wxTopicDetailVo = new WxTopicDetailVO(topicReadVO.getGoodsList(), topicReadVO.getTopic());
        return BaseRespVo.ok(wxTopicDetailVo,"成功",0);

    }

    /**
     * 专题精选模块 类似商品
     * @param id
     * @return
     */
    @RequestMapping("related")
    public BaseRespVo related(Integer id){

        TopicBO topicBO = new TopicBO();
        topicBO.setLimit(4);
        topicBO.setPage(1);
        TopicData topicData = topicService.list(topicBO);
        return BaseRespVo.ok(topicData,"成功",0);
    }
}
