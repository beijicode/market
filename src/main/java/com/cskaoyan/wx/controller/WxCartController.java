package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.wx.bean.bo.cartbo.CartAddBO;
import com.cskaoyan.wx.bean.bo.cartbo.CartCheckedBO;
import com.cskaoyan.wx.bean.bo.cartbo.CartDeleteBO;
import com.cskaoyan.wx.bean.bo.cartbo.CartUpdateBO;
import com.cskaoyan.wx.bean.vo.cartvo.CartCheckOutVO;
import com.cskaoyan.wx.bean.vo.cartvo.CartIndexVO;
import com.cskaoyan.wx.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
@RestController
@RequestMapping("wx/cart")
public class WxCartController {
    @Autowired
    CartService cartService;

    /**
     * 方法说明 购物车展示界面
     *
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/8 15:19
     */
    @RequestMapping("index")
    public BaseRespVo index() {
        CartIndexVO cartIndexVO = cartService.index();
        return BaseRespVo.ok(cartIndexVO, "成功", 0);
    }

    /**
     * 方法说明 更改选中状态
     *
     * @param cartCheckedBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/8 15:51
     */
    @PostMapping("checked")
    public BaseRespVo checked(@RequestBody CartCheckedBO cartCheckedBO) {
        CartIndexVO cartIndexVO = cartService.checked(cartCheckedBO);
        return BaseRespVo.ok(cartIndexVO, "成功", 0);
    }


    /**
     * 方法说明 更新订单商品数量
     *
     * @param cartUpdateBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/8 16:24
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody CartUpdateBO cartUpdateBO) {
        Boolean isupdate = cartService.update(cartUpdateBO);
        if (isupdate) {
            return BaseRespVo.ok(0, "成功");
        }
        return BaseRespVo.invalidData("更新失败");
    }


    /**
     * 方法说明 删除订单
     * @author zhanfuqiang
     * @date 2022/5/8 16:36
     * @param cartDeleteBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody CartDeleteBO cartDeleteBO) {
        CartIndexVO cartIndexVO = cartService.delete(cartDeleteBO);
        return BaseRespVo.ok(cartIndexVO,"成功",0);
    }

    /**
     * 方法说明 获得购物车商品数量
     * @author zhanfuqiang
     * @date 2022/5/9 12:03
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("goodscount")
    public BaseRespVo goodscount(){
        int count = cartService.goodscount();
        return BaseRespVo.ok(count,"成功",0);
    }


    /**
     * 方法说明 向购物车中添加商品
     * @author zhanfuqiang
     * @date 2022/5/9 12:04
     * @param cartAddBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("add")
    public BaseRespVo add(@RequestBody CartAddBO cartAddBO){
        int count = cartService.add(cartAddBO);
        return BaseRespVo.ok(count,"成功",0);
    }

    /**
     * 方法说明 直接下单
     * @author zhanfuqiang
     * @date 2022/5/9 15:39
     * @param cartAddBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("fastadd")
    public BaseRespVo fastadd(@RequestBody CartAddBO cartAddBO){
        int id = cartService.fastadd(cartAddBO);
        return BaseRespVo.ok(id,"成功",0);
    }

    /**
     * 方法说明 结算订单
     * @author zhanfuqiang
     * @date 2022/5/10 11:15
     * @param cartId
     * @param addressId
     * @param couponId
     * @param userCouponId
     * @param grouponRulesId
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("checkout")
    public BaseRespVo checkout(Integer cartId,Integer addressId,Integer couponId,Integer userCouponId,Integer grouponRulesId){
        CartCheckOutVO cartCheckOutVO = cartService.checkout(cartId,addressId,couponId,userCouponId,grouponRulesId);
        return BaseRespVo.ok(cartCheckOutVO,"成功",0);
    }


}
