package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.wx.bean.bo.regbo.RegCaptchaBO;
import com.cskaoyan.wx.bean.bo.regbo.RegisterBO;
import com.cskaoyan.wx.service.RegService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 9:42
 */
@RestController
@RequestMapping("wx/auth")
public class WxRegCaptchaController {

    String code = null;
    @Autowired
    RegService regService;

    /**
     * 发送验证码
     * @param regCaptchaBO
     * @return
     */
    @RequestMapping("regCaptcha")
    public BaseRespVo authRegCaptcha(@RequestBody RegCaptchaBO regCaptchaBO){

        code = regService.getRegCaptcha(regCaptchaBO);

        return BaseRespVo.ok(null,"成功",0);
    }

    /**
     * 微信用户注册
     * @param registerBO
     * @return
     */
    @RequestMapping("register")
    public BaseRespVo authRegister(@RequestBody RegisterBO registerBO){

        System.out.println(code);
        int num = regService.register(registerBO,code);
        if (num == 703){
            return BaseRespVo.ok(null,"验证码错误",703);
        }

        if (num == 704){
            return BaseRespVo.ok(null,"用户名已注册",704);
        }
        if (num == 200){
            return BaseRespVo.ok(null,"成功",0);
        }
        return null;
    }
}
