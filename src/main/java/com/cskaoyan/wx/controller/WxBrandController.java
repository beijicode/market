package com.cskaoyan.wx.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketBrand;
import com.cskaoyan.service.BrandService;
import com.cskaoyan.wx.bean.vo.brand.WxBrandListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @since 2022/05/08 23:15
 * @author tangwei
 */
@RequestMapping("wx/brand")
@RestController
public class WxBrandController {
    @Autowired
    BrandService brandService;

    /**
     * 首页品牌商直供商页面显示
     * @param 
     * @return 
     * @author tangwei
     * @since 2022/5/9 15:11 
     */
    @RequestMapping("/list")
    public BaseRespVo queryAllBrands(Integer page, Integer limit) {
        WxBrandListVO wxBrandListVO = brandService.queryAllBrands(page, limit);
        return BaseRespVo.ok(wxBrandListVO, "成功", 0);
    }
    
    /**
     *  品牌商直供商详情
     * @param
     * @return 
     * @author tangwei
     * @since 2022/5/9 15:11 
     */
    @RequestMapping("/detail")
    public BaseRespVo BrandDetail(Integer id) {
        MarketBrand marketBrand = brandService.queryBrandDetail(id);
        return BaseRespVo.ok(marketBrand, "成功", 0);
    }
}