package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketFeedback;

public interface WxFeedbackService {
    void submit(MarketFeedback marketFeedback);

}
