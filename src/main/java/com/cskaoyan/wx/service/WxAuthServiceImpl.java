package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.mapper.MarketUserMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/09 22:55
 */
@Service
public class WxAuthServiceImpl implements WxAuthService {

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public MarketUser login(String username) {
        MarketUser marketUser = marketUserMapper.queryByUsername(username);
        return marketUser;
    }

    @Override
    public void updateLastLogin(String remoteAddr, Date date, String username) {
        MarketUser marketUser = marketUserMapper.queryByUsername(username);
        marketUser.setLastLoginTime(date);
        marketUser.setLastLoginIp(remoteAddr);
        marketUserMapper.updateByPrimaryKeySelective(marketUser);
    }

    @Override
    public String lastLoginIp(String username) {
        MarketUser user = marketUserMapper.queryByUsername(username);
        String lastLoginIp = user.getLastLoginIp();
        return lastLoginIp;
    }

}
