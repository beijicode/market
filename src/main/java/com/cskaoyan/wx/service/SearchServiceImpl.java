package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.*;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketKeywordMapper;
import com.cskaoyan.mapper.MarketSearchHistoryMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/7 17:23
 */
@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    MarketKeywordMapper marketKeywordMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    /**
     * 搜索 显示默认搜索
     * @return
     */
    @Override
    public MarketKeyword selectDefaultKeyWord() {

        List<MarketKeyword> marketKeywords = marketKeywordMapper.selectDefaultKeyWord();
        int size = marketKeywords.size();
        // 根据list大小内的随机数
        Random random = new Random();
        int randomNum = random.nextInt(size) + 1;
        // 根据随机数从list中返回一个marketKeywrods
        MarketKeyword marketKeyword = marketKeywords.get(randomNum - 1);
        return marketKeyword;

    }

    /**
     * 搜索 搜索历史
     * @return
     */
    @Override
    public List<MarketSearchHistory> selectMarketSearchHistory(String username) {

        MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
        MarketSearchHistoryExample.Criteria criteria = marketSearchHistoryExample.createCriteria();

        if (username != null){
            // 通过username找出userid
            MarketUser marketUser = marketUserMapper.queryByUsername(username);
            criteria.andUserIdEqualTo(marketUser.getId());
        } else {
            criteria.andUserIdEqualTo(-1);
        }


        List<MarketSearchHistory> marketSearchHistoryList = marketSearchHistoryMapper.selectByExample(marketSearchHistoryExample);
        return marketSearchHistoryList;
    }

    /**
     * 搜索 热门搜索
     * @return
     */
    @Override
    public List<MarketKeyword> selectHotKeyWord() {

        List<MarketKeyword> marketKeywords = marketKeywordMapper.selectHotKeyWord();
        return marketKeywords;
    }

    @Override
    public String[] selectGoodByGoods(String keyword) {

        String name = "%" + keyword + "%";
        String[] names = marketGoodsMapper.selectGoodsNameByKeyword(name);

        return names;
    }

    @Override
    public int updateSearchHistory() {

        MarketSearchHistory marketSearchHistory = new MarketSearchHistory();
        marketSearchHistory.setUpdateTime(new Date());
        marketSearchHistory.setDeleted(true);

        int affectedRows = marketSearchHistoryMapper.updateByExampleSelective(marketSearchHistory,new MarketSearchHistoryExample());

        return affectedRows;
    }
}
