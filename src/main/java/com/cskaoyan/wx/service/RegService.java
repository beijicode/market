package com.cskaoyan.wx.service;

import com.cskaoyan.wx.bean.bo.regbo.RegCaptchaBO;
import com.cskaoyan.wx.bean.bo.regbo.RegisterBO;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 9:50
 */
public interface RegService {

    String getRegCaptcha(RegCaptchaBO regCaptchaBO);

    int register(RegisterBO registerBO, String code);
}
