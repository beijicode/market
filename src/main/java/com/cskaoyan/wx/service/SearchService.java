package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketKeyword;
import com.cskaoyan.bean.reverseengineering.MarketSearchHistory;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/7 17:22
 */
public interface SearchService {

    MarketKeyword selectDefaultKeyWord();

    List<MarketSearchHistory> selectMarketSearchHistory(String username);

    List<MarketKeyword> selectHotKeyWord();

    String[] selectGoodByGoods(String keyword);

    int updateSearchHistory();

}
