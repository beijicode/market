package com.cskaoyan.wx.service;

import com.cskaoyan.wx.bean.vo.WxFootprintListVO;

import java.util.Map;

public interface WxFootPrintService {
    WxFootprintListVO list(Integer limit, Integer page,String username);

    void delete(Map footprint);
}
