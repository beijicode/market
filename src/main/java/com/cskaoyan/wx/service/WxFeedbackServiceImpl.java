package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketFeedback;
import com.cskaoyan.mapper.MarketFeedbackMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Charadic
 * @since 2022/05/07 16:35
 */
@Service
public class WxFeedbackServiceImpl implements WxFeedbackService {

    @Autowired
    MarketFeedbackMapper marketFeedbackMapper;

    @Override
    public void submit(MarketFeedback marketFeedback) {
        marketFeedbackMapper.insertSelective(marketFeedback);
    }
}
