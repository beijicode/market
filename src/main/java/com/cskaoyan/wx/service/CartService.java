package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketCart;
import com.cskaoyan.wx.bean.bo.cartbo.CartAddBO;
import com.cskaoyan.wx.bean.bo.cartbo.CartCheckedBO;
import com.cskaoyan.wx.bean.bo.cartbo.CartDeleteBO;
import com.cskaoyan.wx.bean.bo.cartbo.CartUpdateBO;
import com.cskaoyan.wx.bean.vo.cartvo.CartCheckOutVO;
import com.cskaoyan.wx.bean.vo.cartvo.CartIndexVO;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public interface CartService {
    CartIndexVO index();

    CartIndexVO checked(CartCheckedBO cartCheckedBO);

    Boolean update(CartUpdateBO cartUpdateBO);

    CartIndexVO delete(CartDeleteBO cartDeleteBO);

    int add(CartAddBO cartAddBO);

    int goodscount();

    int fastadd(CartAddBO cartAddBO);

    CartCheckOutVO checkout(Integer cartId, Integer addressId, Integer couponId, Integer userCouponId, Integer grouponRulesId);

    Integer getIdByUsername();

}
