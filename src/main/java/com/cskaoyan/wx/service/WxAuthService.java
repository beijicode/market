package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketUser;

import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/09 22:54
 */
public interface WxAuthService {
    MarketUser login(String username);

    void updateLastLogin(String remoteAddr, Date date, String username);

    String lastLoginIp(String username);
}
