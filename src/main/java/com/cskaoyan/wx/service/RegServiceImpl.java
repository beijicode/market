package com.cskaoyan.wx.service;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.config.AliyunComponent;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.util.Md5Utils;
import com.cskaoyan.wx.bean.bo.regbo.RegCaptchaBO;
import com.cskaoyan.wx.bean.bo.regbo.RegisterBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/11 9:50
 */
@Service
public class RegServiceImpl implements RegService {

    @Autowired
    AliyunComponent aliyunComponent;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public String getRegCaptcha(RegCaptchaBO regCaptchaBO) {

        // 随机生成六位数
        int code = (int) ((Math.random() * 9 + 1) * 100000);
        aliyunComponent.sendMsg(regCaptchaBO.getMobile(),Integer.toString(code));

        return Integer.toString(code);
    }

    @Override
    public int register(RegisterBO registerBO, String code) {

        // 如果输入的验证码和随机生成的不一致，则返回703
        if (!registerBO.getCode().equals(code)){
            return 703;
        }

        // 查询用户名是否已经注册过
        MarketUser marketUser1 = marketUserMapper.queryByUsername(registerBO.getUsername());
        if (marketUser1 != null){
            return 704;
        }

        MarketUser marketUser = new MarketUser();
        marketUser.setAddTime(new Date());
        marketUser.setDeleted(false);
        marketUser.setMobile(registerBO.getMobile());
        marketUser.setPassword(Md5Utils.getMd5(registerBO.getPassword()));
        marketUser.setUsername(registerBO.getUsername());
        marketUser.setNickname(registerBO.getUsername());
        marketUser.setWeixinOpenid(registerBO.getWxCode());
        marketUserMapper.insertSelective(marketUser);
        return 200;
    }
}
