package com.cskaoyan.wx.service;

import com.cskaoyan.wx.bean.bo.address.AddressSaveBO;
import com.cskaoyan.wx.bean.vo.address.AddressQueryDetailVO;
import com.cskaoyan.wx.bean.vo.address.AddressQueryVO;

import java.util.Map;

/**
 *
 * @since 2022/05/08 14:59
 * @author tangwei
 */
public interface WxAddressService {

    AddressQueryVO queryAddress(String username);

    AddressQueryDetailVO showAddressDetail(Integer id);

    void saveAddress(AddressSaveBO addressSaveBO);

    void deleteAddress(Map deleteAddress);

}