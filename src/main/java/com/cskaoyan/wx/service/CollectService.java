package com.cskaoyan.wx.service;

import com.cskaoyan.wx.bean.bo.collectbo.CollectBO;
import com.cskaoyan.wx.bean.vo.collectvo.CollectListVO;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
public interface CollectService {

    CollectListVO list(Byte type, Integer page, Integer limit);

    int addordelete(CollectBO collectBO);

    Integer getIdByUsername();
}
