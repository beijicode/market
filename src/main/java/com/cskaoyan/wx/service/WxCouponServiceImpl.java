package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.*;
import com.cskaoyan.mapper.MarketCartMapper;
import com.cskaoyan.mapper.MarketCouponMapper;
import com.cskaoyan.mapper.MarketCouponUserMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.wx.bean.vo.MarketCouponUserVO;
import com.cskaoyan.wx.bean.vo.MarketCouponVO;
import com.cskaoyan.wx.bean.vo.WxCouponListVO;
import com.cskaoyan.wx.bean.vo.WxCouponUserListVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Charadic
 * @since 2022/05/07 22:28
 */
@Service
public class WxCouponServiceImpl implements WxCouponService {

    @Autowired
    MarketCouponMapper marketCouponMapper;

    @Autowired
    MarketCouponUserMapper marketCouponUserMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Autowired
    MarketCartMapper marketCartMapper;

    @Override
    public WxCouponListVO list(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);

        List<MarketCouponVO> list = marketCouponMapper.queryAllCoupons();

        PageInfo<MarketCouponVO> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        WxCouponListVO wxCouponListVO = new WxCouponListVO(limit, page, list, pages, (int) total);
        return wxCouponListVO;
    }

    @Override
    public WxCouponUserListVO myList(Short status, Integer page, Integer limit, String username) {

        MarketUser user = marketUserMapper.queryByUsername(username);

        PageHelper.startPage(page, limit);

        List<MarketCouponUserVO> list = marketCouponUserMapper.queryMyList(user.getId(), status);


        PageInfo<MarketCouponUserVO> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        WxCouponUserListVO wxCouponUserListVO = new WxCouponUserListVO(limit, page, list, pages, (int) total);
        return wxCouponUserListVO;
    }

    @Override
    public WxCouponUserListVO selectList(Integer cartId, Integer grouponRulesId, String username) {

        MarketUser user = marketUserMapper.queryByUsername(username);

        List<MarketCouponUserVO> list = marketCouponUserMapper.queryMyList(user.getId(), (short) 0);

        // 如果没有优惠券，返回
        if (list.size() == 0) {
            WxCouponUserListVO wxCouponUserListVO = new WxCouponUserListVO(0, 1, list, 1, 0);
            return wxCouponUserListVO;
        }

        // 直接下单的情况，cartId!=0,只有一个
        if (cartId != 0) {
            MarketCart marketCart = marketCartMapper.selectByPrimaryKey(cartId);
            BigDecimal number = new BigDecimal(marketCart.getNumber());
            for (MarketCouponUserVO marketCouponUserVO : list) {
                if (marketCart.getPrice().multiply(number).compareTo(marketCouponUserVO.getMin()) != -1) {
                    marketCouponUserVO.setAvailable(true);
                }
            }
            WxCouponUserListVO wxCouponUserListVO = new WxCouponUserListVO(list.size(), 1, list, 1, list.size());
            return wxCouponUserListVO;
        }

        // 购物车下单的情况，cartId==0,多个check,根据userId
        MarketCartExample marketCartExample = new MarketCartExample();
        MarketCartExample.Criteria criteria = marketCartExample.createCriteria();
        criteria.andCheckedEqualTo(true);
        criteria.andUserIdEqualTo(user.getId());
        List<MarketCart> marketCarts = marketCartMapper.selectByExample(marketCartExample);
        BigDecimal price = new BigDecimal(0);
        for (MarketCart cart : marketCarts) {
            BigDecimal number = new BigDecimal(cart.getNumber());
            price = price.add(cart.getPrice().multiply(number));
        }
        for (MarketCouponUserVO marketCouponUserVO : list) {
            if (price.compareTo(marketCouponUserVO.getMin()) != -1) {
                marketCouponUserVO.setAvailable(true);
            }
        }
        WxCouponUserListVO wxCouponUserListVO = new WxCouponUserListVO(list.size(), 1, list, 1, list.size());
        return wxCouponUserListVO;
    }

    @Override
    public Integer receive(Map coupon, String username) {
        MarketUser user = marketUserMapper.queryByUsername(username);

        Integer couponId = (Integer) coupon.get("couponId");
        MarketCoupon marketCoupon = marketCouponMapper.selectByPrimaryKey(couponId);

        MarketCouponUserExample marketCouponUserExample = new MarketCouponUserExample();
        MarketCouponUserExample.Criteria criteria = marketCouponUserExample.createCriteria();
        criteria.andUserIdEqualTo(user.getId());
        criteria.andCouponIdEqualTo(couponId);
        List<MarketCouponUser> list = marketCouponUserMapper.selectByExample(marketCouponUserExample);
        if (list.size() >= marketCoupon.getLimit()) {
            return 740;
        }
        if (marketCoupon.getType() != 0 || marketCoupon.getDeleted()) {
            return 741;
        }
        Short timeType = marketCoupon.getTimeType();
        Short days = marketCoupon.getDays();
        Date startTime = marketCoupon.getStartTime();
        Date endTime = marketCoupon.getEndTime();
        MarketCouponUser marketCouponUser = new MarketCouponUser();
        marketCouponUser.setCouponId(couponId);
        marketCouponUser.setUserId(user.getId());
        if (timeType == 0) {
            Date oldDate = new Date();
            long time = oldDate.getTime() + days * 86400000;
            Date newDate = new Date();
            newDate.setTime(time);
            marketCouponUser.setStartTime(oldDate);
            marketCouponUser.setEndTime(newDate);
            marketCouponUser.setAddTime(oldDate);
            marketCouponUser.setUpdateTime(oldDate);
            marketCouponUserMapper.insertSelective(marketCouponUser);
            return 200;
        }
        marketCouponUser.setStartTime(startTime);
        marketCouponUser.setEndTime(endTime);
        marketCouponUser.setAddTime(new Date());
        marketCouponUser.setUpdateTime(new Date());
        return 200;

    }

    @Override
    public Integer exchange(Map coupon, String username) {
        MarketUser user = marketUserMapper.queryByUsername(username);

        String code = (String) coupon.get("code");

        MarketCoupon marketCoupon = marketCouponMapper.selectByCode(code);

        if (marketCoupon == null) {
            return 742;
        }

        MarketCouponUserExample marketCouponUserExample = new MarketCouponUserExample();
        MarketCouponUserExample.Criteria criteria = marketCouponUserExample.createCriteria();
        criteria.andUserIdEqualTo(user.getId());
        criteria.andCouponIdEqualTo(marketCoupon.getId());
        List<MarketCouponUser> list = marketCouponUserMapper.selectByExample(marketCouponUserExample);
        if (list.size() >= marketCoupon.getLimit()) {
            return 740;
        }
        if (marketCoupon.getType() != 2 || marketCoupon.getDeleted() || marketCoupon.getStatus() != 0) {
            return 741;
        }

        Short timeType = marketCoupon.getTimeType();
        Short days = marketCoupon.getDays();
        Date startTime = marketCoupon.getStartTime();
        Date endTime = marketCoupon.getEndTime();
        MarketCouponUser marketCouponUser = new MarketCouponUser();
        marketCouponUser.setCouponId(marketCoupon.getId());
        marketCouponUser.setUserId(user.getId());
        if (timeType == 0) {
            Date oldDate = new Date();
            long time = oldDate.getTime() + days * 86400000;
            Date newDate = new Date();
            newDate.setTime(time);
            marketCouponUser.setStartTime(oldDate);
            marketCouponUser.setEndTime(newDate);
            marketCouponUser.setAddTime(oldDate);
            marketCouponUser.setUpdateTime(oldDate);
            marketCouponUserMapper.insertSelective(marketCouponUser);
            return 200;
        }
        marketCouponUser.setStartTime(startTime);
        marketCouponUser.setEndTime(endTime);
        marketCouponUser.setAddTime(new Date());
        marketCouponUser.setUpdateTime(new Date());
        return 200;
    }
}
