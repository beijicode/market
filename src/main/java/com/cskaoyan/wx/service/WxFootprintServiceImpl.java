package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketFootprint;
import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.bean.vo.goodsvo.GoodsListVO;
import com.cskaoyan.mapper.MarketFootprintMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.wx.bean.vo.WxFootprintAndGoodsVO;
import com.cskaoyan.wx.bean.vo.WxFootprintListVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author Charadic
 * @since 2022/05/07 16:49
 */
@Service
public class WxFootprintServiceImpl implements WxFootPrintService {

    @Autowired
    MarketFootprintMapper marketFootprintMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public WxFootprintListVO list(Integer limit, Integer page,String username) {
        MarketUser user = marketUserMapper.queryByUsername(username);

        PageHelper.startPage(page, limit);

        List<WxFootprintAndGoodsVO> list = marketFootprintMapper.list(user.getId());

        PageInfo<WxFootprintAndGoodsVO> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        WxFootprintListVO wxFootprintListVO = new WxFootprintListVO(limit, list, page, pages, (int) total);
        return wxFootprintListVO;
    }

    @Override
    public void delete(Map footprint) {

        Integer id = (Integer) footprint.get("id");
        MarketFootprint marketFootprint = new MarketFootprint();
        marketFootprint.setId(id);
        marketFootprint.setDeleted(true);
        marketFootprintMapper.updateByPrimaryKeySelective(marketFootprint);
    }
}
