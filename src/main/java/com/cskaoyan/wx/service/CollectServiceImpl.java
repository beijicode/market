package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketCollect;
import com.cskaoyan.bean.reverseengineering.MarketCollectExample;
import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.mapper.MarketCollectMapper;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.wx.bean.bo.collectbo.CollectBO;
import com.cskaoyan.wx.bean.vo.collectvo.CollectListVO;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/8
 */
@Service
public class CollectServiceImpl implements CollectService {
    @Autowired
    CollectService collectService;

    @Autowired
    MarketCollectMapper marketCollectMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    //获得用户id
    @Override
    public Integer getIdByUsername(){
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        MarketUser marketUser = marketUserMapper.queryByUsername(username);
        Integer id = marketUser.getId();
        return id;
    }

    @Override
    public CollectListVO list(Byte type, Integer page, Integer limit) {

        CollectListVO collectListVO = new CollectListVO();
        List<CollectListVO.ListEntity> list = collectListVO.getList();

        //获得用户id
        Integer idByUsername = collectService.getIdByUsername();

        MarketCollectExample marketCollectExample = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = marketCollectExample.createCriteria();
        criteria.andTypeEqualTo(type);
        criteria.andUserIdEqualTo(idByUsername);
        List<MarketCollect> marketCollects = marketCollectMapper.selectByExample(marketCollectExample);

        //倒序输出
        LinkedList<MarketCollect> temp = new LinkedList<>();
        for (int i = 0; i < marketCollects.size(); i++) {
            temp.addFirst(marketCollects.get(i));
        }
        marketCollects = temp;

        //若删除状态为true则从链表中去除
        Iterator<MarketCollect> iterator = marketCollects.iterator();
        while (iterator.hasNext()){
            if(iterator.next().getDeleted()){
                iterator.remove();
            }
        }

        for (int i = 0; i < marketCollects.size(); i++) {
            MarketCollect marketCollect = marketCollects.get(i);
            collectListVO.addList();
            CollectListVO.ListEntity listEntity = list.get(i);

            Integer valueId = marketCollect.getValueId();
            listEntity.setValueId(valueId);
            listEntity.setType(marketCollect.getType());
            listEntity.setId(marketCollect.getId());

            //根据valued查询商品信息
            MarketGoods marketGood = marketGoodsMapper.selectByPrimaryKey(valueId);
            listEntity.setBrief(marketGood.getBrief());
            listEntity.setName(marketGood.getName());
            listEntity.setPicUrl(marketGood.getPicUrl());
            listEntity.setRetailPrice(marketGood.getRetailPrice().doubleValue());
        }

        //分页参数赋值
        PageInfo<CollectListVO.ListEntity> pageInfo = new PageInfo<>(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        collectListVO.setLimit(limit);
        collectListVO.setPage(page);
        collectListVO.setPages(pages);
        collectListVO.setTotal(total);

        return collectListVO;

    }

    @Override
    public int addordelete(CollectBO collectBO) {

        Byte type = collectBO.getType();
        Integer valueId = collectBO.getValueId();
        Integer idByUsername = collectService.getIdByUsername();
        //根据valueId和type查询收藏数据，如果查询为空则添加；如果查询到就拿到deleted的值，取反后再赋值回去
        MarketCollectExample marketCollectExample = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = marketCollectExample.createCriteria();
        criteria.andTypeEqualTo(type);
        criteria.andValueIdEqualTo(valueId);
        criteria.andUserIdEqualTo(idByUsername);
        List<MarketCollect> marketCollects = marketCollectMapper.selectByExample(marketCollectExample);

        if(marketCollects.size() == 0){
            //没有查询到，添加收藏
            MarketCollect marketCollect = new MarketCollect();
            marketCollect.setType(type);
            marketCollect.setValueId(valueId);
            Date date = new Date();
            marketCollect.setAddTime(date);
            marketCollect.setDeleted(false);
            marketCollect.setUpdateTime(date);
            //从session中拿用户信息
            marketCollect.setUserId(idByUsername);

            int insert = marketCollectMapper.insertSelective(marketCollect);
            return insert;
        }
        //查询到，对deleted取反再更新
        MarketCollect marketCollect = marketCollects.get(0);
        Boolean deleted = marketCollect.getDeleted();
        marketCollect.setDeleted(!deleted);
        marketCollect.setUpdateTime(new Date());

        int i = marketCollectMapper.updateByPrimaryKeySelective(marketCollect);
        return i;
    }


}
