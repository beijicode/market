package com.cskaoyan.wx.service;

import com.cskaoyan.wx.bean.vo.WxCouponListVO;
import com.cskaoyan.wx.bean.vo.WxCouponUserListVO;

import java.util.Map;

public interface WxCouponService {
    WxCouponListVO list(Integer page, Integer limit);

    WxCouponUserListVO myList(Short status, Integer page, Integer limit, String username);

    WxCouponUserListVO selectList(Integer cartId, Integer grouponRulesId, String username);

    Integer receive(Map coupon, String username);

    Integer exchange(Map coupon, String username);
}
