package com.cskaoyan.wx.service;

import com.cskaoyan.bean.reverseengineering.MarketStorage;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface WxStorageService {
    MarketStorage upload(MultipartFile file, HttpServletRequest request);
}
