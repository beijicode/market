package com.cskaoyan.controller;

import com.cskaoyan.bean.login_demo.AdminInfoBean;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.login_demo.InfoData;
import com.cskaoyan.bean.login_demo.LoginUserData;
import com.cskaoyan.bean.reverseengineering.MarketAdmin;
import com.cskaoyan.service.AuthService;
import com.cskaoyan.service.LogService;
import com.cskaoyan.shiro.MarketToken;
import com.cskaoyan.util.Md5Utils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//shiro整合之后，在做具体的开发
//响应结果都是JSON，Controller组件上我们都用@RestController注解
@RestController
@RequestMapping("admin/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @Autowired
    LogService logService;

    /**
     * 如果参数比较少，类型比较简单的话，使用map来接收也可以
     */
    @PostMapping("login")
    public BaseRespVo<LoginUserData> login(@RequestBody Map map, HttpServletRequest request) {


        String ip = request.getRemoteAddr();
        String username = (String) map.get("username");
        String password = (String) map.get("password");
        password = Md5Utils.getMd5(password);

        logService.insertLog(ip,username,"登录");

        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(new MarketToken(username, password,"admin"));
        } catch (AuthenticationException e) {
            return BaseRespVo.ok(null,"用户帐号或密码不正确",602);
        }

        LoginUserData loginUserData = new LoginUserData();

        MarketAdmin admin = authService.login(username);

        AdminInfoBean adminInfo = new AdminInfoBean();
        adminInfo.setAvatar(admin.getAvatar());
        adminInfo.setNickName(admin.getUsername());

        loginUserData.setAdminInfo(adminInfo);
        loginUserData.setToken((String) subject.getSession().getId());

        return BaseRespVo.ok(loginUserData,"成功",0);
    }

    @RequestMapping("info")
    public BaseRespVo info(String token) {

        Subject subject = SecurityUtils.getSubject();
        String username = null;
        if (subject.isAuthenticated()) {
            //在已经认证成功的情况下，可以获得用户信息
            // 获得的用户信息的来源 → 来源realm的doGetAuthenticationInfo方法的返回值的第一个参数
            username = (String) subject.getPrincipals().getPrimaryPrincipal();
        }

        InfoData infoData = authService.authInfo(username);



        return BaseRespVo.ok(infoData,"成功");
    }

    /**
     * 退出登录
     *
     * @author beiji
     * @since 2022/05/06 10:54
     */
    @RequestMapping("logout")
    public BaseRespVo logout(HttpServletRequest request) {

        Subject subject = SecurityUtils.getSubject();
        logService.insertLog(request.getRemoteAddr(),(String) subject.getPrincipals().getPrimaryPrincipal(),"退出");
        subject.logout();
        return BaseRespVo.ok(null, "成功", 0);
    }
}
