package com.cskaoyan.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author stone
 * @date 2022/01/06 16:00
 */
@RestController
public class HelloController {

    @RequestMapping("hello")
    public String hello() {
        return "hello project2";
    }
    @RequestMapping("hello2")
    public String hello2() {
        return "hello project22";
    }
    @RequestMapping("hello3")
    public String hello3() {
        return "hello project22";
    }
}
