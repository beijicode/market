package com.cskaoyan.controller.issue;

import com.cskaoyan.bean.bo.issue.IssueCreateBO;
import com.cskaoyan.bean.bo.issue.IssueDeleteBO;
import com.cskaoyan.bean.bo.issue.IssueListBO;
import com.cskaoyan.bean.bo.issue.IssueUpdateBO;
import com.cskaoyan.bean.login_demo.BaseRespVO2;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.issue.IssueCreateVO;
import com.cskaoyan.bean.vo.issue.IssueListVO;
import com.cskaoyan.bean.vo.issue.IssueUpdateVO;
import com.cskaoyan.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 
 * @since 2022/05/06 22:36
 * @author tangwei
 */
@RestController
@RequestMapping("admin/issue")
public class IssueController {

    @Autowired
    IssueService issueService;

    /**
     * 显示所有通用问题
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/6 23:43
     */
    @GetMapping("list")
    public BaseRespVo IssueList(IssueListBO issueListBO) {
        IssueListVO issueListVO = issueService.queryAllIssues(issueListBO);
        return BaseRespVo.ok(issueListVO, "成功", 0);
    }

    /**
     * 新建一个通用问题
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/6 23:44
     */
    @PostMapping("create")
    public BaseRespVo createIssue(@RequestBody IssueCreateBO issueCreateBO) {
        IssueCreateVO issueCreateVO = issueService.createIssue(issueCreateBO);
        return BaseRespVo.ok(issueCreateVO, "成功", 0);
    }

    /**
     * 更新一个通用问题
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 8:53
     */
    @PostMapping("update")
    public BaseRespVo updateIssue(@RequestBody IssueUpdateBO issueUpdateBO) {
        IssueUpdateVO issueUpdateVO = issueService.updateIssue(issueUpdateBO);
        return BaseRespVo.ok(issueUpdateBO, "成功", 0);
    }

    /**
     * 删除一个通用问题
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 9:10
     */
    @PostMapping("delete")
    public BaseRespVO2 deletIssue(@RequestBody IssueDeleteBO issueDeleteBO) {
        issueService.deleteIssue(issueDeleteBO);
        return BaseRespVO2.ok(0, "成功");
    }
}