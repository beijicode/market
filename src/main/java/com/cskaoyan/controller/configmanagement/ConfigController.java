package com.cskaoyan.controller.configmanagement;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.configvo.ExpressConfigVO;
import com.cskaoyan.bean.vo.configvo.MallConfigVO;
import com.cskaoyan.bean.vo.configvo.OrderConfigVO;
import com.cskaoyan.bean.vo.configvo.WXConfigVO;
import com.cskaoyan.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */

@RequestMapping("admin/config")
@RestController
public class ConfigController {
    @Autowired
    ConfigService configService;


    /**
     * 方法说明 商场配置管理
     *
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/7 9:20
     */
    @GetMapping("mall")
    public BaseRespVo getMallConfig() {
        MallConfigVO mallConfigVO = configService.getMallConfig();
        return BaseRespVo.ok(mallConfigVO, "成功", 0);
    }

    /**
     * 方法说明 运费配置管理
     *
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/7 11:01
     */
    @GetMapping("express")
    public BaseRespVo getExpressConfig() {
        ExpressConfigVO expressConfigVO = configService.getExpressConfig();
        return BaseRespVo.ok(expressConfigVO, "成功", 0);
    }

    /**
     * 方法说明 订单配置管理
     *
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/7 11:02
     */
    @GetMapping("order")
    public BaseRespVo getOrderConfig() {
        OrderConfigVO orderConfig = configService.getOrderConfig();
        return BaseRespVo.ok(orderConfig, "成功", 0);
    }

    /**
     * 方法说明 小程序配置管理
     *
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/7 11:03
     */
    @GetMapping("wx")
    public BaseRespVo getWXConfig() {
        WXConfigVO wxConfigVO = configService.getWXConfig();
        return BaseRespVo.ok(wxConfigVO, "成功", 0);
    }



    /**
     * 方法说明 修改商场配置
     * @author zhanfuqiang
     * @date 2022/5/7 11:51
     * @param mallConfigVO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @PostMapping("mall")
    public BaseRespVo updateMallConfig(@RequestBody MallConfigVO mallConfigVO) throws IllegalAccessException {
        configService.updateMallConfig(mallConfigVO);
        return BaseRespVo.ok(0,"成功");
    }

    /**
     * 方法说明 修改运费配置
     * @author zhanfuqiang
     * @date 2022/5/7 11:51
     * @param expressConfigVO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @PostMapping("express")
    public BaseRespVo updateExpressConfig(@RequestBody ExpressConfigVO expressConfigVO) throws IllegalAccessException {
        configService.updateExpressConfig(expressConfigVO);
        return BaseRespVo.ok(0,"成功");
    }


    /**
     * 方法说明 修改订单配置
     * @author zhanfuqiang
     * @date 2022/5/7 11:52
     * @param orderConfigVO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @PostMapping("order")
    public BaseRespVo updateOrderConfig(@RequestBody OrderConfigVO orderConfigVO) throws IllegalAccessException {
        configService.updateOrderConfig(orderConfigVO);
        return BaseRespVo.ok(0,"成功");
    }


    /**
     * 方法说明 修改小程序配置
     * @author zhanfuqiang
     * @date 2022/5/7 11:52
     * @param wxConfigVO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @PostMapping("wx")
    public BaseRespVo updateWXConfig(@RequestBody WXConfigVO wxConfigVO) throws IllegalAccessException {
        configService.updateWXConfig(wxConfigVO);
        return BaseRespVo.ok(0,"成功");
    }
}
