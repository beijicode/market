package com.cskaoyan.controller.goodsmanagement;

import com.cskaoyan.bean.bo.goodsbo.CommentBO;
import com.cskaoyan.bean.bo.goodsbo.GoodsListBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.goodsvo.CommentListVO;
import com.cskaoyan.bean.vo.goodsvo.GoodsListVO;
import com.cskaoyan.exception.MyExcpetion;
import com.cskaoyan.service.CommentService;
import com.cskaoyan.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Charadic
 * @since 2022/05/06 20:30
 */

@RestController
@RequestMapping("admin/comment")
public class CommentController {

    @Autowired
    CommentService commentService;

    /**
     *  评论展示
     */
    @RequiresPermissions("admin:comment:list")
    @RequestMapping("list")
    public BaseRespVo list(@Validated CommentBO commentBO, BindingResult bindingResult) throws MyExcpetion {
        if(bindingResult.hasErrors()){
            throw new MyExcpetion("参数错误");
        }
        CommentListVO commentListVO = commentService.list(commentBO);
        return BaseRespVo.ok(commentListVO, "成功");
    }

    /**
     * 评论删除
     * @param comment
     * @return
     */
    @RequiresPermissions("admin:comment:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Map comment) {
        commentService.delete(comment);
        return BaseRespVo.ok(0, "成功");
    }



}
