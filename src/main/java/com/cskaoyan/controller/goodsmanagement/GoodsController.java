package com.cskaoyan.controller.goodsmanagement;

import com.cskaoyan.bean.bo.GoodsCreateBO;
import com.cskaoyan.bean.bo.goodsbo.GoodsListBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.goodsvo.GoodsCatAndBrandVO;
import com.cskaoyan.bean.vo.goodsvo.GoodsDetailVO;
import com.cskaoyan.bean.vo.goodsvo.GoodsListVO;
import com.cskaoyan.exception.MyExcpetion;
import com.cskaoyan.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Charadic
 * @since 2022/05/06 11:29
 */
@RestController
@RequestMapping("admin/goods")
public class GoodsController {

    @Autowired
    GoodsService goodsService;

    /**
     * 商品陈列回显
     */
    @RequiresPermissions("admin:goods:list")
    @RequestMapping("list")
    public BaseRespVo<GoodsListVO> list(GoodsListBO goodsListBO) {
        GoodsListVO goodsListVO = goodsService.list(goodsListBO);
        return BaseRespVo.ok(goodsListVO, "成功");
    }

    /**
     * 商品删除
     * @param goods
     * @return
     */
    @RequiresPermissions("admin:goods:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Map goods) {
        goodsService.delete(goods);
        return BaseRespVo.ok(null, "成功");
    }

    /**
     * 商品详情
     * @param id
     * @return
     */
    @RequiresPermissions("admin:goods:detail")
    @RequestMapping("detail")
    public BaseRespVo detail(Integer id) {
        GoodsDetailVO goodsDetailVO = goodsService.detail(id);
        return BaseRespVo.ok(goodsDetailVO, "成功");
    }

    /**
     * 品牌
     * @return
     */
    @RequiresPermissions("admin:goods:catAndBrand")
    @RequestMapping("catAndBrand")
    public BaseRespVo<GoodsCatAndBrandVO> catAndBrand() {
        GoodsCatAndBrandVO goodsCatAndBrandVO = goodsService.catAndBrand();
        return BaseRespVo.ok(goodsCatAndBrandVO, "成功");
    }

    /**
     * 商品创建
     * @param goodsCreateBO
     * @return
     */
    @RequiresPermissions("admin:goods:create")
    @RequestMapping("create")
    public BaseRespVo create(@Validated @RequestBody GoodsCreateBO goodsCreateBO,BindingResult bindingResult) throws MyExcpetion {
        if(bindingResult.hasErrors()){
            throw new MyExcpetion("参数错误");
        }
        goodsService.create(goodsCreateBO);
        return BaseRespVo.ok(0, "成功");
    }

    /**
     * 商品更新
     * @param goodsUpdateBO
     * @return
     */
    @RequiresPermissions("admin:goods:update")
    @RequestMapping("update")
    public BaseRespVo update(@Validated @RequestBody GoodsCreateBO goodsUpdateBO, BindingResult bindingResult) throws MyExcpetion {
        if(bindingResult.hasErrors()){
            throw new MyExcpetion("参数错误");
        }
        goodsService.update(goodsUpdateBO);
        return BaseRespVo.ok(0, "成功");
    }


}
