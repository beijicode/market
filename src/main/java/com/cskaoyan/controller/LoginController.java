package com.cskaoyan.controller;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Charadic
 * @since 2022/05/10 23:38
 */
@RestController
public class LoginController {

    // 处理没有认证的请求
    @RequestMapping("login.jsp")
    public BaseRespVo login1(){
        return BaseRespVo.ok(null,"未登录",700);
    }

}
