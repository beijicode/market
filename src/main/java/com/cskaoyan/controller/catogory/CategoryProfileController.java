package com.cskaoyan.controller.catogory;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页显示需要的
 * @since 2022/05/06 00:00
 * @author tangwei
 */
@RestController
@RequestMapping("admin/profile")
public class CategoryProfileController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/nnotice")
    public BaseRespVo categoryList() {
        return BaseRespVo.ok(0, "成功", 0);
    }
}