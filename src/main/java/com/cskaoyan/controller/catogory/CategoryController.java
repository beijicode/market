package com.cskaoyan.controller.catogory;

import com.cskaoyan.bean.bo.category.CategoryCreateBO;
import com.cskaoyan.bean.bo.category.CategoryDeleteBO;
import com.cskaoyan.bean.bo.category.CategoryUpdateBO;
import com.cskaoyan.bean.login_demo.BaseRespVO2;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.category.CategoryCreateVO;
import com.cskaoyan.bean.vo.category.CategoryListVO;
import com.cskaoyan.bean.vo.category.CategorySimpleVO;
import com.cskaoyan.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author tangwei
 * @since 2022/05/05 20:19
 */
@RestController
@RequestMapping("admin/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    /**
     * 商品类目首页显示
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/6 14:27
     */
    @GetMapping("list")
    public BaseRespVo categoryList() {
        CategoryListVO categoryListVO = categoryService.queryAllCategory();
        return BaseRespVo.ok(categoryListVO, "成功", 0);
    }

    @GetMapping("l1")
    public BaseRespVo categoryL1() {
        CategorySimpleVO categorySimpleVO = categoryService.queryCategorySimple();
        return BaseRespVo.ok(categorySimpleVO, "成功", 0);
    }

    /**
     * 添加类目
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/6 14:28
     */
    @PostMapping("create")
    public BaseRespVo createCategory(@RequestBody CategoryCreateBO categoryCreateBO) {
        categoryService.createCategory(categoryCreateBO);
        Integer categoryCreateBOId = categoryCreateBO.getId();
        CategoryCreateVO categoryCreateVO = categoryService.queryOneCategoryById(categoryCreateBOId);
        return BaseRespVo.ok(categoryCreateVO, "成功", 0);
    }

    /**
     * 更新类目
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/6 21:51
     */
    @PostMapping("update")
    public BaseRespVO2 updateCategory(@RequestBody CategoryUpdateBO categoryUpdateBO) {
        int code = categoryService.updateCategory(categoryUpdateBO);
        if (code == 0) {
            return BaseRespVO2.ok(0, "成功");
        } else {
            return BaseRespVO2.ok(401, "参数不对");
        }
    }

    /**
     * 删除类目
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/6 21:51
     */
    @PostMapping("delete")
    public BaseRespVO2 deleteCategory(@RequestBody CategoryDeleteBO categoryDeleteBO) {
        categoryService.deleteCategory(categoryDeleteBO);
        return BaseRespVO2.ok(0, "成功");
    }
}