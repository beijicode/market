package com.cskaoyan.controller.marketmanagement;

import com.cskaoyan.bean.login_demo.AdminInfoBean;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.login_demo.InfoData;
import com.cskaoyan.bean.login_demo.LoginUserData;
import com.cskaoyan.bean.vo.RegionListVO;
import com.cskaoyan.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/5
 */
@RestController
@RequestMapping("admin/region")
public class RegionController {

    @Autowired
    BrandService brandService;

    /**
     * 方法说明 获得全国行政区域
     * @author zhanfuqiang
     * @date 2022/5/6 11:04
     * @param
     * @return com.cskaoyan.bean.vo.RegionListVO
     */
    @GetMapping("list")
    public RegionListVO getRegionList() {

        RegionListVO regionListVO = new RegionListVO();
        regionListVO.setErrno(0);
        regionListVO.setErrmsg("成功");
        regionListVO = brandService.getRegionList(regionListVO);

        return regionListVO;
    }

}
