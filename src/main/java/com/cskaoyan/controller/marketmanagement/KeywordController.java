package com.cskaoyan.controller.marketmanagement;

import com.cskaoyan.bean.bo.keywordbo.KeywordDeleteBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordCreateBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordListBO;
import com.cskaoyan.bean.bo.keywordbo.KeywordUpdateBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.keywordvo.KeywordCreateVO;
import com.cskaoyan.bean.vo.keywordvo.KeywordListVO;
import com.cskaoyan.bean.vo.keywordvo.KeywordUpdateVO;
import com.cskaoyan.service.KeywordService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Charadic
 * @since 2022/05/05 19:55
 */

@RestController
@RequestMapping("admin/keyword")
public class KeywordController {

    @Autowired
    KeywordService keywordService;

    /**
     * 关键词展示
     */
    @RequiresPermissions("admin:keyword:list")
    @RequestMapping("list")
    public BaseRespVo<KeywordListVO> list(KeywordListBO keywordListBO) {
        KeywordListVO keywordListVO = keywordService.query(keywordListBO);
        return BaseRespVo.ok(keywordListVO,"成功");
    }

    /**
     * 关键词创建
     */
    @RequiresPermissions("admin:keyword:create")
    @RequestMapping("create")
    public BaseRespVo<KeywordCreateVO> create(@RequestBody KeywordCreateBO keywordCreateBO) {
        KeywordCreateVO keywordCreateVO = keywordService.create(keywordCreateBO);
        return BaseRespVo.ok(keywordCreateVO,"成功");
    }

    /**
     * 关键词修改
     */
    @RequiresPermissions("admin:keyword:update")
    @RequestMapping("update")
    public BaseRespVo<KeywordUpdateVO> update(@RequestBody KeywordUpdateBO keywordUpdateBO) {
        KeywordUpdateVO keywordCreateVO = keywordService.update(keywordUpdateBO);
        return BaseRespVo.ok(keywordCreateVO,"成功");
    }

    /**
     * 关键词逻辑删除
     */
    @RequiresPermissions("admin:keyword:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody KeywordDeleteBO keywordDeleteBO) {
        keywordService.delete(keywordDeleteBO);
        return BaseRespVo.ok(null,"成功");
    }
}
