package com.cskaoyan.controller.marketmanagement;

import com.cskaoyan.bean.bo.brandbo.BrandCreateBO;
import com.cskaoyan.bean.bo.brandbo.BrandDeleteBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.brandvo.BrandListVO;
import com.cskaoyan.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
@RequestMapping("admin/brand")
@RestController
public class BrandController {

    @Autowired
    BrandService brandService;

    /**
     * 方法说明:获得品牌制造商列表
     *
     * @param page
     * @param limit
     * @param id
     * @param name
     * @return com.cskaoyan.bean.login_demo.BaseRespVo<com.cskaoyan.bean.vo.brandvo.BrandListVO>
     * @author zhanfuqiang
     * @date 2022/5/6 11:49
     */
    @RequestMapping("list")
    public BaseRespVo<BrandListVO> list(Integer page, Integer limit, String sort, String order, Integer id, String name) {
        BrandListVO brandListVO = brandService.list(page, limit, sort, order, id, name);
        return BaseRespVo.ok(brandListVO, "成功", 0);
    }


    /**
     * 方法说明 创建新的商品品牌
     * @author zhanfuqiang
     * @date 2022/5/6 19:16
     * @param brandCreateBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody BrandCreateBO brandCreateBO) {

        BrandDeleteBO brandDeleteBO = brandService.create(brandCreateBO);

        if (brandDeleteBO != null) {
            return BaseRespVo.ok(brandDeleteBO, "成功", 0);
        }
        return BaseRespVo.invalidData("更新失败");
    }


    /**
     * 方法说明 更新品牌信息
     *
     * @param brandDeleteBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/6 16:55
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody BrandDeleteBO brandDeleteBO) {
        Integer row = brandService.update(brandDeleteBO);

        if (row == 1) {
            return BaseRespVo.ok(brandDeleteBO, "成功", 0);
        }
        return BaseRespVo.invalidData("更新失败");
    }


    /**
     * 方法说明 删除品牌
     *
     * @param brandDeleteBO
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     * @author zhanfuqiang
     * @date 2022/5/6 16:18
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody BrandDeleteBO brandDeleteBO) {
        Integer row = brandService.delete(brandDeleteBO);
        if (row == 1) {
            return BaseRespVo.ok(0, "成功");
        }
        return BaseRespVo.invalidData("删除失败");
    }
}
