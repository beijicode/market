package com.cskaoyan.controller;

import com.cskaoyan.bean.reverseengineering.MarketStorage;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.StorageListVO;
import com.cskaoyan.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * 创建日期: 2022/05/05 21:45
 *
 * @author yangfan
 */
@RestController
@RequestMapping("admin/storage")
public class StorageController {


    @Autowired
    StorageService storageService;

    // 图片上传
    @RequestMapping("create")
    public BaseRespVo create(MultipartFile file, HttpServletRequest request){

        MarketStorage marketStorage = storageService.insert(file,request);

        return BaseRespVo.ok(marketStorage,"成功",0);
    }

    /**
     * 系统管理，对象存储，列表显示
     * @param name
     * @param key
     * @param param
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo storageList(String name, String key,BaseParam param){

        StorageListVO storageListVO = storageService.query(name,key,param);

        return BaseRespVo.ok(storageListVO,"成功",0);
    }

    /**
     * 系统管理，对象存储，删除对象
     * @param marketStorage
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo storageDelete(@RequestBody MarketStorage marketStorage){

        int affectedRows = storageService.storageDelete(marketStorage);

        if (affectedRows != 1){
            return BaseRespVo.ok(null,"删除失败",500);
        }

        return BaseRespVo.ok(marketStorage,"成功",0);
    }

    /**
     * 系统管理，对象存储，更新对象
     * @param marketStorage
     * @return
     */
    @RequestMapping("update")
    public BaseRespVo storageUpdate(@RequestBody MarketStorage marketStorage){

        int affectedRows = storageService.storageUpdate(marketStorage);

        if (affectedRows != 1){
            return BaseRespVo.ok(null,"更新失败",500);
        }

        return BaseRespVo.ok(marketStorage,"成功",0);
    }
}
