package com.cskaoyan.controller.statGraph;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.statvo.GoodStatVO;
import com.cskaoyan.bean.vo.statvo.OrderStatVO;
import com.cskaoyan.bean.vo.statvo.UserStatVO;
import com.cskaoyan.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
@RequestMapping("admin/stat")
@RestController
public class StatController {

    @Autowired
    StatService statService;


    /**
     * 方法说明 获得用户状态
     * @author zhanfuqiang
     * @date 2022/5/6 19:32
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("user")
    public BaseRespVo getUser() {
        UserStatVO userStatVO = statService.getUser();
        return BaseRespVo.ok(userStatVO, "成功", 0);
    }


    /**
     * 方法说明 获得商品状态
     * @author zhanfuqiang
     * @date 2022/5/6 20:04
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("goods")
    public BaseRespVo getGoods() {
        GoodStatVO goodStatVO = statService.getGoods();
        return BaseRespVo.ok(goodStatVO, "成功", 0);
    }

    /**
     * 方法说明 统计订单信息
     * @author zhanfuqiang
     * @date 2022/5/6 21:58
     * @param
     * @return com.cskaoyan.bean.login_demo.BaseRespVo
     */
    @RequestMapping("order")
    public BaseRespVo getOrders() {
        OrderStatVO orderStatVO = statService.getOrders();
        return BaseRespVo.ok(orderStatVO, "成功", 0);
    }
}
