package com.cskaoyan.controller.promote.ad;

import com.cskaoyan.bean.reverseengineering.MarketAd;
import com.cskaoyan.bean.bo.adbo.AdListBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.promote.adVO.AdData;
import com.cskaoyan.service.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 创建日期: 2022/05/05 17:49
 * 广告管理部分
 * @author yangfan
 */
@RestController
@RequestMapping("admin/ad")
public class AdController {


    @Autowired
    AdService adService;

    // 增加广告
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketAd marketAd) {

        MarketAd marketAdVO = adService.create(marketAd);

        return BaseRespVo.ok(marketAdVO,"成功",0);
    }


    // 修改广告
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketAd marketAd){

        MarketAd marketAdVO = adService.update(marketAd);

        return BaseRespVo.ok(marketAdVO,"成功",0);
    }

    // 删除广告
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketAd marketAd){

        adService.delete(marketAd);

        return BaseRespVo.ok(null,"成功",0);
    }


    // 查询广告
    // 请求网址: http://182.92.235.201:8083/admin/ad/list?page=1&limit=20&sort=add_time&order=desc
    // http://localhost:8083/admin/ad/list?page=1&limit=20&name=%E6%B4%BB%E5%8A%A8&sort=add_time&order=desc
    @RequestMapping("list")
    public BaseRespVo list(AdListBO adListBO) {

        AdData adData = adService.list(adListBO);
        return BaseRespVo.ok(adData,"成功",0);


    }




}
