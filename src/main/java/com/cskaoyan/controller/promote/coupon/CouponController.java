package com.cskaoyan.controller.promote.coupon;

import com.cskaoyan.bean.reverseengineering.MarketCoupon;
import com.cskaoyan.bean.bo.couponbo.CouponBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.vo.promote.couponVO.CouponData;
import com.cskaoyan.bean.vo.promote.couponVO.CouponListuserVO;
import com.cskaoyan.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 创建日期: 2022/05/06 15:59
 *
 * @author yangfan
 */
@RestController
@RequestMapping("admin/coupon")
public class CouponController {
    @Autowired
    CouponService couponService;

    // 显示优惠券
    // http://182.92.235.201:8083/admin/coupon/list?page=1&limit=20&sort=add_time&order=desc
    // http://182.92.235.201:8083/admin/coupon/list?page=1&limit=20&name=1&type=0&status=0&sort=add_time&order=desc
    @RequestMapping("list")
    public BaseRespVo list(CouponBO couponBO) {

        CouponData couponData = couponService.list(couponBO);

        return BaseRespVo.ok(couponData,"成功",0);
    }

    // 添加优惠券
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketCoupon marketCoupon) {

        MarketCoupon marketCouponVO= couponService.create(marketCoupon);
        if (marketCouponVO.getDiscount().intValue() > marketCouponVO.getMin().intValue()) {
            return BaseRespVo.ok(666,"优惠金额必须小于消费金额");
        }
        if (marketCouponVO.getStartTime().compareTo(marketCouponVO.getEndTime()) > 0) {
            return BaseRespVo.ok(667,"优惠券过期时间必须大于优惠券开始时间");
        }


        return BaseRespVo.ok(marketCouponVO,"成功",0);
    }

    // 优惠券详情页
    @RequestMapping("read")
    public BaseRespVo read(Integer id) {

        MarketCoupon marketCoupon = couponService.read(id);

        return BaseRespVo.ok(marketCoupon,"成功",0);
    }

    // 修改优惠券
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketCoupon marketCoupon) {

        MarketCoupon marketCouponVO = couponService.update(marketCoupon);

        return BaseRespVo.ok(marketCouponVO,"成功",0);
    }

    // 删除优惠券
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketCoupon marketCoupon) {

        couponService.delete(marketCoupon);

        return BaseRespVo.ok(null,"成功");
    }

    // 显示listuser
    @RequestMapping("listuser")
    public BaseRespVo listuser(CouponBO couponBO) {

        CouponListuserVO couponListuserVO = couponService.listuser(couponBO);

        return BaseRespVo.ok(couponListuserVO,"成功",0);
    }
}
