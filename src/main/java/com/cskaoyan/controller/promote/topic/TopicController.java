package com.cskaoyan.controller.promote.topic;


import com.cskaoyan.bean.bo.topicbo.TopicBO;
import com.cskaoyan.bean.bo.topicbo.TopicIntegerBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketTopic;
import com.cskaoyan.bean.vo.promote.topicVO.TopicData;
import com.cskaoyan.bean.vo.promote.topicVO.TopicReadVO;
import com.cskaoyan.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 创建日期: 2022/05/06 20:04
 *
 * @author yangfan
 */
@RestController
@RequestMapping("admin/topic")
public class TopicController {

    @Autowired
    TopicService topicService;

    // 显示topic
    // http://182.92.235.201:8083/admin/topic/list?
    // page=1&limit=20&title=zhuanti&subtitle=zibiaoti&sort=add_time&order=desc
    @RequestMapping("list")
    public BaseRespVo list(TopicBO topicBO) {

        TopicData topicData = topicService.list(topicBO);

        return BaseRespVo.ok(topicData,"成功",0);
    }


    // 增加topic
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketTopic marketTopic) {

        MarketTopic marketTopicVO = topicService.create(marketTopic);

        return BaseRespVo.ok(marketTopicVO,"成功",0);
    }

    // 编辑回显
    @RequestMapping("read")
    public BaseRespVo read(Integer id) {


        TopicReadVO topicReadVO = topicService.selectByTopicId(id);

        return BaseRespVo.ok(topicReadVO,"成功",0);

    }

    // 编辑topic
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketTopic marketTopic) {

        MarketTopic marketTopicVO = topicService.update(marketTopic);

        return BaseRespVo.ok(marketTopicVO,"成功",0);
    }


    // 单一删除
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketTopic marketTopic) {

        topicService.delete(marketTopic);

        return BaseRespVo.ok(null,"成功",0);
    }

    // 批量删除
    @RequestMapping("batch-delete")
    public BaseRespVo batchDelete(@RequestBody TopicIntegerBO ids) {


        topicService.batchDelete(ids);

        return BaseRespVo.ok(null,"成功",0);
    }

}
