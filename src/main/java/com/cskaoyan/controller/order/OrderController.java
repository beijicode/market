package com.cskaoyan.controller.order;

import com.cskaoyan.bean.bo.order.OrderListBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.reverseengineering.MarketOrder;
import com.cskaoyan.bean.vo.order.ExpressListVO;
import com.cskaoyan.bean.vo.order.OrderDetailVO;
import com.cskaoyan.bean.vo.order.OrderListVO;
import com.cskaoyan.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author tangwei
 * @since 2022/05/05 23:16
 */
@RestController
@RequestMapping("admin/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    /**
     * 显示所有订单，搜索订单也是这个接口
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 12:25
     */
    @GetMapping("/list")
    public BaseRespVo OrderList(OrderListBO orderListBO) {
        Integer orderId = orderListBO.getOrderId();
        if (orderId != null) {
            return BaseRespVo.ok(502, "系统内部错误");
        } else {
            OrderListVO<MarketOrder> orderListVOList = orderService.queryAllOrders(orderListBO);
            return BaseRespVo.ok(orderListVOList, "成功", 0);
        }
    }

    /**
     * 显示所有快递公司
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 16:36
     */
    @GetMapping("/channel")
    public BaseRespVo ExpressList() {
        List<ExpressListVO> expressListVOList = orderService.queryAllExpress();
        return BaseRespVo.ok(expressListVOList, "成功", 0);
    }

    /**
     * 删除一个订单
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 17:18
     */
    @PostMapping("/delete")
    public BaseRespVo deleteOneOrder(@RequestBody Map orderId) {
        // 101 未付款 102 用户取消 103 系统取消可以删除，其他不能删除
        boolean canDelete = orderService.deleteOneOrder(orderId);
        if (canDelete == false) {

            return BaseRespVo.ok(623, "该订单不能删除");
        }
        return BaseRespVo.ok(0, "成功");
    }

    /**
     * 查询一个订单的详情
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 19:34
     */
    @GetMapping("/detail")
    public BaseRespVo OrderDetail(Integer id) {
        OrderDetailVO orderDetailVO = orderService.queryOrderInfoBy(id);
        return BaseRespVo.ok(orderDetailVO, "成功", 0);
    }

    /**
     * 订单发货
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 20:57
     */
    @PostMapping("/ship")
    public BaseRespVo OrderShip(@RequestBody Map orderShip) {
        orderService.shipOrder(orderShip);
        return BaseRespVo.ok(0, "成功");
    }

    /**
     * 订单退款
     *
     * @param
     * @return
     * @author tangwei
     * @since 2022/5/7 21:14
     */
    @PostMapping("/refund")
    public BaseRespVo OrderRefund(@RequestBody Map orderRefund) {
        orderService.refundOrder(orderRefund);
        return BaseRespVo.ok(0, "成功");
    }

    /**
     * 评论回复
     *
     * @param comment
     * @return
     */

    @RequestMapping("reply")
    public BaseRespVo reply(@RequestBody Map comment) {
        int code = orderService.reply(comment);
        if (code == 622) {
            return BaseRespVo.ok(622, "订单商品已回复！");
        }
        return BaseRespVo.ok(null, "成功");
    }
}
