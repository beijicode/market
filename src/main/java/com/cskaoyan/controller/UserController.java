package com.cskaoyan.controller;

import com.cskaoyan.bean.bo.ProfilePasswordBO;
import com.cskaoyan.bean.login_demo.*;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.DashboardVO;
import com.cskaoyan.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author stone&beiji
 * @date 2022/01/06 16:23
 */
@RequestMapping("admin")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @RequiresPermissions("admin:user:list")
    @RequestMapping("user/list")
    public BaseRespVo userList(String username,String mobile,BaseParam param) {
        UserData userData = userService.query(param,username,mobile);
        return BaseRespVo.ok(userData,"成功");
    }

    /**
     * 首页
     * @author beiji
     * @since 2022/05/06 10:57
     */
    @RequestMapping("dashboard")
    public BaseRespVo dashboard(){
        DashboardVO dashboardVO = userService.dashboard();
        return BaseRespVo.ok(dashboardVO, "成功");
    }
    /**
     * 密码修改
     * @author beiji
     * @since 2022/05/10 16:35
     */
    @PostMapping("profile/password")
    public BaseRespVo updatePassword(@RequestBody ProfilePasswordBO profilePasswordBO){
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipals().getPrimaryPrincipal();
        int status = userService.updateAdminPassword(username,profilePasswordBO);
        if(status == 1){
            return BaseRespVo.ok(0,"成功");
        }
        return BaseRespVo.ok(605,"账号密码不正确");
    }
    /**
     * 会员管理模块下，查看详情后保存修改功能
     * @author beiji
     * @since 2022/05/06 23:11
     */

    @PostMapping("user/update")
    public BaseRespVo update(@RequestBody MarketUser user){
        System.out.println(user);
        int i = userService.updateUser(user);
        return BaseRespVo.ok(1,"成功",0);

        //TODO 和微信小程序模块有联动，待完成
        // if(i == 1){
        //     return BaseRespVo.ok(1,"成功",0);
        // }
        // return BaseRespVo.ok(null,"",500);
    }
    /**
     * 用户管理模块下，会员管理功能根据用户id查找
     * @author beiji
     * @since 2022/05/06 23:13
     */
    @RequestMapping("user/detail")
    public BaseRespVo detail(Integer id){
        MarketUser user = userService.detail(id);
        return BaseRespVo.ok(user,"成功",0);
    }
    /**
     * 用户管理模块下，收货地址显示和查找功能
     * @author beiji
     * @since 2022/05/06 23:13
     */
    @RequestMapping("address/list")
    public BaseRespVo addressList(String name,Integer userId, BaseParam param){
        AddressData addressData = userService.queryAddr(name,userId,param);
        return BaseRespVo.ok(addressData,"成功");
    }
    /**
     * 用户管理模块下，会员收藏显示和查找功能
     * @author beiji
     * @since 2022/05/07 12:20
     */
    @RequestMapping("collect/list")
    public BaseRespVo collectList(Integer userId,Integer valueId,BaseParam param){
        CollectData collectData = userService.queryCollect(userId,valueId,param);
        return BaseRespVo.ok(collectData,"成功");
    }
    /**
     * 用户管理模块下，会员足迹显示和查找功能
     * @author beiji
     * @since 2022/05/07 12:21
     */
    @RequestMapping("footprint/list")
    public BaseRespVo footprintList(Integer userId,Integer goodsId, BaseParam param){
        FootprintData footprintData = userService.queryFoot(userId,goodsId,param);
        return BaseRespVo.ok(footprintData,"成功");
    }
    /**
     * 用户管理模块下，搜索历史显示和查找功能
     * @author beiji
     * @since 2022/05/07 12:22
     */
    @RequestMapping("history/list")
    public BaseRespVo historyList(Integer userId,String keyword,BaseParam param){
        HistoryData historyData = userService.querySearchHistory(userId,keyword,param);
        return BaseRespVo.ok(historyData,"成功");
    }
    /**
     * 用户管理模块下，意见反馈显示和查找功能
     * @author beiji
     * @since 2022/05/07 12:22
     */
    @RequestMapping("feedback/list")
    public BaseRespVo feedbackList(String username,Integer id,BaseParam param){
        FeedbackData feedbackData = userService.queryFeedback(username,id,param);
        return BaseRespVo.ok(feedbackData,"成功");
    }



}
