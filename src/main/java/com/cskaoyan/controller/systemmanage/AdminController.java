package com.cskaoyan.controller.systemmanage;

import com.cskaoyan.bean.reverseengineering.MarketAdmin;
import com.cskaoyan.bean.bo.AdminCreateBO;
import com.cskaoyan.bean.bo.AdminDeleteBO;
import com.cskaoyan.bean.bo.AdminUpdateBO;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.AdminListVO;
import com.cskaoyan.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/5 17:45
 */
@RequestMapping("admin")
@RestController
public class AdminController {

    @Autowired
    AdminService adminService;

    /**
     * 管理员管理，管理员列表
     * @param username
     * @param param
     * @return
     */
    @RequestMapping("admin/list")
    public BaseRespVo adminList(String username, BaseParam param){

        AdminListVO adminListVO = adminService.query(param,username);
        return BaseRespVo.ok(adminListVO,"成功");
    }

    /**
     * 管理员管理，新增管理员
     * @return
     */
    @RequestMapping("admin/create")
    public BaseRespVo adminCreate(@RequestBody AdminCreateBO adminCreateBO){

        // 判断管理员是否已存在
        int count = adminService.selectAdminByUsername(adminCreateBO.getUsername());
        if (count != 0){
            return BaseRespVo.ok(null,"管理员已经存在",602);
        }
        if (adminCreateBO.getUsername().length() < 6){
            return BaseRespVo.ok(null,"管理员名称不符合规定",601);
        }
        if (adminCreateBO.getPassword().length() < 6){
            return BaseRespVo.ok(null,"管理员密码长度不能小于6",602);
        }

        MarketAdmin marketAdmin = adminService.adminCreate(adminCreateBO);
        return BaseRespVo.ok(marketAdmin,"成功",0);
    }

    /**
     * 管理员管理，修改管理员
     * @param adminUpdateBO
     * @return
     */
    @RequestMapping("admin/update")
    public BaseRespVo adminUpdate(@RequestBody AdminUpdateBO adminUpdateBO){

        // 判断更新的用户名是否已有
        int count = adminService.selectAdminByUsername(adminUpdateBO.getUsername());
        MarketAdmin marketAdmin1 = adminService.SelectAdminById(adminUpdateBO.getId());
        if (count != 0 && !marketAdmin1.getUsername().equals(adminUpdateBO.getUsername())){
            return BaseRespVo.ok(null,"该用户名已存在，更新失败",500);
        }
        if (adminUpdateBO.getUsername().length() < 6){
            return BaseRespVo.ok(null,"管理员名称不符合规定",601);
        }
        if (adminUpdateBO.getPassword().length() < 6){
            return BaseRespVo.ok(null,"管理员密码长度不能小于6",602);
        }
        // 更新管理员
        int affectedRows = adminService.adminUpdate(adminUpdateBO);
        if (affectedRows != 1){
            return BaseRespVo.ok(null,"管理员更新失败",500);
        }

        // 将更新的该管理员返回回去
        MarketAdmin marketAdmin = adminService.SelectAdminById(adminUpdateBO.getId());
        return BaseRespVo.ok(marketAdmin,"成功",0);
    }

    /**
     * 管理员管理，删除管理员
     * @return
     */
    @RequestMapping("admin/delete")
    public BaseRespVo adminDelete(@RequestBody AdminDeleteBO adminDeleteBO){

        int affectedRows = adminService.adminDelete(adminDeleteBO);

        if (affectedRows != 1){
            BaseRespVo.ok(null,"删除管理员失败！",500);
        }

        return BaseRespVo.ok(null,"成功",0);
    }
}
