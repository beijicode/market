package com.cskaoyan.controller.systemmanage;

import com.cskaoyan.bean.bo.PermissionBO;
import com.cskaoyan.bean.reverseengineering.MarketPermission;
import com.cskaoyan.bean.reverseengineering.MarketRole;
import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.RoleListVO;
import com.cskaoyan.bean.vo.RoleOptionsVO;
import com.cskaoyan.bean.vo.permissionvo.RolePermissionsVO;
import com.cskaoyan.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @description:
 * @author: lxc
 * @date: 2022/5/5 22:36
 */
@RequestMapping("admin")
@RestController
public class RoleController {

    @Autowired
    RoleService roleService;

    /**
     * 系统管理，角色管理，显示所有角色
     * @param name
     * @param param
     * @return
     */
    @RequestMapping("role/list")
    public BaseRespVo roleList(String name, BaseParam param){

        RoleListVO roleListVO = roleService.query(param,name);
        return BaseRespVo.ok(roleListVO,"成功");
    }

    /**
     * 系统管理，角色管理，创建角色
     * @param marketRole
     * @return
     */
    @RequestMapping("role/create")
    public BaseRespVo roleCreate(@RequestBody MarketRole marketRole){

        // 判断角色名称是否存在
        Long count = roleService.selectRole(marketRole.getName());
        if (count != 0){
            return BaseRespVo.ok(null,"角色已经存在",640);
        }
        // 插入数据
        MarketRole marketRole1 = roleService.roleCreate(marketRole);

        return BaseRespVo.ok(marketRole1,"成功");
    }

    /**
     * 系统管理，角色管理，管理员页面显示options
     * @return
     */
    @RequestMapping("role/options")
    public BaseRespVo roleOptions(){

        RoleOptionsVO roleOptionsVO = roleService.selectRoleOptions();
        return BaseRespVo.ok(roleOptionsVO,"成功");
    }

    /**
     * 系统管理，角色管理，删除角色
     * @return
     */
    @RequestMapping("role/delete")
    public BaseRespVo roleDelete(@RequestBody MarketRole marketRole){

        int affectedRows = roleService.roleDelete(marketRole);

        if (affectedRows != 1){
            BaseRespVo.ok(null,"删除角色失败！",500);
        }

        return BaseRespVo.ok(null,"成功",0);
    }

    /**
     * 系统管理，角色管理，修改角色
     * @param marketRole
     * @return
     */
    @RequestMapping("role/update")
    public BaseRespVo roleUpdate(@RequestBody MarketRole marketRole){

        // 查看所修改的昵称是否有重复
        Long count = roleService.selectRole(marketRole.getName());
        if (count != 0){
            return BaseRespVo.ok(null,"该用户名重复",500);
        }

        int affectedRows = roleService.roleUpdate(marketRole);
        if (affectedRows != 1){
            BaseRespVo.ok(null,"删除更新失败！",500);
        }

        return BaseRespVo.ok(null,"成功",0);
    }

    /**
     * 系统管理，角色管理，权限授权
     * @param roleId
     * @return
     */
    @GetMapping("role/permissions")
    public BaseRespVo rolePermissions(Integer roleId){

        RolePermissionsVO rolePermissionsVO = roleService.rolePermissions(roleId);

        return BaseRespVo.ok(rolePermissionsVO,"成功",0);
    }

    @PostMapping("role/permissions")
    public BaseRespVo rolePermissions(@RequestBody PermissionBO permissionBO){

        if (permissionBO.getRoleId() == 1){
            return BaseRespVo.ok(null,"当前角色的超级权限不能变更",500);
        }

        roleService.rolePermissionsUpdate(permissionBO.getPermissions(),permissionBO.getRoleId());
        return BaseRespVo.ok(null,"成功",0);
    }
}
