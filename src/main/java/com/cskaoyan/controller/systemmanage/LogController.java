package com.cskaoyan.controller.systemmanage;

import com.cskaoyan.bean.login_demo.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.LogListVO;
import com.cskaoyan.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 23:33
 */
@RequestMapping("admin")
@RestController
public class LogController {

    @Autowired
    LogService logService;

    @RequestMapping("log/list")
    public BaseRespVo logList(String name, BaseParam param){
        LogListVO logListVO = logService.query(param,name);
        return BaseRespVo.ok(logListVO,"成功");
    }
}
