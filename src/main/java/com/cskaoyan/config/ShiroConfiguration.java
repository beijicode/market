package com.cskaoyan.config;

import com.cskaoyan.shiro.CustomAuthenticator;
import com.cskaoyan.shiro.AdminRealm;
import com.cskaoyan.shiro.CustomShiroSessionManager;
import com.cskaoyan.shiro.WxRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author Charadic
 * @since 2022/05/09 15:46
 */
@Configuration
public class ShiroConfiguration {

    @Bean
    public ShiroFilterFactoryBean shiroFilter(DefaultWebSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();



        // beiji 此处添加匿名Filter
        filterChainDefinitionMap.put("/admin/auth/login", "anon");
        filterChainDefinitionMap.put("/admin/auth/logout", "anon");


        // 此处添加认证Filter
        filterChainDefinitionMap.put("/admin/**", "authc");

        // Charadic 微信认证部分
        filterChainDefinitionMap.put("/wx/footprint/*", "authc");
        filterChainDefinitionMap.put("/wx/coupon/mylist", "authc");
        filterChainDefinitionMap.put("/wx/coupon/selectlist", "authc");
        filterChainDefinitionMap.put("/wx/coupon/receive", "authc");
        filterChainDefinitionMap.put("/wx/coupon/exchange", "authc");
        filterChainDefinitionMap.put("/wx/cart/*", "authc");
        filterChainDefinitionMap.put("/wx/collect/*", "authc");
        filterChainDefinitionMap.put("/wx/address/*", "authc");
        filterChainDefinitionMap.put("/wx/order/*","authc");


        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }

    @Bean
    public DefaultWebSecurityManager securityManager(AdminRealm adminRealm,
                                                     CustomShiroSessionManager shiroSessionManager,
                                                     CustomAuthenticator authenticator) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(adminRealm);
        securityManager.setAuthenticator(authenticator);
        securityManager.setSessionManager(shiroSessionManager);
        return securityManager;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    @Bean
    public CustomAuthenticator authenticator(AdminRealm adminRealm, WxRealm wxRealm) {
        CustomAuthenticator customAuthenticator = new CustomAuthenticator();
        ArrayList<Realm> realms = new ArrayList<>();
        realms.add(adminRealm);
        realms.add(wxRealm);
        customAuthenticator.setRealms(realms);
        return customAuthenticator;
    }
}
