package com.cskaoyan.shiro;

import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * @author Charadic
 * @since 2022/05/09 15:54
 */
@Component
public class  CustomShiroSessionManager extends DefaultWebSessionManager {

    private static final String ADMIN_HEADER = "X-CskaoyanMarket-Admin-Token";
    private static final String WX_HEADER = "X-CskaoyanMarket-Token";


    @Override
    protected Serializable getSessionId(ServletRequest servletRequest, ServletResponse response) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String AdminSessionId = request.getHeader(ADMIN_HEADER);
        if (AdminSessionId != null && !"".equals(AdminSessionId)) {
            return AdminSessionId;
        }
        String WxSessionId = request.getHeader(WX_HEADER);
        if (WxSessionId != null && !"".equals(WxSessionId)) {
            return WxSessionId;
        }

        return super.getSessionId(servletRequest, response);
    }
}
