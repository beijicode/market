package com.cskaoyan.shiro;

import com.cskaoyan.bean.reverseengineering.MarketAdmin;
import com.cskaoyan.bean.reverseengineering.MarketUser;
import com.cskaoyan.mapper.MarketUserMapper;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Charadic
 * @since 2022/05/09 22:04
 */
@Component
public class WxRealm extends AuthorizingRealm {
    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        MarketUser marketUser = marketUserMapper.queryByUsername(username);
        String password = marketUser.getPassword();
        return new SimpleAuthenticationInfo(username, password, getName());
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }


}
