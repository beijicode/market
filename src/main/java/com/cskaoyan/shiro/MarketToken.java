package com.cskaoyan.shiro;

import lombok.Data;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author Charadic
 * @since 2022/05/09 21:42
 */

@Data
public class MarketToken extends UsernamePasswordToken {
    String type;

    public MarketToken(String username, String password, String type) {
        super(username, password);
        this.type = type;
    }
}
