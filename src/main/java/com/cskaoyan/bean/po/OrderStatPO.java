package com.cskaoyan.bean.po;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class OrderStatPO {

    /**
     * amount : 10
     * orders : 1
     * customers : 2
     * day : 2022-05-05
     * pcr : 1
     */
    private Double amount;
    private Integer orders;
    private Integer customers;
    private String day;
    private Double pcr;

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public void setCustomers(Integer customers) {
        this.customers = customers;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setPcr(Double pcr) {
        this.pcr = pcr;
    }

    public Double getAmount() {
        return amount;
    }

    public Integer getOrders() {
        return orders;
    }

    public Integer getCustomers() {
        return customers;
    }

    public String getDay() {
        return day;
    }

    public Double getPcr() {
        return pcr;
    }
}
