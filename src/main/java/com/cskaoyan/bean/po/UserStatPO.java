package com.cskaoyan.bean.po;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class UserStatPO {

    /**
     * day : 2019-04-20
     * users : 1
     */
    private String day;
    private Integer users;

    public void setDay(String day) {
        this.day = day;
    }

    public void setUsers(Integer users) {
        this.users = users;
    }

    public String getDay() {
        return day;
    }

    public Integer getUsers() {
        return users;
    }
}
