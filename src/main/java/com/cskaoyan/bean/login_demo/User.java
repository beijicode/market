package com.cskaoyan.bean.login_demo;

import lombok.Data;

/**
 * @author stone
 * @date 2022/01/06 16:33
 */
@Data
public class User {
        private Integer id;
        private String username;
        private String password;
        private Integer gender;
        private String lastLoginTime;
        private String lastLoginIp;
        private Integer userLevel;
        private String nickname;
        private String mobile;
        private String avatar;
        private String weixinOpenid;
        private String sessionKey;
        private Integer status;
        private String addTime;
        private String updateTime;
        private Boolean deleted;
}
