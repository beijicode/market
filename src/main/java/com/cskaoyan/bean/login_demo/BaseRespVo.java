package com.cskaoyan.bean.login_demo;

import lombok.Data;

/**
 * @author stone
 * @date 2022/01/06 16:24
 */
@Data
public class BaseRespVo<T> {
    T data;
    String errmsg;
    int errno;

    public static <T> BaseRespVo ok(T data) {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setData(data);
        return baseRespVo;
    }
    public static <T> BaseRespVo ok(T data, String errmsg){
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrmsg(errmsg);
        baseRespVo.setData(data);
        return baseRespVo;
    }
    public static <T> BaseRespVo ok(T data, String errmsg, int errno){
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrmsg(errmsg);
        baseRespVo.setData(data);
        baseRespVo.setErrno(errno);
        return baseRespVo;
    }

    public static <T> BaseRespVo ok(int errno,String errmsg){
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrmsg(errmsg);
        baseRespVo.setErrno(errno);
        return baseRespVo;
    }

    public static <T> BaseRespVo invalidData(String msg) {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(504);
        baseRespVo.setErrmsg(msg);
        return baseRespVo;
    }
    public static <T> BaseRespVo invalidData() {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(504);
        baseRespVo.setErrmsg("更新数据已失效");
        return baseRespVo;
    }


}