package com.cskaoyan.bean.login_demo;

import com.cskaoyan.bean.reverseengineering.MarketAddress;
import com.cskaoyan.bean.reverseengineering.MarketFeedback;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author beiji
 * @date 2022/05/06 16:28
 */
@NoArgsConstructor
@Data
public class FeedbackData {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketFeedback> list; //当前页的数据
    //alt + s

    public static FeedbackData data(long total, Integer pages, Integer limit, Integer page, List list) {
        FeedbackData userData = new FeedbackData();
        userData.setTotal((int) total);
        userData.setPages(pages);
        userData.setPage(page);
        userData.setLimit(limit);
        userData.setList(list);
        return userData;
    }
}
