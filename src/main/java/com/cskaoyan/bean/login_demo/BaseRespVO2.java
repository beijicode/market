package com.cskaoyan.bean.login_demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @since 2022/05/06 20:49
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseRespVO2 {
    String errmsg;
    int errno;

    public static BaseRespVO2 ok(int errno, String errmsg) {
        BaseRespVO2 baseRespVO2 = new BaseRespVO2();
        baseRespVO2.setErrno(errno);
        baseRespVO2.setErrmsg(errmsg);
        return baseRespVO2;
    }
}