package com.cskaoyan.bean.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author stone
 * @date 2022/01/06 16:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseParam {
    Integer page;
    Integer limit;
    String sort;
    String order;
}
