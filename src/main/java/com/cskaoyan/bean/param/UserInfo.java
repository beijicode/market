package com.cskaoyan.bean.param;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author stone
 * @date 2022/04/06 10:13
 */
@NoArgsConstructor
@Data
public class UserInfo {


    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListBean> list;

    @NoArgsConstructor
    @Data
    public static class ListBean {
        private Integer id;
        private String name;
        private String keywords;
        private String desc;
        private String iconUrl;
        private String picUrl;
        private String level;
        private List<ChildrenBean> children;

        @NoArgsConstructor
        @Data
        public static class ChildrenBean {
            private Integer id;
            private String name;
            private String keywords;
            private String desc;
            private String iconUrl;
            private String picUrl;
            private String level;
        }
    }
}
