package com.cskaoyan.bean.reverseengineering;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketAftersale {
    private Integer id;

    private String aftersaleSn;

    private String orderId;

    private Integer userId;

    private Integer type;

    private String reason;

    private BigDecimal amount;

    private String[] pictures;

    private String comment;

    private Short status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date handleTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean deleted;

}