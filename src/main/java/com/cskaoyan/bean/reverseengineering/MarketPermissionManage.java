package com.cskaoyan.bean.reverseengineering;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/9 22:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketPermissionManage {

    private Integer id;
    private Integer pid;
    private String name;
    private String label;
    private String api;
    private Integer type;
}
