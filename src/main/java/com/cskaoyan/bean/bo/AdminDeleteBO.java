package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 21:36
 */
@Data
public class AdminDeleteBO {

    private Integer id;
    private String username;
    private String avatar;
    private Integer[] roleIds;

}
