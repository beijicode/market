package com.cskaoyan.bean.bo.couponbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建日期: 2022/05/06 16:17
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponBO {
    private Integer page;
    private Integer limit;
    private String name;
    private Integer type;
    private Integer status;
    private String sort;
    private String order;
    private Integer total;
    private Integer pages;
    private Integer couponId;
    private Integer userId;
}
