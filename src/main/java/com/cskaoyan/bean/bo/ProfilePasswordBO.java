package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @description:
 * @date: 2022/05/10 14:38
 * @author: beiji
 */
@Data
public class ProfilePasswordBO {
    private String oldPassword;
    private String newPassword;
    private String newPassword2;
}
