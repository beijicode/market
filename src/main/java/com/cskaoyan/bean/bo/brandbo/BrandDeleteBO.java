package com.cskaoyan.bean.bo.brandbo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class BrandDeleteBO {

    /**
     * floorPrice : 10
     * picUrl : http://182.92.235.201:8083/wx/storage/fetch/bkhq6ym78cwicex4ckzg.jpg
     * addTime : 2022-05-06 11:36:59
     * name : 雪糕
     * updateTime : 2022-05-06 11:36:59
     * id : 1046015
     * desc : 大块的
     */
    private BigDecimal floorPrice;
    private String picUrl;
    private Date addTime;
    private String name;
    private Date updateTime;
    private Integer id;
    private String desc;

    public void setFloorPrice(BigDecimal floorPrice) {
        this.floorPrice = floorPrice;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getFloorPrice() {
        return floorPrice;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public Date getAddTime() {
        return addTime;
    }

    public String getName() {
        return name;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Integer getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }
}
