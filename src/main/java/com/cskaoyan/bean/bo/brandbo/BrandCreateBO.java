package com.cskaoyan.bean.bo.brandbo;

import java.math.BigDecimal;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class BrandCreateBO {

    /**
     * floorPrice : 5
     * picUrl : http://182.92.235.201:8083/wx/storage/fetch/50c668yoveqbugdhd66z.jpg
     * name : 雪糕
     * desc : 大块的
     */
    private BigDecimal floorPrice;
    private String picUrl;
    private String name;
    private String desc;

    public void setFloorPrice(BigDecimal floorPrice) {
        this.floorPrice = floorPrice;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getFloorPrice() {
        return floorPrice;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
