package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/10 10:23
 */
@Data
public class PermissionBO {

    String[] permissions;
    Integer roleId;
}
