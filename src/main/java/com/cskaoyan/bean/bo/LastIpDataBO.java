package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @author Charadic
 * @since 2022/05/11 14:09
 */
@Data
public class LastIpDataBO {
    String countryCode;
    String country;
    String province;
    String city;
    String ip;
    String latitude;
    String longtitude;
    String zipcode;
    String timezone;
    String refer;
}
