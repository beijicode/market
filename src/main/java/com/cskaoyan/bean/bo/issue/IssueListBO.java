package com.cskaoyan.bean.bo.issue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @since 2022/05/07 10:48
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueListBO {
    Integer page;
    Integer limit;
    String sort;
    String order;
    String question;
}