package com.cskaoyan.bean.bo.issue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 
 * @since 2022/05/06 23:25
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueDeleteBO {
    private String answer;
    private String question;

    private Integer id;
    private Date addTime;
    private Date updateTime;

    private boolean deleted;
}