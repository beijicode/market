package com.cskaoyan.bean.bo.issue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author tangwei
 * @since 2022/05/06 23:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueCreateBO {
    private String answer;
    private String question;

    private Integer id;
    private Date addTime;
    private Date updateTime;
}