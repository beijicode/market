package com.cskaoyan.bean.bo.goodsbo;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;

/**
 * @author Charadic
 * @since 2022/05/06 20:33
 */

@Data
public class CommentBO {
    Integer page;
    Integer limit;
    @DecimalMin("0")
    String userId;
    @DecimalMin("0")
    String valueId;
    String sort;
    String order;
}
