package com.cskaoyan.bean.bo.goodsbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Charadic
 * @since 2022/05/06 11:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsListBO {
    Integer page;
    Integer limit;
    String goodsSn;
    String name;
    String sort;
    String order;
    String goodsId;
}
