package com.cskaoyan.bean.bo.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 
 * @since 2022/05/07 12:03
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderListBO {

    private Integer page;
    private Integer limit;
    private String sort;
    private String order;

    // 注意是数组
    private Short[] orderStatusArray;
    private Integer orderId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date start;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date end;

    // private String start;
    // private String end;
    private Integer userId;
    private Integer orderSn;
}