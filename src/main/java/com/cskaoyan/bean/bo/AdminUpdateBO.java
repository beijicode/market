package com.cskaoyan.bean.bo;

import lombok.Data;

import java.util.Date;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 20:12
 */
@Data
public class AdminUpdateBO {

    private Integer id;

    private String username;

    private String password;

    private String lastLoginIp;

    private Date lastLoginTime;

    private String avatar;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    private Integer[] roleIds;
}
