package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 19:12
 */
@Data
public class AdminCreateBO {

    private String username;
    private String password;
    private String avatar;
    private Integer[] roleIds;

}
