package com.cskaoyan.bean.bo.keywordbo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/06 00:05
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordDeleteBO {
    private Integer id;

    private String keyword;

    private String url;

    private Boolean isHot;

    private Boolean isDefault;

    private Integer sortOrder;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}
