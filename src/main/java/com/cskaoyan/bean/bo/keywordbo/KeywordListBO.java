package com.cskaoyan.bean.bo.keywordbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Charadic
 * @since 2022/05/05 23:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordListBO {
    Integer page;
    Integer limit;
    String keyword;
    String url;
    String sort;
    String order;

}
