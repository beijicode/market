package com.cskaoyan.bean.bo.keywordbo;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Charadic
 * @since 2022/05/05 21:20
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordCreateBO {
    private String keyword;
    private String url;
    private Boolean isHot;
    private Boolean isDefault;
}
