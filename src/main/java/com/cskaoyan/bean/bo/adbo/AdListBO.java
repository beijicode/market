package com.cskaoyan.bean.bo.adbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建日期: 2022/05/06 15:03
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdListBO {
    private Integer page;
    private Integer limit;
    private String name;
    private String content;
    private String sort;
    private String order;
}
