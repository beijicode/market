package com.cskaoyan.bean.bo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 
 * @since 2022/05/06 14:31
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCreateBO {

    private Integer id;

    private String desc;
    private String iconUrl;
    private String keywords;
    private String level;
    private String name;
    private String picUrl;
    private Integer pid;

    private Integer sortOrder;
    private Date addTime;
    private Date updateTime;
    private Integer deleted;
}