package com.cskaoyan.bean.bo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 
 * @since 2022/05/06 16:51
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryUpdateBO {

    private List<CategoryUpdateChildrenBO> children;

    private String desc;
    private String iconUrl;
    private Integer id;
    private String keywords;
    private String level;
    private String name;
    private String picUrl;

    private Integer pid;

    private Date addTime;
    private Date updateTime;
}