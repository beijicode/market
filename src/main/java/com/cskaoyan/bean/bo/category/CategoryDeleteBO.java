package com.cskaoyan.bean.bo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/06 21:47
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDeleteBO {

    private List<CategoryDeleteChildrenBO> children;

    private String desc;
    private String iconUrl;
    private Integer id;
    private String keywords;
    private String level;
    private String name;
    private String picUrl;
}