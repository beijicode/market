package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @author Charadic
 * @since 2022/05/11 14:06
 */
@Data
public class LastIpBO {
    Integer code;
    String message;
    LastIpDataBO data;
}
