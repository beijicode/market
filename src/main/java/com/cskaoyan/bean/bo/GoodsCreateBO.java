package com.cskaoyan.bean.bo;

import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketGoodsAttribute;
import com.cskaoyan.bean.reverseengineering.MarketGoodsProduct;
import com.cskaoyan.bean.reverseengineering.MarketGoodsSpecification;
import lombok.Data;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/06 21:15
 */
@Data
public class GoodsCreateBO {
    private List<MarketGoodsAttribute> attributes;
    private MarketGoods goods;
    private List<MarketGoodsProduct> products;
    private List<MarketGoodsSpecification> specifications;
}
