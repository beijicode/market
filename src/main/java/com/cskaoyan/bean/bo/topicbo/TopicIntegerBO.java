package com.cskaoyan.bean.bo.topicbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建日期: 2022/05/06 22:25
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicIntegerBO {

    private Integer[] ids;
}
