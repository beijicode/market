package com.cskaoyan.bean.bo.topicbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建日期: 2022/05/06 20:11
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicBO {
    private Integer page;
    private Integer limit;
    private String title;
    private String subtitle;
    private String sort;
    private String order;
}
