package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.reverseengineering.MarketLog;
import lombok.Data;


import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 23:50
 */
@Data
public class LogListVO {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketLog> list; //当前页的数据

    public static LogListVO data(long total, Integer pages, Integer limit, Integer page, List list) {
        LogListVO logListVO = new LogListVO();
        logListVO.setTotal((int) total);
        logListVO.setPages(pages);
        logListVO.setPage(page);
        logListVO.setLimit(limit);
        logListVO.setList(list);
        return logListVO;
    }
}
