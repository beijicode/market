package com.cskaoyan.bean.vo;

import java.util.LinkedList;
import java.util.List;

/**
 * 创建日期: 2022/05/05 17:20
 *
 * @author zhangfuqiang
 */

public class RegionListVO {


    /**
     * errno : 0
     * data : {"total":31,"pages":1,"limit":31,"page":1,"list":[{"code":110000,"children":[{"code":110100,"children":[{"code":110101,"name":"东城区","id":376,"type":3}],"name":"市辖区","id":32,"type":2}],"name":"北京市","id":1,"type":1}]}
     * errmsg : 成功
     */
    private Integer errno;
    private DataEntity data = new DataEntity();
    private String errmsg;

    public void setErrno(Integer errno) {
        this.errno = errno;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public Integer getErrno() {
        return errno;
    }

    public DataEntity getData() {
        return data;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public class DataEntity {
        /**
         * total : 31
         * pages : 1
         * limit : 31
         * page : 1
         * list : [{"code":110000,"children":[{"code":110100,"children":[{"code":110101,"name":"东城区","id":376,"type":3}],"name":"市辖区","id":32,"type":2}],"name":"北京市","id":1,"type":1}]
         */
        private Integer total = 31;
        private Integer pages = 1;
        private Integer limit = 31;
        private Integer page = 1;
        private List<ListEntity> list = new LinkedList<>();

        public void setTotal(Integer total) {
            this.total = total;
        }

        public void setPages(Integer pages) {
            this.pages = pages;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public void setList(List<ListEntity> list) {
            this.list = list;
        }

        public Integer getTotal() {
            return total;
        }

        public Integer getPages() {
            return pages;
        }

        public Integer getLimit() {
            return limit;
        }

        public Integer getPage() {
            return page;
        }

        public List<ListEntity> getList() {
            return list;
        }
        public List<ListEntity> addList() {
            ListEntity listEntity = new ListEntity();
            list.add(listEntity);
            return list;
        }

        public class ListEntity {
            /**
             * code : 110000
             * children : [{"code":110100,"children":[{"code":110101,"name":"东城区","id":376,"type":3}],"name":"市辖区","id":32,"type":2}]
             * name : 北京市
             * id : 1
             * type : 1
             */
            private Integer code;
            private List<ChildrenEntity> children =  new LinkedList<>();
            private String name;
            private Integer id;
            private Integer type;

            public void setCode(Integer code) {
                this.code = code;
            }

            public void setChildren(List<ChildrenEntity> children) {
                this.children = children;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public void setType(Integer type) {
                this.type = type;
            }

            public Integer getCode() {
                return code;
            }

            public List<ChildrenEntity> getChildren() {
                return children;
            }

            public List<ChildrenEntity> addChildren() {
                ChildrenEntity childrenEntity = new ChildrenEntity();
                children.add(childrenEntity);
                return children;
            }

            public String getName() {
                return name;
            }

            public Integer getId() {
                return id;
            }

            public Integer getType() {
                return type;
            }

            public class ChildrenEntity {
                /**
                 * code : 110100
                 * children : [{"code":110101,"name":"东城区","id":376,"type":3}]
                 * name : 市辖区
                 * id : 32
                 * type : 2
                 */
                private Integer code;
                private List<ChildrenEntity2> children = new LinkedList<>();
                private String name;
                private Integer id;
                private Integer type;


                public List<ChildrenEntity2> getChildren() {
                    return children;
                }

                public List<ChildrenEntity2> addChildren() {
                    ChildrenEntity2 childrenEntity2 = new ChildrenEntity2();
                    children.add(childrenEntity2);
                    return children;
                }

                public void setChildren(List<ChildrenEntity2> children) {
                    this.children = children;
                }

                public void setCode(Integer code) {
                    this.code = code;
                }


                public void setName(String name) {
                    this.name = name;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public void setType(Integer type) {
                    this.type = type;
                }

                public Integer getCode() {
                    return code;
                }


                public String getName() {
                    return name;
                }

                public Integer getId() {
                    return id;
                }

                public Integer getType() {
                    return type;
                }

                public class ChildrenEntity2 {
                    /**
                     * code : 110101
                     * name : 东城区
                     * id : 376
                     * type : 3
                     */
                    private Integer code;
                    private String name;
                    private Integer id;
                    private Integer type;

                    public void setCode(Integer code) {
                        this.code = code;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public void setId(Integer id) {
                        this.id = id;
                    }

                    public void setType(Integer type) {
                        this.type = type;
                    }

                    public Integer getCode() {
                        return code;
                    }

                    public String getName() {
                        return name;
                    }

                    public Integer getId() {
                        return id;
                    }

                    public Integer getType() {
                        return type;
                    }
                }
            }
        }
    }
}
