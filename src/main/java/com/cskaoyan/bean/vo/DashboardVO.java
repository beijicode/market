package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @description:
 * @date: 2022/05/05 20:21
 * @author: beiji
 */
@Data
public class DashboardVO {
    private Integer goodsTotal;
    private Integer userTotal;
    private Integer productTotal;
    private Integer orderTotal;
}
