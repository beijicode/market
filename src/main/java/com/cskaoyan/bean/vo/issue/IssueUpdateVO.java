package com.cskaoyan.bean.vo.issue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 
 * @since 2022/05/06 23:26
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueUpdateVO {
    Integer id;
    String question;
    String answer;
    Date addTime;
    Date updateTime;
}