package com.cskaoyan.bean.vo.issue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 
 * @since 2022/05/06 22:51
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueListIntermediateVO {
    Integer id;
    String question;
    String answer;
    Date addTime;
    Date updateTime;
    boolean deleted;
}