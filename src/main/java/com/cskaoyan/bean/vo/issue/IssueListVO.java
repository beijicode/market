package com.cskaoyan.bean.vo.issue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/06 22:47
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueListVO {

    private long total;
    private int pages;
    private int limit;
    private int page;

    private List<IssueListIntermediateVO> list;

}