package com.cskaoyan.bean.vo.keywordvo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Charadic
 * @since 2022/05/05 22:01
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordCreateVO {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date addTime;
    Integer id;
    String keyword;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date updateTime;
    String url;

}
