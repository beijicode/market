package com.cskaoyan.bean.vo.keywordvo;

import com.cskaoyan.bean.reverseengineering.MarketKeyword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/05 19:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeywordListVO {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<MarketKeyword> list;
}
