package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.reverseengineering.MarketStorage;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/7 9:07
 */
@Data
public class StorageListVO {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketStorage> list; //当前页的数据

    public static StorageListVO data(long total, Integer pages, Integer limit, Integer page, List list) {
        StorageListVO storageListVO = new StorageListVO();
        storageListVO.setTotal((int) total);
        storageListVO.setPages(pages);
        storageListVO.setPage(page);
        storageListVO.setLimit(limit);
        storageListVO.setList(list);
        return storageListVO;
    }
}
