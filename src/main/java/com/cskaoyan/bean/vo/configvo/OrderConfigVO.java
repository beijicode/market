package com.cskaoyan.bean.vo.configvo;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/7
 */
public class OrderConfigVO {

    /**
     * market_order_unconfirm : 7
     * market_order_unpaid : 30
     * market_order_comment : 14
     */
    private String market_order_unconfirm;
    private String market_order_unpaid;
    private String market_order_comment;

    public void setMarket_order_unconfirm(String market_order_unconfirm) {
        this.market_order_unconfirm = market_order_unconfirm;
    }

    public void setMarket_order_unpaid(String market_order_unpaid) {
        this.market_order_unpaid = market_order_unpaid;
    }

    public void setMarket_order_comment(String market_order_comment) {
        this.market_order_comment = market_order_comment;
    }

    public String getMarket_order_unconfirm() {
        return market_order_unconfirm;
    }

    public String getMarket_order_unpaid() {
        return market_order_unpaid;
    }

    public String getMarket_order_comment() {
        return market_order_comment;
    }
}
