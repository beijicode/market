package com.cskaoyan.bean.vo.configvo;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/7
 */
public class ExpressConfigVO {

    /**
     * market_express_freight_min : 87
     * market_express_freight_value : 1
     */
    private String market_express_freight_min;
    private String market_express_freight_value;

    public void setMarket_express_freight_min(String market_express_freight_min) {
        this.market_express_freight_min = market_express_freight_min;
    }

    public void setMarket_express_freight_value(String market_express_freight_value) {
        this.market_express_freight_value = market_express_freight_value;
    }

    public String getMarket_express_freight_min() {
        return market_express_freight_min;
    }

    public String getMarket_express_freight_value() {
        return market_express_freight_value;
    }
}
