package com.cskaoyan.bean.vo.configvo;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/7
 */
public class WXConfigVO {

    /**
     * market_wx_index_new : 6
     * market_wx_index_topic : 5
     * market_wx_share : true
     * market_wx_index_brand : 4
     * market_wx_catlog_goods : 9
     * market_wx_catlog_list : 4
     * market_wx_index_hot : 6
     */
    private String market_wx_index_new;
    private String market_wx_index_topic;
    private String market_wx_share;
    private String market_wx_index_brand;
    private String market_wx_catlog_goods;
    private String market_wx_catlog_list;
    private String market_wx_index_hot;

    public void setMarket_wx_index_new(String market_wx_index_new) {
        this.market_wx_index_new = market_wx_index_new;
    }

    public void setMarket_wx_index_topic(String market_wx_index_topic) {
        this.market_wx_index_topic = market_wx_index_topic;
    }

    public void setMarket_wx_share(String market_wx_share) {
        this.market_wx_share = market_wx_share;
    }

    public void setMarket_wx_index_brand(String market_wx_index_brand) {
        this.market_wx_index_brand = market_wx_index_brand;
    }

    public void setMarket_wx_catlog_goods(String market_wx_catlog_goods) {
        this.market_wx_catlog_goods = market_wx_catlog_goods;
    }

    public void setMarket_wx_catlog_list(String market_wx_catlog_list) {
        this.market_wx_catlog_list = market_wx_catlog_list;
    }

    public void setMarket_wx_index_hot(String market_wx_index_hot) {
        this.market_wx_index_hot = market_wx_index_hot;
    }

    public String getMarket_wx_index_new() {
        return market_wx_index_new;
    }

    public String getMarket_wx_index_topic() {
        return market_wx_index_topic;
    }

    public String getMarket_wx_share() {
        return market_wx_share;
    }

    public String getMarket_wx_index_brand() {
        return market_wx_index_brand;
    }

    public String getMarket_wx_catlog_goods() {
        return market_wx_catlog_goods;
    }

    public String getMarket_wx_catlog_list() {
        return market_wx_catlog_list;
    }

    public String getMarket_wx_index_hot() {
        return market_wx_index_hot;
    }
}
