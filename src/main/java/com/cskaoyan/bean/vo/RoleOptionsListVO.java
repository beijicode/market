package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 14:05
 */
@Data
public class RoleOptionsListVO {

    private Integer value;
    private String label;

}
