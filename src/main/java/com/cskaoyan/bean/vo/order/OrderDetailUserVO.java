package com.cskaoyan.bean.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @since 2022/05/07 20:04
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailUserVO {
    private String nickname;
    private String avatar;
}