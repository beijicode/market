package com.cskaoyan.bean.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author tangwei
 * @since 2022/05/07 12:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderListVO<T> {

    // private int id;
    // private int userId;
    // private String orderSn;
    // private int orderStatus;
    // private int aftersaleStatus;
    // private String consignee;
    // private String mobile;
    // private String address;
    // private String message;
    // private double goodsPrice;
    // private double freightPrice;
    //
    // private double couponPrice;
    // private double integralPrice;
    // private double grouponPrice;
    // private double orderPrice;
    // private double actualPrice;
    // private int comments;
    // private String addTime;
    // private String updateTime;
    // private boolean deleted;


    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<T> list; //当前页的数据

    public static OrderListVO data(long total, Integer pages, Integer limit, Integer page, List list) {
        OrderListVO orderListVO = new OrderListVO();

        orderListVO.setTotal((int) total);
        orderListVO.setPages(pages);
        orderListVO.setPage(page);
        orderListVO.setLimit(limit);
        orderListVO.setList(list);

        return orderListVO;
    }
}