package com.cskaoyan.bean.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @since 2022/05/07 16:36
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpressListVO {
    private String code;
    private String name;
}