package com.cskaoyan.bean.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @since 2022/05/07 19:58
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailOrderVO {
    private Integer id;

    private Integer userId;

    private String orderSn;

    private Short orderStatus;

    private Short aftersaleStatus;

    private String consignee;

    private String mobile;

    private String address;

    private String message;

    private BigDecimal goodsPrice;

    private BigDecimal freightPrice;

    private BigDecimal couponPrice;

    private BigDecimal integralPrice;

    private BigDecimal grouponPrice;

    private BigDecimal orderPrice;

    private BigDecimal actualPrice;

    private Short comments;

    private Date endTime;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
    
}