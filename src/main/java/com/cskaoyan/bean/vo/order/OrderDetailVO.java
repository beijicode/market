package com.cskaoyan.bean.vo.order;

import com.cskaoyan.bean.reverseengineering.MarketOrderGoods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/07 19:36
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailVO {
    // private MarketOrder marketOrder;
    // private List<MarketOrderGoods> marketOrderGoods;
    // private MarketUser marketUser;

    // private OrderDetailOrderVO orderDetailOrderVO;
    // private List<MarketOrderGoods> marketOrderGoods;
    // private OrderDetailUserVO orderDetailUserVO;

    private OrderDetailOrderVO order;
    private List<MarketOrderGoods> orderGoods;
    private OrderDetailUserVO user;

}