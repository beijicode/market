package com.cskaoyan.bean.vo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/05 20:56
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryListVO {
    private long total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    // private List<ParentCategoryVO> parentCategoryVOList;
    private List<ParentCategoryVO> list;

    // public static CategoryListVO CategoryList(Integer total, Integer pages, Integer limit, Integer page, List list) {
    //     CategoryListVO categoryListVO = new CategoryListVO();
    //     categoryListVO
    // }
}