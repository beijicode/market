package com.cskaoyan.bean.vo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 
 * @since 2022/05/06 14:51
 * @author tangwei
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryCreateVO {

    private int id;
    private String name;
    private String keywords;
    private String desc;
    private int pid;
    private String iconUrl;
    private String picUrl;
    private String level;
    private Date addTime;
    private Date updateTime;
}