package com.cskaoyan.bean.vo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tangwei
 * @since 2022/05/05 22:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParentCategoryIntermediateVO {

    private Integer id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
}