package com.cskaoyan.bean.vo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/05 22:07
 * @author tangwei
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParentCategoryVO {

    private Integer id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    // List<ChildrenOfCategoryVO> childrenOfCategoryList;
    List<ChildrenOfCategoryVO> children;

}