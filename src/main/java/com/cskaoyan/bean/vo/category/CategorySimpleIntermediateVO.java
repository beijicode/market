package com.cskaoyan.bean.vo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @since 2022/05/05 23:49
 * @author tangwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategorySimpleIntermediateVO {
    Integer value;
    String label;
}