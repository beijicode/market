package com.cskaoyan.bean.vo.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 
 * @since 2022/05/05 23:47
 * @author tangwei
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategorySimpleVO {
    private long total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<CategorySimpleIntermediateVO> list;
    
}