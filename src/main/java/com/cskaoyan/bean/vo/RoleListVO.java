package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.reverseengineering.MarketRole;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/5 22:27
 */
@Data
@NoArgsConstructor
public class RoleListVO {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketRole> list; //当前页的数据

    public static RoleListVO data(long total, Integer pages, Integer limit, Integer page, List list) {
        RoleListVO roleListVO = new RoleListVO();
        roleListVO.setTotal((int) total);
        roleListVO.setPages(pages);
        roleListVO.setPage(page);
        roleListVO.setLimit(limit);
        roleListVO.setList(list);
        return roleListVO;
    }
}
