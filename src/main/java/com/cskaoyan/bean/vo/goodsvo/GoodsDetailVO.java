package com.cskaoyan.bean.vo.goodsvo;

import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketGoodsAttribute;
import com.cskaoyan.bean.reverseengineering.MarketGoodsProduct;
import com.cskaoyan.bean.reverseengineering.MarketGoodsSpecification;
import lombok.Data;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/06 15:28
 */
@Data
public class GoodsDetailVO {
    private List<MarketGoodsAttribute> attributes;
    private Integer[] categoryIds;
    private MarketGoods goods;
    private List<MarketGoodsProduct> products;
    private List<MarketGoodsSpecification> specifications;

}
