package com.cskaoyan.bean.vo.goodsvo;

import lombok.Data;

/**
 * @author Charadic
 * @since 2022/05/06 17:10
 */
@Data
public class CategoryChildren {
    private Integer value;
    private String label;
}
