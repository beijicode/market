package com.cskaoyan.bean.vo.goodsvo;

import lombok.Data;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/06 16:37
 */
@Data
public class GoodsCatAndBrandVO {
    private List<GoodsBrandVO> brandList;
    private List<GoodsCategoryVO> categoryList;

}
