package com.cskaoyan.bean.vo.goodsvo;

import lombok.Data;

/**
 * @author Charadic
 * @since 2022/05/06 17:03
 */
@Data
public class GoodsBrandVO {
    private Integer value;
    private String label;
}
