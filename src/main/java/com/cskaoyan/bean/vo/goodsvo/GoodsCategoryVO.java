package com.cskaoyan.bean.vo.goodsvo;

import lombok.Data;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/06 17:03
 */
@Data
public class GoodsCategoryVO {
    private List<CategoryChildren> children;
    private Integer value;
    private String label;
}
