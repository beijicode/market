package com.cskaoyan.bean.vo.goodsvo;

import com.cskaoyan.bean.reverseengineering.MarketComment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/06 20:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentListVO {
    Integer limit;
    Integer page;
    Integer pages;
    Integer total;
    List<MarketComment> list;

}
