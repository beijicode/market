package com.cskaoyan.bean.vo.goodsvo;

import com.cskaoyan.bean.reverseengineering.MarketGoods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Charadic
 * @since 2022/05/06 11:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsListVO {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<MarketGoods> list;
}
