package com.cskaoyan.bean.vo.statvo;

import com.cskaoyan.bean.po.GoodsStatPO;

import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class GoodStatVO {

    /**
     * columns : ["day","orders","products","amount"]
     * rows : [{"amount":3307,"orders":4,"day":"2022-05-05","products":4}]
     */
    private List<String> columns = new LinkedList<>();
    private List<GoodsStatPO> rows = new LinkedList<>();

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public void setRows(List<GoodsStatPO> rows) {
        this.rows = rows;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<GoodsStatPO> getRows() {
        return rows;
    }

    public List<String> addColumns() {
        String s = new String();
        columns.add(s);
        return columns;
    }

    public List<GoodsStatPO> addRows() {
        GoodsStatPO goodsStatPO = new GoodsStatPO();
        rows.add(goodsStatPO);
        return rows;
    }

    public class RowsEntity {
        /**
         * amount : 3307.0
         * orders : 4
         * day : 2022-05-05
         * products : 4
         */
        private Double amount;
        private Integer orders;
        private String day;
        private Integer products;

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public void setOrders(Integer orders) {
            this.orders = orders;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Double getAmount() {
            return amount;
        }

        public Integer getOrders() {
            return orders;
        }

        public String getDay() {
            return day;
        }

        public Integer getProducts() {
            return products;
        }
    }
}
