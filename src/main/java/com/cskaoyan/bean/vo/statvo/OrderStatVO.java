package com.cskaoyan.bean.vo.statvo;

import com.cskaoyan.bean.po.OrderStatPO;

import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class OrderStatVO {

    /**
     * columns : ["day","orders","customers","amount","pcr"]
     * rows : [{"day" :"2022-05-05","orders" :1,"customers" : 2,"amount" : 10,"pcr" : 1}]
     */
    private List<String> columns = new LinkedList<>();
    private List<OrderStatPO> rows;

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public void setRows(List<OrderStatPO> rows) {
        this.rows = rows;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<OrderStatPO> getRows() {
        return rows;
    }
}
