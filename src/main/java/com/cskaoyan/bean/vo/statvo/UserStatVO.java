package com.cskaoyan.bean.vo.statvo;

import com.cskaoyan.bean.po.UserStatPO;

import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
public class UserStatVO {

    /**
     * columns : ["day","users"]
     * rows : [{"day":"2019-04-20","users":1}]
     */
    private List<String> columns = new LinkedList<>();
    private List<UserStatPO> rows = new LinkedList<>();

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public void setRows(List<UserStatPO> rows) {
        this.rows = rows;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<UserStatPO> getRows() {
        return rows;
    }

    public List<String> addColumns() {
        String s = new String();
        columns.add(s);
        return columns;
    }

    public List<UserStatPO> addRows() {

        UserStatPO userStatPO = new UserStatPO();
        rows.add(userStatPO);
        return rows;
    }

    public class RowsEntity {
        /**
         * day : 2019-04-20
         * users : 1
         */
        private String day;
        private int users;

        public void setDay(String day) {
            this.day = day;
        }

        public void setUsers(int users) {
            this.users = users;
        }

        public String getDay() {
            return day;
        }

        public int getUsers() {
            return users;
        }
    }
}
