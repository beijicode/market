package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.reverseengineering.MarketAdmin;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: 管理员管理
 * @author: lxc
 * @date: 2022/5/5 20:37
 */
@Data
@NoArgsConstructor
public class AdminListVO {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketAdmin> list; //当前页的数据

    public static AdminListVO data(long total, Integer pages, Integer limit, Integer page, List list) {
        AdminListVO adminListVO = new AdminListVO();
        adminListVO.setTotal((int) total);
        adminListVO.setPages(pages);
        adminListVO.setPage(page);
        adminListVO.setLimit(limit);
        adminListVO.setList(list);
        return adminListVO;
    }
}
