package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 14:01
 */
@Data
@NoArgsConstructor
public class RoleOptionsVO {

    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<RoleOptionsListVO> list; //当前页的数据

    public static RoleOptionsVO data(long total, Integer pages, Integer limit, Integer page, List list) {
        RoleOptionsVO roleOptionsVO = new RoleOptionsVO();
        roleOptionsVO.setTotal((int) total);
        roleOptionsVO.setPages(pages);
        roleOptionsVO.setPage(page);
        roleOptionsVO.setLimit(limit);
        roleOptionsVO.setList(list);
        return roleOptionsVO;
    }
}
