package com.cskaoyan.bean.vo.promote.adVO;

import com.cskaoyan.bean.reverseengineering.MarketAd;
import com.cskaoyan.bean.bo.adbo.AdListBO;
import com.cskaoyan.bean.param.BaseParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/05 20:30
 *
 * @author yangfan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdData {
    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<MarketAd> list; //当前页的数据


}
