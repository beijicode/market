package com.cskaoyan.bean.vo.promote.topicVO;


import com.cskaoyan.bean.reverseengineering.MarketGoods;
import com.cskaoyan.bean.reverseengineering.MarketTopic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/06 21:59
 *
 * @author yangfan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TopicReadVO {
    private List<MarketGoods> goodsList;
    private MarketTopic topic;
}
