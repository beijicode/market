package com.cskaoyan.bean.vo.promote.couponVO;


import com.cskaoyan.bean.reverseengineering.MarketCouponUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/06 23:01
 *
 * @author yangfan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponListuserVO {
    private Integer page;
    private Integer limit;
    private String sort;
    private String order;
    private Integer total;
    private Integer pages;
    private Integer couponId;
    private List<MarketCouponUser> list;
}
