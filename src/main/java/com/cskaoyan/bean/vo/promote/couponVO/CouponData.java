package com.cskaoyan.bean.vo.promote.couponVO;

import com.cskaoyan.bean.reverseengineering.MarketCoupon;
import com.cskaoyan.bean.bo.couponbo.CouponBO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 创建日期: 2022/05/06 16:23
 *
 * @author yangfan
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponData {
    private Integer page;
    private Integer limit;
    private String name;
    private Integer type;
    private Integer status;
    private String sort;
    private String order;
    private Integer total;
    private Integer pages;

    private List<MarketCoupon> list;
}
