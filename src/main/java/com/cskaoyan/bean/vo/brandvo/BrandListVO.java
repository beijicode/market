package com.cskaoyan.bean.vo.brandvo;

import com.cskaoyan.bean.reverseengineering.MarketBrand;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

/**
 * 类说明
 *
 * @author zhangfuqiang
 * @date 2022/5/6
 */
@AllArgsConstructor
@NoArgsConstructor
public class BrandListVO {

    /**
     * total : 1
     * pages : 1
     * limit : 20
     * page : 1
     * list : [{"floorPrice":12,"picUrl":"http://182.92.235.201:8083/wx/storage/fetch/o7bbykfdr6hr8mrn7zvj.jpg","deleted":false,"addTime":"2022-05-06 11:05:29","sortOrder":50,"name":"测试制造商","updateTime":"2022-05-06 11:05:29","id":1046013,"desc":"测试专用"}]
     */
    private Long total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<MarketBrand> list = new LinkedList<>();


    public void setTotal(Long total) {
        this.total = total;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setList(List<MarketBrand> list) {
        this.list = list;
    }

    public Long getTotal() {
        return total;
    }

    public Integer getPages() {
        return pages;
    }

    public Integer getLimit() {
        return limit;
    }

    public Integer getPage() {
        return page;
    }

    public List<MarketBrand> getList() {
        return list;
    }

    /*public class ListEntity {
        *//**
         * floorPrice : 12.0
         * picUrl : http://182.92.235.201:8083/wx/storage/fetch/o7bbykfdr6hr8mrn7zvj.jpg
         * deleted : false
         * addTime : 2022-05-06 11:05:29
         * sortOrder : 50
         * name : 测试制造商
         * updateTime : 2022-05-06 11:05:29
         * id : 1046013
         * desc : 测试专用
         *//*
        private BigDecimal floorPrice;
        private String picUrl;
        private Boolean deleted;
        private Date addTime;
        private Byte sortOrder;
        private String name;
        private Date updateTime;
        private Integer id;
        private String desc;

        public void setFloorPrice(BigDecimal floorPrice) {
            this.floorPrice = floorPrice;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public void setDeleted(Boolean deleted) {
            this.deleted = deleted;
        }

        public void setAddTime(Date addTime) {
            this.addTime = addTime;
        }

        public void setSortOrder(Byte sortOrder) {
            this.sortOrder = sortOrder;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setUpdateTime(Date updateTime) {
            this.updateTime = updateTime;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public BigDecimal getFloorPrice() {
            return floorPrice;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public Boolean isDeleted() {
            return deleted;
        }

        public Date getAddTime() {
            return addTime;
        }

        public Byte getSortOrder() {
            return sortOrder;
        }

        public String getName() {
            return name;
        }

        public Date getUpdateTime() {
            return updateTime;
        }

        public Integer getId() {
            return id;
        }

        public String getDesc() {
            return desc;
        }
    }*/
}
