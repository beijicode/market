package com.cskaoyan.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @date: 2022/05/10 22:42
 * @author: beiji
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxAboutVO {
    private String address;
    private String latitude;
    private String longitude;
    private String name;
    private String phone;
    private String qq;

}
