package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/6 9:18
 */
@Data
@NoArgsConstructor
public class RoleCreateVO {

    private Integer id;
    private String name;
    private String desc;
    private Date addTime;
    private Date updateTime;

}
