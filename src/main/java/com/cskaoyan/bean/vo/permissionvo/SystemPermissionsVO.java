package com.cskaoyan.bean.vo.permissionvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/9 22:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemPermissionsVO {

    private String id;
    private String label;
    private Integer pid;
    private Integer tId;
    private List<NextChildrenVO> children;


}
