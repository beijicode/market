package com.cskaoyan.bean.vo.permissionvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/9 22:15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePermissionsVO {

    private String[] assignedPermissions;
    private List<SystemPermissionsVO> systemPermissions;
}
