package com.cskaoyan.bean.vo.permissionvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/9 23:42
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NextChildrenVO {

    private String id;
    private String label;
    private Integer pId;
    private Integer tId;
    private List<ChildrenVO> children;
}
