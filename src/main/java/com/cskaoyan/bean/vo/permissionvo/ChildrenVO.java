package com.cskaoyan.bean.vo.permissionvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: lxc
 * @date: 2022/5/9 22:42
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChildrenVO {

    private String api;
    private String id;
    private String label;
    private Integer tId;
    private Integer pId;
}
